package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Container_account_activity;
import com.phazeprojectsystems.Activities.Login_screen;
import com.phazeprojectsystems.Api.GetProfilepic_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.GetProfilePicData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ravi on 7/21/2017.
 */

public class Edit_profile extends Fragment {

    private CircleImageView userImage;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView username;
    private RelativeLayout editprofile, upgradepro, signout , acback_arrow_about;
    private String[] list;
    private String image,userid ;
    public  static Edit_profile edit_profile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.edit_profile_fragment, container, false);

        FindView(view);
        userid=list[0];
        clicklist();



        image = list[3];

        if (!(image == null)) {
            if (!(image.equals(""))) {
                String image1 = Constant.picuri.concat(image);
                Glide.with(this).load(image1).fitCenter().centerCrop().into(userImage);
            } else {
            }
        } else {
        }
     //   GetImage(userid);

        edit_profile=this;
        return view;
    }
    private void FindView(View view) {

        username = (TextView) view.findViewById(R.id.edit_prifile_username);
        userImage = (CircleImageView) view.findViewById(R.id.UserImage);
        editprofile = (RelativeLayout) view.findViewById(R.id.relative_edit_profile);
        acback_arrow_about = (RelativeLayout) view.findViewById(R.id.acback_arrow_about);
        upgradepro = (RelativeLayout) view.findViewById(R.id.relative_upgrade_pro);
        signout = (RelativeLayout) view.findViewById(R.id.relative_signout);

        list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        username.setText(list[1]);

    }

    private void clicklist()
    {
        acback_arrow_about.setOnClickListener(v->{

            getActivity().onBackPressed();

        });

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new Account_detail_fragment(), "");
            }
        });

        upgradepro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "This function will work soon", Toast.LENGTH_SHORT).show();
            }
        });

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MySharePrafranceClass.ClearAll(getActivity());
                MySharePrafranceClass.ClearAllLoginShare(getActivity());
                MySharePrafranceClass.ClearAllProjectShare(getActivity());
                Intent intent = new Intent(getActivity(), Login_screen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        String[] dataList = MySharePrafranceClass.GetSharePrefrance(getActivity());
           String Updateimage=dataList[3];

        if (!(Updateimage == null)) {
            if (!(image.equals(""))) {
                String image = Constant.picuri.concat(Updateimage);

                Glide.with(this).load(image).fitCenter().centerCrop().into(userImage);

            } else {
            }
        } else {
        }


    }

    private void GetImage(String Userid) {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        GetProfilepic_Api api = restAdapter.create(GetProfilepic_Api.class);
        api.GetProfilePic(Userid, new Callback<GetProfilePicData>() {
            @Override
            public void success(GetProfilePicData getProfilePicData, Response response) {
                    String thumb = getProfilePicData.getThumb();

                    if (!(thumb == null)) {
                        String image = Constant.picuri.concat(thumb);
                        Glide.with(getActivity()).load(image).into(userImage);
                    }else {}

            }

            @Override
            public void failure(RetrofitError error) {
                Constant.toast(getActivity(), ""+error);
            }
        });
    }

}
