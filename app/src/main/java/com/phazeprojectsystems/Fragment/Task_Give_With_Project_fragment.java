package com.phazeprojectsystems.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.*;
import com.phazeprojectsystems.Adapter.AssignToAdapter;
import com.phazeprojectsystems.Api.Create_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.FragmentDate;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Model.CreateTaskData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


/**
 * Created by Ravi on 9/22/2017.
 */

public class Task_Give_With_Project_fragment extends Fragment {


    private  String projectname,get_description,get_taskname,get_dueDate;
    private static  final  int DIALOG_ID=1;
    private TextView TaskName,discription,due_Date,Project_name,AddTag,selectedMemer;
    private int Year_X,Month_X,Day_X;
    private RelativeLayout datePicker,assignetask;
    private EditText txt_taskname,txt_description;
    private ImageView backarrowedittask,AssignePlussIconBlue1,img_submit,img_red,img_yellow,img_green,img_cancel,img_red_cancel,img_yellow_cancel,img_green_cancel;
    private  String[] list;
    private String SelecterMember="";
    private String getMemberuserid="",ggetmemberid="";
    public static Map<String,String> TaskMember =  new HashMap<>();
    private String[] projectitemlist;
    RelativeLayout addtag,imgcolor_tag,date_picker;
    private static  boolean redclicked=false;
    private static boolean yellowclicked=false;
    private static boolean greenclicked=false;
    public static Task_Give_With_Project_fragment task_Give_With_Project_fragment;
    private int color=0;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;
    Spinner spcategoryphaze, spdivphaze, spareaphaze, spsubphaze, spphaze;


    String categoryphaze[] = {"Category Phaze", "Walls", "Trim"};
    String divphaze[] = {"Div Phaze", "Main Floor", "Trim"};
    String areaphaze[] = {"Area Phaze", "Entry", "Entry Closet"};
    String phaze[] = {"Phaze", "Prime", "First Coat", "Final"};
    String subphaze[] = {"Sub Phaze", "Roll", "Cut-In", "Sand", "S.Fill", "S.Sand", "S.Prime",};
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView Assigntolist;
    private AssignToAdapter assigntoAdapter;
    private List<String> MemberNAme = new ArrayList<>();



    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.task_give_with_project_fragment, container, false);


        projectitemlist= MySharePrafranceClass.ProjectGetSharePrefrance(getActivity());
        list=MySharePrafranceClass.GetSharePrefrance(getActivity());
        TaskMember.clear();
        task_Give_With_Project_fragment=this;
        findView(view);
        clicklis();
        projectname = projectitemlist[1];
        Project_name.setText(projectname);


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        due_Date.setText(new StringBuilder().append(day)
                .append("/").append(month + 1).append("/").append(year)
                .append(" "));

        date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogFragment dialogFragment=new FragmentDate(view);
                dialogFragment.show(getFragmentManager(),"DatePicker");
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    Intent intent = new Intent(getActivity(),MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        } );

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


        if (TaskMember!=null){
            if (!TaskMember.isEmpty()){

                MemberNAme.clear();


                for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                    String e = entry.getKey();

                    MemberNAme.add(e);
                }

                SetLayout();

            }
        }


    }



    //    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(getActivity(), pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;


            due_Date.setText(new StringBuilder().append(day)
                    .append("/").append(month + 1).append("/").append(year)
                    .append(" "));

        }
    };



    private void findView(View view) {

        AssignePlussIconBlue1 = (ImageView)view.findViewById(R.id.AssignePlussIconBlue1);

        selectedMemer=(TextView)view.findViewById(R.id.selectedMemer);
        Assigntolist = (RecyclerView)view.findViewById(R.id.assigned_member12);
        datePicker=(RelativeLayout)view.findViewById(R.id.txt_edittext_date_Picker);
        AddTag=(TextView)view.findViewById(R.id.addtag);
        Project_name= (TextView) view.findViewById(R.id.text_edittext_projectname);
        due_Date= (TextView) view.findViewById(R.id.text_edittext_date);
        txt_taskname= (EditText)view.findViewById(R.id.txt_taskname_in_edit_task);
        txt_description= (EditText) view.findViewById(R.id.txt_edittext_description);
        img_submit= (ImageView) view.findViewById(R.id.img_submitedittask);
        assignetask=(RelativeLayout)view.findViewById(R.id.relative_assign_to_task);
        img_red= (ImageView) view.findViewById(R.id.imgtag_red);
        img_red_cancel= (ImageView) view.findViewById(R.id.imgtag_red_cancel);
        img_yellow= (ImageView) view.findViewById(R.id.imgtag_yellow);
        img_yellow_cancel= (ImageView) view.findViewById(R.id.imgtag_yellow_cancel);
        img_green= (ImageView) view.findViewById(R.id.imgtag_green);
        img_green_cancel= (ImageView) view.findViewById(R.id.imgtag_green_cancel);
        addtag= (RelativeLayout) view.findViewById(R.id.relative_tag);
        imgcolor_tag= (RelativeLayout)view.findViewById(R.id.relative_imgcolor);
        date_picker= (RelativeLayout) view.findViewById(R.id.txt_edittext_date_Picker);
        img_cancel= (ImageView) view.findViewById(R.id.img_cancel);
        backarrowedittask=(ImageView) view.findViewById(R.id.backarrow_edittask);

    }

    private void SetLayout() {

        linearLayoutManager= new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        Assigntolist.setLayoutManager(linearLayoutManager);
        assigntoAdapter = new AssignToAdapter(getActivity(),MemberNAme,"Task With Project");
        Assigntolist.setAdapter(assigntoAdapter);
        assigntoAdapter.notifyDataSetChanged();
    }

    private void clicklis() {

        backarrowedittask.setOnClickListener((View v)->{

            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        addtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imgcolor_tag.setVisibility(View.VISIBLE);
            }
        });
        img_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                color=isred();


            }
        });


        img_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color=isyellow();

            }
        });

        img_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = isgreen();
            }
        });


        assignetask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),Select_team_menber.class));
            }
        });

        AssignePlussIconBlue1.setOnClickListener(v->{

            startActivity(new Intent(getActivity(),Select_team_menber.class));

        });

        AddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        img_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_taskname=txt_taskname.getText().toString().trim();
                get_description=txt_description.getText().toString().trim();
                get_dueDate=due_Date.getText().toString();


                if (get_taskname.trim().equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), "Enter Task name", Toast.LENGTH_SHORT).show();
                }
//                else  if (get_description.trim().equalsIgnoreCase(""))
//                {
//                    Toast.makeText(getActivity(), "Enter description", Toast.LENGTH_SHORT).show();
//                }
                else  if (get_dueDate.trim().equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), "Enter Date ", Toast.LENGTH_SHORT).show();
                }
                else {
                    String userid = list[0];
                    String projectid = projectitemlist[0];
                    Createtask(userid,projectid,get_taskname,get_description,get_dueDate);
                }

            }
        });

        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().showDialog(DIALOG_ID);
            }
        });

    }


    private void Createtask(String s, final String projectid, final String get_taskname, String description, String duedate) {


        if (Network_check.isNetWorking(getActivity())){

            final ProgressDialog progressDoalog;
            progressDoalog = new ProgressDialog(getActivity());
            progressDoalog.setTitle("Please wait..");
            progressDoalog.show();



            SelecterMember="";
            getMemberuserid="";

            if (!(TaskMember.isEmpty())) {


                for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                    getMemberuserid =  entry.getValue()+ "," + getMemberuserid;


                    if(getMemberuserid.endsWith(",")) {

                        ggetmemberid= getMemberuserid.substring(0, getMemberuserid.length() - 1);
                    }
                }

            }

            if( color == 0){
                color =4;
            }

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);

            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();

            Create_Task_Api api = rest.create(Create_Task_Api.class);
            api.CreateTask(s, projectid,get_taskname,description, "",duedate,""+color,ggetmemberid, new Callback<CreateTaskData>() {
                @Override
                public void success(CreateTaskData createTaskData, Response response) {
                    if(createTaskData.getSuccess().equals("1")){
                        progressDoalog.dismiss();
                        Intent intent= new Intent(getActivity(),Task_activity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }else{
                        progressDoalog.dismiss();
                        Constant.toast(getActivity(),""+createTaskData.getMessage());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    progressDoalog.dismiss();
                    Constant.toast(getActivity(),"Check connection Please"+ error.getMessage());
                }
            });
        }else {

            Toast.makeText(getActivity(), "Check Connection Please", Toast.LENGTH_SHORT).show();
        }
    }



    private int isgreen(){
        if(!isGreenclicked()){
            setGreenclicked(true);
            setRedclicked(false);
            setYellowclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.cancel_green);
            return 3;
        }else {
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;

        }

    }
    private int isyellow(){
        if(!isYellowclicked()){
            setYellowclicked(true);
            setRedclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow);
            img_green.setImageResource(R.drawable.green_circle);
            return 2;

        }else{
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }
    private int  isred(){
        if(!isRedclicked()){
            setRedclicked(true);
            setGreenclicked(false);
            setYellowclicked(false);
            img_red.setImageResource(R.drawable.red);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 1;
        }
        else{
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }

    public static void AddTaskMember( Map <String,String> member) {

        TaskMember=member;
        for (int index=0 ; index < member.size();index++){

        }

    }
    public static boolean isRedclicked() {
        return redclicked;
    }

    public static void setRedclicked(boolean redclicked) {
        redclicked = redclicked;
    }

    public static boolean isYellowclicked() {
        return yellowclicked;
    }

    public static void setYellowclicked(boolean yellowclicked) {
        yellowclicked = yellowclicked;
    }

    public static boolean isGreenclicked() {
        return greenclicked;
    }

    public static void setGreenclicked(boolean greenclicked) {
        greenclicked = greenclicked;
    }


}
