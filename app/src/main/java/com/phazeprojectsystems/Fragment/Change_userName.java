package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Activities.Userproject_screen;
import com.phazeprojectsystems.Api.Change_UserName_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.ChangeUserNameData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ravi on 8/9/2017.
 */

public class Change_userName extends Fragment {
    private EditText NewUserName;
    private ImageView changeUserName;
    private RelativeLayout backarrow;
    private  String userCompany,userImage,userrole,newusername,userid,oldusername;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.change_user_name,container,false);
         FINDVIEWBYID(view);
         CLICKABLE(view);
         return view;
    }
    private void FINDVIEWBYID(View view) {
        NewUserName=(EditText)view.findViewById(R.id.newUserName_EditText);
        changeUserName=(ImageView)view.findViewById(R.id.click_sign_change_userNmae);
        backarrow= (RelativeLayout) view.findViewById(R.id.back_arrow_change_name);
    }
    private void CLICKABLE(View view) {

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        changeUserName.setOnClickListener((View v)-> {


                String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
                 userid=list[0];
                 oldusername=list[1];
                 userCompany=list[2];
                 userImage=list[3];
                 userrole=list[4];
                 newusername = NewUserName.getText().toString();


                 if (!validate()){
                     Toast.makeText(getActivity(), "Your Request Has Failed", Toast.LENGTH_SHORT).show();

                 }else {
                     ChangeUserName(userid,newusername,userCompany,userImage,userrole);
                 }
        });
    }

    private boolean validate(){
        boolean valid=true;

        if (newusername.isEmpty()){
            NewUserName.setError("Enter Valid User Name");
            valid=false;
        }
        if (newusername.equalsIgnoreCase(oldusername)){
            NewUserName.setError("Try Other User Name");
        }
        return valid;
    }
    private void ChangeUserName(final String userid, final String newusername, final String userCompany, final String image , final String role) {
        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Change_UserName_Api api=rest.create(Change_UserName_Api.class);
        api.ChnageUserName(userid, newusername, new Callback<ChangeUserNameData>() {
            @Override
            public void success(ChangeUserNameData changeUserNameData, Response response) {
                String status = changeUserNameData.getSuccess();
                        if(status.equals("1")) {


                            MySharePrafranceClass.ClearAll(getActivity());
                            MySharePrafranceClass.Share(getActivity(),userid,newusername,userCompany,image,role);
                            Toast.makeText(getActivity(), ""+changeUserNameData.getMessage(), Toast.LENGTH_SHORT).show();
                            NewUserName.setText("");

                           if (role.equals("1")) {
                               Intent intent = new Intent(getActivity(), MainActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
                               startActivity(intent);
                               getActivity().finish();
                           }else {

                               Intent intent = new Intent(getActivity(), Userproject_screen.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
                               startActivity(intent);
                               getActivity().finish();
                           }

                        } else{
                            Toast.makeText(getActivity(), ""+changeUserNameData.getMessage(), Toast.LENGTH_SHORT).show();
                        }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "Check Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
