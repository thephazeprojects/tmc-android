package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Activities.User_taskscreen;
import com.phazeprojectsystems.Activities.Userproject_screen;
import com.phazeprojectsystems.Api.Change_Email_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.ChangeEmailData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Change_email extends Fragment {
    private EditText newEmail,currentPass;
    private ImageView changeemail;
    private RelativeLayout backarrow;
    private  String NewEmail,curtpass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.change_email,container,false);
        FINDVIEWBYID(view);
        CLICKABLE(view);
        return view;
    }

    private void CLICKABLE(final View view) {

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });



        changeemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  NewEmail = newEmail.getText().toString();
                  curtpass = currentPass.getText().toString();
                  String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
                  String userid=list[0];
                  String userrole=list[4];


                if (!validate()){
                    Toast.makeText(getActivity(), "Your Request Has Failed", Toast.LENGTH_SHORT).show();
                }else {
                    ChangeEmail(userid, NewEmail,userrole);
                }

            }
        });
    }

    private boolean validate(){
        boolean valid=true;

        if (NewEmail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(NewEmail).matches()){
            newEmail.setError("Enter Valid Email");
            valid=false;
        }
        if (curtpass.isEmpty()||curtpass.length()<6){
            currentPass.setError("Enter Valid Password");
        }
        return valid;
    }
    private void ChangeEmail(String userid, String newEmail, final String role) {
        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Change_Email_Api api=rest.create(Change_Email_Api.class);
        api.ChangeEmail(userid,newEmail, new Callback<ChangeEmailData>() {
            @Override
            public void success(ChangeEmailData changeEmailData, Response response) {
                String status=changeEmailData.getSuccess();
                if(status.equals("1")){

                    Toast.makeText(getActivity(), ""+changeEmailData.getMessage(), Toast.LENGTH_SHORT).show();
                    MySharePrafranceClass.ClearAllLoginShare(getActivity());
                    clearText();
                    if (role.equals("1")){
                    Intent intent=new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    }
                    else {
                        Intent intent=new Intent(getActivity(), Userproject_screen.class);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    MySharePrafranceClass.LoginShare(getActivity(),newEmail,curtpass);

                }else
                {
                    Toast.makeText(getActivity(), ""+changeEmailData.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "Check Connection :-  "+error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void FINDVIEWBYID(final View view) {
        newEmail = (EditText) view.findViewById(R.id.newEmail);
        changeemail=(ImageView)view.findViewById(R.id.click_sign_change_Email);
        currentPass = (EditText) view.findViewById(R.id.Change_Email_currentPassword);
        backarrow= (RelativeLayout) view.findViewById(R.id.back_arrow_change_email);
    }
    private void clearText(){
        newEmail.setText("");
        currentPass.setText("");
    }

}
