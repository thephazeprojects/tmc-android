package com.phazeprojectsystems.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.Login_screen;
import com.phazeprojectsystems.R;

import static com.phazeprojectsystems.R.id.forgetemail_enter;


/**
 * Created by Ravi on 8/4/2017.
 */

public class Forget_Password extends DialogFragment{
    Button Forget_Email_Enter;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.change_password, container, false);
        Clickable(view);
        return view;
    }

private void Clickable(View view)
{
    Forget_Email_Enter=(Button)view.findViewById(forgetemail_enter);
    Forget_Email_Enter.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Toast.makeText(getActivity(), "Enter Email sucessfully", Toast.LENGTH_SHORT).show();
        }
    });
}}