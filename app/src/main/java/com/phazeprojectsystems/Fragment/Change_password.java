package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Activities.Userproject_screen;
import com.phazeprojectsystems.Api.Change_Password_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.ChangePasswordData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Change_password extends android.support.v4.app.Fragment {

    private  EditText currentPassword,newPassword,confirmPassword;
    private RelativeLayout back_arrow;
    private ImageView changePassword;
    private  String CurrentPassword,NewPassword,ConfirmPassword;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.change_password,container,false);
        FINDVIEWBYID(view);
        CLICKABLE(view);
        return view;
    }
    private void clearText(){
        currentPassword.setText("");
        confirmPassword.setText("");
        newPassword.setText("");
    }

    private void CLICKABLE(final View view) {  
        
           changePassword.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {


           
             CurrentPassword=currentPassword.getText().toString();
             NewPassword=newPassword.getText().toString();
             ConfirmPassword=confirmPassword.getText().toString();

            String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
            String userid=list[0];
            String userrole=list[4];

            if (!validate()){
                Toast.makeText(getActivity(), "Your Request Has Failed", Toast.LENGTH_SHORT).show();
            }else {
                Change_Password(userid,CurrentPassword,NewPassword,ConfirmPassword,userrole);
            }
         }
    });

           back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    getFragmentManager().popBackStackImmediate();
            }
        });
    }

    private boolean validate(){
        boolean valid=true;

        if (CurrentPassword.isEmpty()||CurrentPassword.length()<6){
            currentPassword.setError("Enter Valid  Current Password");
            valid=false;
        }
        if (NewPassword.isEmpty()||NewPassword.length()<6){
            newPassword.setError("Enter Valid New Password");
            valid=false;
        }
        if (ConfirmPassword.isEmpty()||ConfirmPassword.length()<6){
            confirmPassword.setError("Enter Valid Comfirm Password");
            valid=false;
        }
        if (NewPassword.isEmpty()||ConfirmPassword.isEmpty()||!NewPassword.equals(ConfirmPassword)){
            confirmPassword.setError("Confirm Password Is Not Same As  New Password");
            valid=false;
        }

        return valid;
    }

    private void Change_Password(String userid, final String currentPassword, String newPassword, final String confirmPassword, final String role) {

        RestAdapter adapter=new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
         Change_Password_Api api=adapter.create(Change_Password_Api.class);
          api.Changepassword(userid, currentPassword, newPassword, confirmPassword, new Callback<ChangePasswordData>() {
              @Override
              public void success(ChangePasswordData changePasswordData, Response response) {
                  String status=changePasswordData.getSuccess();
                  if(status.equals("1")){

                             Toast.makeText(getActivity(),""+changePasswordData.getMessage(), Toast.LENGTH_SHORT).show();
                      if (role.equals("1")){
                             Intent intent=new Intent(getActivity(), MainActivity.class);
                             startActivity(intent);
                          clearText();
                             getActivity().finish();
                      }else {
                          Intent intent = new Intent(getActivity(), Userproject_screen.class);
                          startActivity(intent);
                          clearText();
                          getActivity().finish();
                      }

                         }
                         else{
                      Toast.makeText(getActivity(), ""+changePasswordData.getMessage(), Toast.LENGTH_SHORT).show();
                  }
              }

              @Override
              public void failure(RetrofitError error) {
                  Toast.makeText(getActivity(), "Check connection -: "+error, Toast.LENGTH_SHORT).show();
              }
          });
    }

    private void FINDVIEWBYID(final View view) {
        currentPassword = (EditText) view.findViewById(R.id.currentPassword);
        newPassword = (EditText) view.findViewById(R.id.newPassword);
        confirmPassword = (EditText) view.findViewById(R.id.passwordConfirm);
        changePassword = (ImageView) view.findViewById(R.id.click_sign_change_password);
        back_arrow= (RelativeLayout) view.findViewById(R.id.back_arrow_change_password);

    }
}
