package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.Container_account_activity;
import com.phazeprojectsystems.Api.Register_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.Writereviewmodel;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NAVJOT SINGH on 12-10-2017.
 */

public class Write_a_review extends Fragment {

    private EditText txt_name,txt_message;
    private Button submit;
    private ImageView backarrow_write_review;
    private String get_name,get_email,get_message;
    private String[] LoginList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.write_a_review,container,false);
        LoginList= MySharePrafranceClass.LoginGetSharePrefrance(getActivity());
        findview(view);

        clicklis();


        return view;

    }

    private void findview(View view) {

        txt_name= (EditText) view.findViewById(R.id.txt_write_review_name);
      //  txt_email= (EditText) view.findViewById(R.id.txt_write_review_email);
        txt_message= (EditText) view.findViewById(R.id.txt_write_review_message);
        submit= (Button) view.findViewById(R.id.btn_write_review_submit);
        backarrow_write_review = (ImageView)view.findViewById(R.id.backarrow_write_review);
    }

    private void clicklis() {


        backarrow_write_review.setOnClickListener((View v)->{
            getFragmentManager().popBackStackImmediate();
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_name=txt_name.getText().toString().trim();
                get_email=LoginList[0];
                get_message=txt_message.getText().toString().trim();

            if(!validate())
            {
                Toast.makeText(getActivity(), "Please Enter all Fields", Toast.LENGTH_SHORT).show();
            }
            else{
                 writeus(get_name,get_email,get_message);
                }
/*
                if (get_name.equa`lsIgnoreCase("")||get_email.equalsIgnoreCase("")||get_message.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), "Please Enter all Fields", Toast.LENGTH_SHORT).show();
                }
                else {
                    writeus(get_name,get_email,get_message);
                }*/


                         }
        });
    }
    private boolean validate(){
        boolean valid=true;

        if (get_name.isEmpty()){
            txt_name.setError("Enter Valid  Name");
            valid=false;
        }
        if (get_message.isEmpty()){
            txt_message.setError("Enter Valid Message");
        }
        return valid;
    }
    private void writeus(String get_name, String get_email, String get_message) {

        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Register_Api api=rest.create(Register_Api.class);

        api.writereview(get_name, get_email, get_message, new Callback<Writereviewmodel>() {


            @Override
            public void success(Writereviewmodel writereviewmodel, Response response) {

                if (writereviewmodel.getSuccess()==1)
                {
                    Intent intent=new Intent(getActivity(),Container_account_activity.class);
                    startActivity(intent);

                }
                else {

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
