package com.phazeprojectsystems.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phazeprojectsystems.Adapter.AllSeletedMemveListAdapter;
import com.phazeprojectsystems.Adapter.MemberListAdapter;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompanyData;
import com.phazeprojectsystems.Model.CompanyDataResult;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ravi on 9/25/2017.
 */

public class Team_list_fragment extends Fragment {
    private RecyclerView menberListView;
    private ImageView crossImage;
    private AllSeletedMemveListAdapter memberListAdapter;
    private List<CompanyDataResult> menmber = new ArrayList<>();
    private android.support.v7.widget.SearchView searchView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.team_list_fragment,container,false);
        String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        UserByCompany(list[2]);
        ViewById(view);
        ClickAble(view);
        return view;
    }

    private void ClickAble(View view) {
        crossImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPress();
            }
        });
    }

    private void onBackPress() {
        getFragmentManager().popBackStack();
        // getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void ViewById(View view) {
        menberListView=(RecyclerView)view.findViewById(R.id.memberList_of_task);
        searchView=(android.support.v7.widget.SearchView)view.findViewById(R.id.member_list_seach_view);
        crossImage=(ImageView)view.findViewById(R.id.cross_Team_list_fragment);
    }
    private void UserByCompany(String company){
        RestAdapter restAdapter= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        User_by_Company_Api api = restAdapter.create(User_by_Company_Api.class);
         api.UserbyCompany(company, new Callback<CompanyData>() {
             @Override
             public void success(CompanyData companyData, Response response) {
                 if(companyData.getSuccess()==1){
                     menmber=companyData.getResult();
                     RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(getActivity());
                     menberListView.setLayoutManager(mlayoutManeger);
                     memberListAdapter= new AllSeletedMemveListAdapter(getActivity(),menmber);
                     menberListView.setAdapter(memberListAdapter);
                     memberListAdapter.notifyDataSetChanged();
                 }
                 else{
                     Constant.toast(getActivity()," Have No Employ");
                 }
             }

             @Override
             public void failure(RetrofitError error) {
                       Constant.toast(getActivity(),"Check Connection plz");
             }
         });
    }
}
