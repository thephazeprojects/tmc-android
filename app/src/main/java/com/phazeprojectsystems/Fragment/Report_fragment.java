package com.phazeprojectsystems.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phazeprojectsystems.R;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Report_fragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.report_fragment,container,false);

        return view;
    }
}
