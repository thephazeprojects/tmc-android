package com.phazeprojectsystems.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.phazeprojectsystems.R;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class About_fragment  extends Fragment{

    private RelativeLayout backarrow;
    private TextView abouttext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.about_fragment,container,false);

        find(view);
        click();
        setData();


        return view;
    }

    private void setData() {
        abouttext.setText("TMC is designed from the ground-up to focus specifically on the needs of a trades contractor with 3 to 15 employees.\n" +"It offers flexibility and transparency that allow for better project collaboration and eases communication between everyone involved. \n");
    }


    private void find(View view) {

        backarrow= (RelativeLayout) view.findViewById(R.id.back_arrow_about);
        abouttext=(TextView)view.findViewById(R.id.abouttext);
    }
    private void click() {

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }
}
