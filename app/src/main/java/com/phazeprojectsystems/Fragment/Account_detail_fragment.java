package com.phazeprojectsystems.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Container_account_activity;
import com.phazeprojectsystems.Api.GetProfilepic_Api;
import com.phazeprojectsystems.Api.UploadPic_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.GetProfilePicData;
import com.phazeprojectsystems.Model.UploadPicData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Account_detail_fragment extends Fragment {

    private RelativeLayout change_password,change_email,change_name,upload_image,about_us,write_a_review;
    private Handler handler = new Handler();
    private int status = 0;
    private ProgressDialog progressdialog;
    private static final int Capture_code=1;
    private  static final int Gallary_code=2;
    static String picturePath = "";
    final int  PIC_CROP=3;
    TypedFile UploadedFile = null;
    private  static Uri fileUri;
    private TextView editProfile,Titlename ,actualUsername, actualEmail;
    private RelativeLayout backarroweditprofile;
    private  static String mCurrentPath;
    private File CameraFile;
    private static final String  IMAGE_DIRECTORY_NAME="Camera";
    String[] list,LoginList;
    private  String userid,userChoosenTask,username,userCompany,userImage,useremail,userRole;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.account_detail_fragment,container,false);


        list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        LoginList= MySharePrafranceClass.LoginGetSharePrefrance(getActivity());


        userid=list[0];
        username=list[1];
        userCompany=list[2];
        userRole=list[4];
        useremail = LoginList[0];

        change_password= (RelativeLayout) view.findViewById(R.id.chnage_password);
        upload_image= (RelativeLayout) view.findViewById(R.id.relative_upload_image);
        change_email= (RelativeLayout) view.findViewById(R.id.change_email);
        change_name=(RelativeLayout) view.findViewById(R.id.change_name);
        editProfile=(TextView)view.findViewById(R.id.Editprofile);


        Titlename =(TextView)view.findViewById(R.id.Titlename);

        actualUsername=(TextView)view.findViewById(R.id.actualUsername);
        actualEmail=(TextView)view.findViewById(R.id.actualEmail);
        about_us= (RelativeLayout) view.findViewById(R.id.about_us);
        write_a_review= (RelativeLayout) view.findViewById(R.id.write_a_review);
        backarroweditprofile=(RelativeLayout) view.findViewById(R.id.back_arrow_edit_profile);



        actualUsername.setText(username);
        Titlename.setText(username);
        actualEmail.setText(useremail);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    Intent intent = new Intent(getActivity(),Container_account_activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        } );

        upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();

            }
        });


        backarroweditprofile.setOnClickListener((View v)->{
            Intent intent = new Intent(getActivity(),Container_account_activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        change_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Container_account_activity.showFragment(new Change_userName(),"");
            }
        });


        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new Change_password(),"");
            }
        });

        change_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Container_account_activity.showFragment(new Change_email(),"");
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new About_fragment(),"");
            }
        });

        write_a_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new Write_a_review(),"");
            }
        });


        return view;
    }


    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.phazeprojectsystems.Constant.Utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = FileProvider.getUriForFile(getActivity(),
                "com.phazeprojectsystems.FileProvider",
                getOutputMediaFile(MEDIA_TYPE_IMAGE));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);



        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {

            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        } else {
             List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
             for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }
        startActivityForResult(intent, Capture_code);
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Gallary_code);
    }


    public static String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == Capture_code) {

            if (resultCode == RESULT_OK) {

                Uri piture = Uri.parse(picturePath);
                 performCrop(piture);
            }

        } else if (requestCode == Gallary_code) {
            if (resultCode == RESULT_OK) {

                Uri picuri =data.getData();
                performCrop(picuri);
            }
        }
        else if (requestCode == PIC_CROP){
            if (resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();

                Bitmap thePic = extras.getParcelable("data");


                String picture = getImageUri(getContext(),thePic).toString();

                String path = getRealPathFromURI(picture,getActivity());

                uploadImage(userid,username,userCompany,userRole,path);
            }

            else {
                uploadImage(userid,username,userCompany,userRole,picturePath);
            }

        }

    }

//===========================================================================================================
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage=data.getData();
            String[] filePathColumn={MediaStore.Images.Media.DATA};
            Cursor cursor=getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/.PhazeSystem/");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }
        File mediaFile;
        java.util.Date date = new java.util.Date();
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "IMG_" + new Timestamp(date.getTime()).toString() + ".jpg");
            picturePath=mediaFile.getAbsolutePath();

        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "VID_" + new Timestamp(date.getTime()).toString() + ".mp4");
        } else {
            return null;
        }

        return mediaFile;

    }


    private void performCrop(Uri picuri ){

        try {

        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picuri, "image/*");


        List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(cropIntent, 0);
        int size = list.size();

        if (size == 0)
        {
            Toast.makeText(getActivity(), "Can Not Find Image Crop", Toast.LENGTH_SHORT).show();
        }
        else {
                //Intent cropIntent = new Intent("com.android.camera.action.CROP");

                ResolveInfo res = list.get(0);

                Intent intent = new Intent();
                intent.setClassName(res.activityInfo.packageName, res.activityInfo.name);



                cropIntent.putExtra("crop", "true");

                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);

                cropIntent.putExtra("outputX", 256);
                cropIntent.putExtra("outputY", 256);

                cropIntent.putExtra("return-data", true);
                startActivityForResult(cropIntent, PIC_CROP);
             }
        }
         catch (ActivityNotFoundException anfe)
        {

            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast.makeText(getActivity(), "" + errorMessage, Toast.LENGTH_SHORT).show();
        }

    }

    private void GetImage(final String Userid, final String UserName, final String Companyname, final String role) {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        GetProfilepic_Api api = restAdapter.create(GetProfilepic_Api.class);
        api.GetProfilePic(Userid, new Callback<GetProfilePicData>() {
            @Override
            public void success(GetProfilePicData getProfilePicData, Response response) {



                    MySharePrafranceClass.ClearAll(getActivity());
                    String thumb = getProfilePicData.getThumb();
                    MySharePrafranceClass.Share(getActivity(), Userid, UserName, Companyname, thumb, role);
                }


            @Override
            public void failure(RetrofitError error) {
                Constant.toast(getActivity(), "Check Connection");
            }
        });
    }

    public void CreateProgressDialog()
    {
        progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(false);
        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressdialog.setCancelable(false);
        progressdialog.setMax(100);
        progressdialog.setTitle("Upload Image");
        progressdialog.show();
    }
    public void ShowProgressDialog()
    {
        status = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(status < 100){

                    status +=1;

                    try{
                        Thread.sleep(200);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            progressdialog.setProgress(status);

                            if(status == 100){

                                progressdialog.dismiss();
                            }
                        }
                    });
                }
            }
        }).start();
    }

    private void uploadImage(final String userid, final String Username, final String Company, final String Role,String picturePath) {

        if (picturePath != null) {
            UploadedFile = new TypedFile("image/*", new File(String.valueOf(picturePath)));
        }
           CreateProgressDialog();
           ShowProgressDialog();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        UploadPic_Api api = restAdapter.create(UploadPic_Api.class);
        api.uploadpic(userid,UploadedFile,new Callback<UploadPicData>() {
            @Override
            public void success(UploadPicData uploadPicData, Response response) {
                Integer status = uploadPicData.getSuccess();
                if(status==1){

                    GetImage(userid,Username,Company,Role);

                    Constant.toast(getActivity(),"Wait For Uploading...!");
                }
                else {
                    progressdialog.dismiss();
                    Constant.toast(getActivity(),""+uploadPicData.getMsg());}
            }

            @Override
            public void failure(RetrofitError error) {

                progressdialog.dismiss();

                Toast.makeText(getActivity(), ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
