package com.phazeprojectsystems.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.phazeprojectsystems.Activities.Container_account_activity;
import com.phazeprojectsystems.R;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Extras_fragment extends Fragment {

    LinearLayout getsupport,about,write_review;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.extras_fragment,container,false);

//        getsupport= (LinearLayout) view.findViewById(R.id.get_support);
        about= (LinearLayout) view.findViewById(R.id.about);
        write_review= (LinearLayout) view.findViewById(R.id.write_review);

//
        write_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new Write_a_review(),"");
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new About_fragment(),"");
            }

        });
        return view;
    }
}
