package com.phazeprojectsystems.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.phazeprojectsystems.Activities.Container_account_activity;
import com.phazeprojectsystems.Activities.Login_screen;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Account_fragment extends Fragment {

    LinearLayout accoutdetail, signout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.account_fragment, container, false);

        FINDVIEWBYID(view);
        CLICKLISTINER();

        return view;
    }


    private void FINDVIEWBYID(View view) {

        accoutdetail = (LinearLayout) view.findViewById(R.id.account_detail);
        signout = (LinearLayout) view.findViewById(R.id.sign_out);


    }

    private void CLICKLISTINER() {

        accoutdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Container_account_activity.showFragment(new Account_detail_fragment(), "");
            }
        });

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySharePrafranceClass.ClearAll(getActivity());
                Intent intent=new Intent(getActivity(), Login_screen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }});




    }


}



