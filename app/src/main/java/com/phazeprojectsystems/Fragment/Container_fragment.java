package com.phazeprojectsystems.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.phazeprojectsystems.R;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Container_fragment extends Fragment {

      static FragmentActivity activity;
    FrameLayout frameLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.activity_container_account_activity,container,false);

        frameLayout = (FrameLayout)view.findViewById(R.id.container_frame_account_activity);

        activity=getActivity();

        return view;
    }

}
