package com.phazeprojectsystems.Fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Adapter.AdapterNoutification;
import com.phazeprojectsystems.Adapter.AdapterOldNoutification;
import com.phazeprojectsystems.Model.Dtabase_Notification_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.NotificationData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17/1/18.
 */

public class Fragment_notification  extends DialogFragment {
    private RecyclerView notificationRecycle,newBotificationlayout;
    private RelativeLayout readnew;
    private ImageView cross;


    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager linearLayoutManager1;



    private AdapterNoutification adapterNoutification;
    private AdapterOldNoutification adapterOldNoutification;


    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();

    private List<Dtabase_Notification_modle> newnotificationlist = new ArrayList<>();





    public Fragment_notification(List<Dtabase_Notification_modle>notificationlist,List<Dtabase_Notification_modle> newnotificationlist){
        super();
        this.notificationlist=notificationlist;
        this.newnotificationlist=newnotificationlist;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment_out,container);
        findView(view);
        onclick();
        adapterNoutification.notifyDataSetChanged();
        adapterOldNoutification.notifyDataSetChanged();
        if(notificationlist!=null){
            if (notificationlist.isEmpty()){
                readnew.setVisibility(View.GONE);
            }
        }


        NewreadFromDatabaseAndEnterOld();


        return view;
    }

    private void onclick() {
        cross.setOnClickListener(v->{

            this.getDialog().dismiss();
        });
    }

    private void SetOldNotificationLayout() {

        linearLayoutManager1= new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        notificationRecycle.setLayoutManager(linearLayoutManager1);
        adapterOldNoutification = new AdapterOldNoutification(this.getActivity(),newnotificationlist);
        notificationRecycle.setAdapter(adapterOldNoutification);
        adapterOldNoutification.notifyDataSetChanged();



        ItemTouchHelper.SimpleCallback simpleCallback =

                new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                Integer id =  newnotificationlist.get(viewHolder.getAdapterPosition()).getId();
                NotificationData notificationData = new NotificationData(getContext());
                SQLiteDatabase database=notificationData.getWritableDatabase();
                notificationData.Deletenotification(database,id);


                newnotificationlist.remove(viewHolder.getAdapterPosition());
                adapterOldNoutification.notifyItemChanged(viewHolder.getAdapterPosition());
                adapterOldNoutification.notifyItemRangeChanged(viewHolder.getAdapterPosition(),newnotificationlist.size());
                adapterOldNoutification.notifyDataSetChanged();



                Toast.makeText(getContext(), "Swipe Item", Toast.LENGTH_SHORT).show();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);

        itemTouchHelper.attachToRecyclerView(notificationRecycle);

    }


    private void findView(View view)
    {
        cross =(ImageView)view.findViewById(R.id.cross);
        notificationRecycle = (RecyclerView) view.findViewById(R.id.notification_dialog_recycle);
        newBotificationlayout = (RecyclerView) view.findViewById(R.id.oldnotification_dialog_recycle);
        readnew =(RelativeLayout) view.findViewById(R.id.newBotificationlayout);


        SetOldNotificationLayout();
        setnewNotificationLayout();


    }


    private void setnewNotificationLayout(){


    linearLayoutManager= new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
    newBotificationlayout.setLayoutManager(linearLayoutManager);
    adapterNoutification= new AdapterNoutification(this.getActivity(),notificationlist);
    newBotificationlayout.setAdapter(adapterNoutification);
    adapterNoutification.notifyDataSetChanged();

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            Integer id =  notificationlist.get(viewHolder.getAdapterPosition()).getId();


            NotificationData notificationData = new NotificationData(getContext());
            SQLiteDatabase database=notificationData.getWritableDatabase();
            notificationData.NewDeletenotification(database,id);


            notificationlist.remove(viewHolder.getAdapterPosition());
            adapterNoutification.notifyItemChanged(viewHolder.getAdapterPosition());
            adapterNoutification.notifyItemRangeChanged(viewHolder.getAdapterPosition(), notificationlist.size());
            adapterNoutification.notifyDataSetChanged();



            Toast.makeText(getContext(), "Swipe Item", Toast.LENGTH_SHORT).show();
        }
    };

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);

    itemTouchHelper.attachToRecyclerView(newBotificationlayout);

}




    private void NewreadFromDatabaseAndEnterOld(){

        NotificationData notificationData = new NotificationData(this.getActivity());

        SQLiteDatabase database = notificationData.getWritableDatabase();

        Cursor cursor = notificationData.NewGetNotification(database);

        if (cursor.getCount()>0){

            newnotificationlist.clear();

            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex(NotificationData.NEWCOLUMN_id));
                String msg=cursor.getString(cursor.getColumnIndex(NotificationData.NEWCOLUMN_MSG));
                String time=cursor.getString(cursor.getColumnIndex(NotificationData.NEWTIMEOFNOTIFICATION));

                String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskname));
                String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetduwdate));
                String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetdescription));


                String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskid));
                String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetaddtag));



                notificationData.AddNotification(database,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag);




           //     newnotificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

            }

        }

        deleteDataFromNewDataBase();

        notificationData.close();



    }

    private void deleteDataFromNewDataBase() {

        NotificationData notificationData = new NotificationData(this.getActivity());

        SQLiteDatabase database = notificationData.getWritableDatabase();

        notificationData.RemoveNewNotification(database);
    }
}
