package com.phazeprojectsystems.Constant;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by user on 19/3/18.
 */

public class Network_check {

    public static Boolean isNetWorking(Context context) {
        try {
            ConnectivityManager ConMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = ConMgr.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {

                return true;
            }
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
