package com.phazeprojectsystems.Constant;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.phazeprojectsystems.R;

import java.util.Calendar;

/**
 * Created by android on 10/4/17.
 */

public class FragmentDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    String date;
    View view;
    TextView due_date;
    public FragmentDate(View v){

        due_date=(TextView)v.findViewById(R.id.text_edittext_date);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),this, yy, mm, dd);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int yy, int mm, int dd) {
        int d =dd;
        int m=mm+1;
        int y=yy;
        setDate(d,m,y);
    }
    public void setDate(int day, int month, int year){
        date = day+"/"+month+"/"+year ;
        due_date.setText(date);
    }

    public String getDate() {
        return date;
    }
}
