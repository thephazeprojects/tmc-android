package com.phazeprojectsystems.Constant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.phazeprojectsystems.Activities.Login_screen;
/**
 * Created by Ravi on 8/16/2017.
 */

public class MySessionManager extends Login_screen implements GoogleApiClient.OnConnectionFailedListener {

   private  Context context;
   public static GoogleApiClient mGoogleApiClient;
    public MySessionManager (Context context){
        this.context=context;
        GoogleSignInOptions signOption = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

         mGoogleApiClient = new GoogleApiClient.Builder(context).addApi(Auth.GOOGLE_SIGN_IN_API,signOption).build();
         //mGoogleApiClient.connect();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(context, "connection fail", Toast.LENGTH_SHORT).show();
    }

}
