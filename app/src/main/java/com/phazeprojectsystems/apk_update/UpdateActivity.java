package com.phazeprojectsystems.apk_update;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.phazeprojectsystems.R;
import com.phazeprojectsystems.l;

import java.util.Observable;
import java.util.Observer;

public class UpdateActivity extends AppCompatActivity implements Observer{

	private AutoUpdateApk aua;
	private MustUpdateDialog updateDialog;
	private LinearLayout viewOnDialog;
	private RelativeLayout backg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_main);
		backg=(RelativeLayout)findViewById(R.id.backg);
		viewOnDialog=new LinearLayout(this);
		LinearLayout.inflate(this, R.layout.update_dialog, viewOnDialog);
		aua = new AutoUpdateApk(this/*getApplicationContext()*/);	// <-- don't forget to instantiate

		aua.addObserver(this);	
		aua.setUpdateInterval(AutoUpdateApk.MINUTES*61);
		AutoUpdateApk.enableMobileUpdates();
		aua.checkUpdatesManually();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override	
	protected Dialog onCreateDialog(int id) {
		switch(id){
		
		case 1:	return updateDialog;
		
		}
	
			
		 return null;
	}
    
	@Override
	public void update(Observable observable, Object data) {
		if( ((String)data).equalsIgnoreCase(AutoUpdateApk.AUTOUPDATE_GOT_UPDATE) ) {
			android.util.Log.i("AutoUpdateApkActivity", "Have just received update!");
		}
		if( ((String)data).equalsIgnoreCase(AutoUpdateApk.AUTOUPDATE_HAVE_UPDATE) ) {
			android.util.Log.i("AutoUpdateApkActivity", "There's an update2 available!");
		}
	}

	public void showUpdateNesseseryDialog() {
		updateDialog=new MustUpdateDialog(this);
		LinearLayout ll= (LinearLayout) viewOnDialog.findViewById(R.id.dialog_layout);
		ll.setMinimumWidth(500);;
		viewOnDialog.setMinimumHeight((int) (backg.getHeight()*0.5f));
		updateDialog .setContentView(viewOnDialog);
		 
		 showDialog(1); 
	}

	public void sound(int mode) {
		 int soundAddress;
		soundAddress=0;
		switch(mode){
		case 1:;
		case 3: ;
		case 2: soundAddress=R.raw.shshelk ;break;
		
		}
		
			final int u=soundAddress;
			
		new Thread(new Runnable(){ 
			 private MediaPlayer mp;
			 public void run(){ 
				mp=MediaPlayer.create(UpdateActivity.this ,u);
			 localSound(mp); 
			}
			 private void localSound(MediaPlayer mdp) {
				 synchronized(mdp){
					  mdp.start();
					l.pause(700);
					 if(mdp!=null){
						 mdp.stop();
						 mdp.release();
					 }

					
					 }
				 mdp=null;
			}
			 }).start();
	}
}
