package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.Create_task;
import com.phazeprojectsystems.Activities.Edit_task;
import com.phazeprojectsystems.Api.Edittask_api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Fragment.Task_Give_With_Project_fragment;
import com.phazeprojectsystems.Model.UnAssignModle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 13/2/18.
 */

public class AssignToAdapter extends RecyclerView.Adapter<AssignToAdapter.MyViewHolder>{
    private Activity activity;
    private List<String> MemberNAme = new ArrayList<>();
    Map<String,String> TaskMember =  new HashMap<>();
    private String[] list;
    private String Action;

    public AssignToAdapter(Activity activity,List<String> MemberNAme,String Action){
        this.activity=activity;
        this.MemberNAme=MemberNAme;
        this.Action=Action;
        list= MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.availableassignlist, parent, false);

        return new AssignToAdapter.MyViewHolder (itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String data = MemberNAme.get(position);
        holder.text.setText(data);
        holder.croseImage.setOnClickListener(v-> {

            if (Action.equals("EditTask")) {

                String adminid  = list[0];
                String Taskid = Edit_task.edit_task.getGet_taskid();

                String id  = Edit_task.TaskMember.get(data);

                unAssigned(id,Taskid,adminid);

                Edit_task.TaskMember.remove(data);



            }
            if (Action.equals("CreateTask")) {



                String id  = Create_task.TaskMember.get(data);
                Toast.makeText(activity, "   "+id, Toast.LENGTH_SHORT).show();
                Create_task.TaskMember.remove(data);

            }
            if (Action.equals("Task With Project")) {



                String id  = Task_Give_With_Project_fragment.TaskMember.get(data);
                Toast.makeText(activity, "   "+id, Toast.LENGTH_SHORT).show();
                Task_Give_With_Project_fragment.TaskMember.remove(data);

            }

            removeAt(position);

            notifyDataSetChanged();
        }) ;

    }

    private void unAssigned(String userid ,String  Taskid, String adminUserid){
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Edittask_api api = rest.create(Edittask_api.class);
        api.unassignedtask(userid, Taskid, adminUserid, new Callback<UnAssignModle>() {
            @Override
            public void success(UnAssignModle unAssignModle, Response response) {
                if (unAssignModle.getSuccess().equals("1")) {
                    Toast.makeText(activity, ""+ unAssignModle.getResult(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(activity, ""+error , Toast.LENGTH_SHORT).show();
            }

        });
    }
    @Override
    public int getItemCount() {
        return MemberNAme.size();
    }
    public void removeAt(int position)
    {
        MemberNAme.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, MemberNAme.size());
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView text;
        private ImageView croseImage;
        public MyViewHolder(View itemView) {

             super(itemView);

             text=(TextView)itemView.findViewById(R.id.text);
             croseImage=(ImageView)itemView.findViewById(R.id.croseImage);
        }
    }
}
