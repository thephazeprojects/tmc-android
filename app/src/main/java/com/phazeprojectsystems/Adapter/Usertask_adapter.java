package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.User_comment_screen;
import com.phazeprojectsystems.Model.Usertasklis;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NAVJOT SINGH on 26-10-2017.
 */

public class Usertask_adapter extends RecyclerView.Adapter<Usertask_adapter.MyViewHolder> {
    List<Usertasklis> tasklist = new ArrayList<>();
    private Activity activity;
    private String get_add_tag;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView TaskTextView;
        ImageView tagimage;

        MyViewHolder(View itemView) {
            super(itemView);
            TaskTextView = (TextView) itemView.findViewById(R.id.Taskname_inList);
            tagimage = (ImageView) itemView.findViewById(R.id.tag_image);
        }
    }

    public Usertask_adapter(Activity activity, List<Usertasklis> tasklist) {
        this.activity = activity;
        this.tasklist = tasklist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_task_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Usertasklis taskResult = tasklist.get(position);
        holder.TaskTextView.setText(taskResult.getTaskName());

        get_add_tag = taskResult.getAddTag();
        switch (get_add_tag) {
            case "1":
                holder.tagimage.setImageResource(R.drawable.red1_circle);
                break;
            case "2":
                holder.tagimage.setImageResource(R.drawable.yellow_circle);
                break;
            case "3":
                holder.tagimage.setImageResource(R.drawable.green_circle);
                break;
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String gettaskname = taskResult.getTaskName();
                String getduwdate = taskResult.getDueDate();
                String getdescription = taskResult.getDescription();
                String taskid = taskResult.getTaskId();
                String add_tag = taskResult.getAddTag();
                String getprojectid=taskResult.getProjectId();

                MySharePrafranceClass.ProjectShare(activity,getprojectid,"");


                Intent intent = new Intent(holder.itemView.getContext(), User_comment_screen.class);
                intent.putExtra("gettaskname", gettaskname);
                intent.putExtra("getdewdate", getduwdate);
                intent.putExtra("getdescription", getdescription);
                intent.putExtra("gettaskid", taskid);
                intent.putExtra("getaddtag", add_tag);
                intent.putExtra("getprojectid", getprojectid);
                intent.putExtra("comingintent", "other");
                holder.itemView.getContext().startActivity(intent);

            }
        });

        YoYo.with(Techniques.Landing).playOn(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return tasklist.size();
    }
}
