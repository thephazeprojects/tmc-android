package com.phazeprojectsystems.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Activities.Task_activity;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompleteProject;
import com.phazeprojectsystems.Model.DeleteProjectData;
import com.phazeprojectsystems.Model.DeleteProject_Result_Model;
import com.phazeprojectsystems.Model.InCompleteProjectResult;
import com.phazeprojectsystems.Model.PermanentDeleteModle;
import com.phazeprojectsystems.Model.RestoreData_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 21/2/18.
 */

public class DeleteProjectListAdapter  extends RecyclerView.Adapter<DeleteProjectListAdapter.MyViewHolder> {
    private List<DeleteProject_Result_Model> projectDelete;
    private MainActivity mainActivity;

    public DeleteProjectListAdapter(MainActivity mainActivity, List<DeleteProject_Result_Model> projectDelete) {
        this.mainActivity=mainActivity;
        this.projectDelete =projectDelete;
    }

    @Override
    public DeleteProjectListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);
        return new DeleteProjectListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeleteProjectListAdapter.MyViewHolder holder, int position) {
        final DeleteProject_Result_Model listdata = projectDelete.get(position);


        holder.ProjectTextView.setText(listdata.getProjact_name());

        holder.projectTaskIncompleteCount.setVisibility(View.GONE);

        YoYo.with(Techniques.Landing).playOn(holder.itemView);




        holder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(mainActivity, holder.img_icon);
                popupMenu.getMenuInflater().inflate(R.menu.projectlistmenu, popupMenu.getMenu());
                popupMenu.getMenu().getItem(0).setVisible(false);
                popupMenu.getMenu().getItem(1).setVisible(false);
                popupMenu.getMenu().getItem(2).setVisible(false);



                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delete_project:
                                break;
                            case R.id.Complete_project:
                                break;
                            case R.id.Incomplete_project:
                                break;
                            case R.id.Parmanent_delete:

                                final String projectid = listdata.getProject_id();
                                AlertDialog.Builder dialog = new AlertDialog.Builder(mainActivity);
                                dialog.setCancelable(false);
                                dialog.setTitle("Project Delete");
                                dialog.setMessage("Are you sure you want to Delete this Project Permanent  ?" );
                                dialog.setPositiveButton("Permanent Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {


                                        RestAdapter rest1 = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                        Get_User_Project Api = rest1.create(Get_User_Project.class);
                                        Api.PermanentDelete(projectid, new Callback<PermanentDeleteModle>() {
                                            @Override
                                            public void success(PermanentDeleteModle permanentDeleteModle, Response response) {

                                                String status = permanentDeleteModle.getSuccess();
                                        if (status.equalsIgnoreCase("1")) {

                                            Toast.makeText(mainActivity, "Project Deleted Permanent", Toast.LENGTH_SHORT).show();

                                            removeAt(position);

                                            notifyDataSetChanged();
                                        }


                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                                Toast.makeText(mainActivity, "Failuree" + error, Toast.LENGTH_SHORT).show();

                                            }
                                        });
                                    }
                                })
                                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                                final AlertDialog alert = dialog.create();
                                alert.show();

                                break;


                            case R.id.Restore_delete:


                                final String projectid1 = listdata.getProject_id();
                                AlertDialog.Builder dialog1 = new AlertDialog.Builder(mainActivity);
                                dialog1.setCancelable(false);
                                dialog1.setTitle("Project Restore");
                                dialog1.setMessage("Are you sure you want to Restore this Project  ?" );
                                dialog1.setPositiveButton("Restore Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {


                                        RestAdapter rest1 = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                        Get_User_Project Api = rest1.create(Get_User_Project.class);
                                        Api.RestoreProject(projectid1, new Callback<RestoreData_modle>() {
                                            @Override
                                            public void success(RestoreData_modle restoreData_modle, Response response) {
                                                if(restoreData_modle.getSuccess().equals("1")){

                                                    Toast.makeText(mainActivity, "Project Restore SucessFully ", Toast.LENGTH_SHORT).show();

                                                    removeAt(position);

                                                }
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(mainActivity, "Failuree" + error, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                })
                                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                                final AlertDialog alert1 = dialog1.create();
                                alert1.show();
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectDelete.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ProjectTextView;
        Button projectTaskIncompleteCount;
        RelativeLayout img_icon;
        public Context context;
        public MyViewHolder(View itemView) {
            super(itemView);
            ProjectTextView = (TextView) itemView.findViewById(R.id.projectname_inList);
            img_icon = (RelativeLayout) itemView.findViewById(R.id.relative_option_icon_deleteproject);
            projectTaskIncompleteCount = (Button)itemView.findViewById(R.id.projectTaskIncompleteCount);
            context = itemView.getContext();
        }
    }
    public void removeAt(int position) {
        projectDelete.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, projectDelete.size());
    }
}
