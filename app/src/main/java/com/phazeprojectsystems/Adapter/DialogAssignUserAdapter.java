package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Api.DeleteTask_Api;
import com.phazeprojectsystems.Api.Edittask_api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.DeleteTaskData;
import com.phazeprojectsystems.Model.GetAssigneUser_Result_modle;
import java.util.List;

import com.phazeprojectsystems.Model.UnAssignModle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 19/2/18.
 */

public class DialogAssignUserAdapter  extends RecyclerView.Adapter<DialogAssignUserAdapter.MyViewHolder>  {
    private Activity activity;
    private List<GetAssigneUser_Result_modle> assigneuser;
    private String[] list;
    private String active;

    public DialogAssignUserAdapter(Activity activity,List<GetAssigneUser_Result_modle> assigneuser, String active){
        this.activity=activity;
        this.assigneuser=assigneuser;
        this.active=active;
        list = MySharePrafranceClass.GetSharePrefrance(activity);

    }

    @Override
    public DialogAssignUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userlist, parent, false);

        return new DialogAssignUserAdapter.MyViewHolder (itemView);
    }

    @Override
    public void onBindViewHolder(DialogAssignUserAdapter.MyViewHolder holder, int position) {
        GetAssigneUser_Result_modle data = assigneuser.get(position);

        if (data!=null) {

            String image = data.getThumb();
            String Name = data.getUsername();
            holder.AssigneeuserName.setText(Name);


            if (image!=null) {
                String menberimage = Constant.picuri.concat(image);
                Glide.with(activity).load(menberimage).into(holder.AssigneeUserImage);
            }
            else {
                Glide.with(activity).load(R.drawable.user_icn_grey).into(holder.AssigneeUserImage);
            }


            holder.relative_option_iconofAssignee.setOnClickListener(v-> {



                if (active.equals("CommentScreen")){

                String taskid = Comment_screen.comment_screen.gettaskid;
                String Userid = data.getId();
                String admin = list[0];

                PopupMenu popupMenu = new PopupMenu(activity, holder.relative_option_iconofAssignee);
                popupMenu.getMenuInflater().inflate(R.menu.task_menu, popupMenu.getMenu());
                    popupMenu.getMenu().getItem(1).setVisible(false);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.delete_task:


                                unAssigned(Userid,taskid, admin, position);

                                break;

                            case R.id.add_task:



                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();

            }else {

                     String taskid=active;
                     String Userid = data.getId();
                     String admin = list[0];
                    PopupMenu popupMenu = new PopupMenu(activity, holder.relative_option_iconofAssignee);
                    popupMenu.getMenuInflater().inflate(R.menu.task_menu, popupMenu.getMenu());
                    popupMenu.getMenu().getItem(1).setVisible(false);

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {

                                case R.id.delete_task:


                                    unAssigned(Userid,taskid, admin, position);

                                    break;
                            }
                            return false;
                        }
                    });

                    popupMenu.show();

                }

            });


        }


    }

    private void unAssigned(String userid ,String  Taskid, String adminUserid , int position){
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Edittask_api api = rest.create(Edittask_api.class);
        api.unassignedtask(userid, Taskid, adminUserid, new Callback<UnAssignModle>() {
            @Override
            public void success(UnAssignModle unAssignModle, Response response) {
                if (unAssignModle.getSuccess().equals("1")) {
                    removeAt(position);
                    Toast.makeText(activity, ""+ unAssignModle.getResult(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(activity, ""+error , Toast.LENGTH_SHORT).show();

            }

        });
    }
    public void removeAt(int position) {
        assigneuser.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, assigneuser.size());
    }
    @Override
    public int getItemCount() {
        return assigneuser.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView AssigneeuserName;
        CircleImageView AssigneeUserImage;
        RelativeLayout relative_option_iconofAssignee;
        public MyViewHolder(View itemView) {
            super(itemView);
            AssigneeuserName = (TextView)itemView.findViewById(R.id.AssigneeuserName);
            AssigneeUserImage =(CircleImageView)itemView.findViewById(R.id.AssigneeUserImage);
            relative_option_iconofAssignee=(RelativeLayout)itemView.findViewById(R.id.relative_option_iconofAssignee);
        }
    }
}
