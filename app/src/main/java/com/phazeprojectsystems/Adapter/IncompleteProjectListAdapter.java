package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.Task_activity;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Api.Project_Incomplete_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompleteProject;
import com.phazeprojectsystems.Model.CompleteProjectResult;
import com.phazeprojectsystems.Model.DeleteProjectData;
import com.phazeprojectsystems.Model.GetInCompleteProjectTask__result;
import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;
import com.phazeprojectsystems.Model.InCompleteProjectResult;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by user on 5/1/18.
 */

public class IncompleteProjectListAdapter  extends RecyclerView.Adapter<IncompleteProjectListAdapter.MyViewHolder>  {
    private List<InCompleteProjectResult> projectincomplete;
    private Activity activity;

    public IncompleteProjectListAdapter(Activity activity, List<InCompleteProjectResult> projectincomplete) {
        this.activity = activity;
        this.projectincomplete = projectincomplete;
    }

    @Override
    public IncompleteProjectListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(IncompleteProjectListAdapter.MyViewHolder holder, int position) {
        final InCompleteProjectResult listdata = projectincomplete.get(position);
        holder.ProjectTextView.setText(listdata.getProjactName());

        String projectid = listdata.getProjectId();


        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Project_Incomplete_Task_Api api =rest.create(Project_Incomplete_Task_Api.class);

        api.getProjectAllIncompleteTask(projectid, new Callback<Get_In_complete_Project_task_modle>() {
            @Override
            public void success(Get_In_complete_Project_task_modle get_in_complete_project_task_modle, Response response) {
                List<GetInCompleteProjectTask__result> incompletetask=get_in_complete_project_task_modle.getResult();

                if (incompletetask!=null) {
                    holder.projectTaskIncompleteCount.setText(Integer.toString(incompletetask.size()));
                }else {
                    holder.projectTaskIncompleteCount.setText(0);
                }

            }
            @Override
            public void failure(RetrofitError error) {

            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), Task_activity.class);
                MySharePrafranceClass.ProjectShare(holder.itemView.getContext(), projectincomplete.get(position).getProjectId(),
                        projectincomplete.get(position).getProjactName());

                holder.itemView.getContext().startActivity(intent);
            }
        });

        YoYo.with(Techniques.Landing).playOn(holder.itemView);


        holder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(activity, holder.img_icon);
                popupMenu.getMenuInflater().inflate(R.menu.projectlistmenu, popupMenu.getMenu());
                popupMenu.getMenu().getItem(2).setVisible(false);
                popupMenu.getMenu().getItem(3).setVisible(false);
                popupMenu.getMenu().getItem(4).setVisible(false);


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delete_project:

//                                  model2=data.get(position);

                                final String projectid = listdata.getProjectId();

                                RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                DeleteProject_Api api = rest.create(DeleteProject_Api.class);

                                api.DeleteProject(projectid, new Callback<DeleteProjectData>() {
                                    @Override
                                    public void success(DeleteProjectData deleteProjectData, Response response) {

                                        String status = deleteProjectData.getSuccess();
                                        if (status.equalsIgnoreCase("1")) {
                                            Toast.makeText(activity, "Project Deleted", Toast.LENGTH_SHORT).show();

                                            projectincomplete.remove(position);
                                            notifyDataSetChanged();
                                        }

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                        Toast.makeText(activity, "Failuree" + error, Toast.LENGTH_SHORT).show();
                                    }
                                });

                                break;

                            case R.id.Complete_project:

                                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                dialog.setCancelable(false);
                                dialog.setTitle("Project Complete");
                                dialog.setMessage("Are you sure you want to Complete this Project ?" );
                                dialog.setPositiveButton("Complete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {


                                        RestAdapter rest1 = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                        Get_User_Project Api = rest1.create(Get_User_Project.class);
                                        Api.CompleteProject(listdata.getUserId(), listdata.getProjectId(), new Callback<CompleteProject>() {
                                            @Override
                                            public void success(CompleteProject completeProject, Response response) {
                                                removeAt(position);

                                                notifyDataSetChanged();
                                                Toast.makeText(activity, ""+completeProject.getResult(), Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(activity, ""+error, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                })
                                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                                final AlertDialog alert = dialog.create();
                                alert.show();

                                break;

                            case R.id.Incomplete_project:
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectincomplete.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        Button projectTaskIncompleteCount;
        TextView ProjectTextView;
        RelativeLayout img_icon;
        public Context context;
        public MyViewHolder(View itemView) {
            super(itemView);
            ProjectTextView = (TextView) itemView.findViewById(R.id.projectname_inList);
              img_icon = (RelativeLayout) itemView.findViewById(R.id.relative_option_icon_deleteproject);
             projectTaskIncompleteCount = (Button) itemView.findViewById(R.id.projectTaskIncompleteCount);
             context = itemView.getContext();
        }
    }
    public void removeAt(int position) {
        projectincomplete.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, projectincomplete.size());
    }


    private void  projectInCompleteTask(String projectid) {

    }
}
