package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Select_team_menber;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.AssigneeByTask_Result_model;
import com.phazeprojectsystems.Model.CompanyDataResult;
import com.phazeprojectsystems.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ravi on 9/25/2017.
 */

public class MemberListAdapter extends RecyclerView.Adapter<MemberListAdapter.MyViewHolder> {
    private List<AssigneeByTask_Result_model> memberlist = new ArrayList<>();
    private  Activity activity;

    private Map<String,String> SelectedMember =  new HashMap<>();
    String s=null;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.member_list, parent, false);
        return new MyViewHolder(itemView);
    }

   public MemberListAdapter(Activity activity,List<AssigneeByTask_Result_model> memberlist){
        this.activity=activity;
        this.memberlist=memberlist;
        SelectedMember.clear();
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final AssigneeByTask_Result_model companymember=memberlist.get(position);
        holder.itemView.setBackgroundColor(companymember.isSelected() ? Color.CYAN : Color.WHITE);
        holder.txt_membername.setText(companymember.getUsername());
        s = companymember.getUserimage();

        if (s!=null) {

            String menberimage = Constant.picuri.concat(s);
            Glide.with(activity).load(menberimage).into(holder.img_Member);
        }
        else {
            Glide.with(activity).load(R.drawable.user_icn_grey).into(holder.img_Member);
        }

        companymember.getUserimage();

        holder.Assied_User_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companymember.setSelected(!companymember.isSelected());

                if (companymember.isSelected()){
                    companymember.setSelected(true);

                    String Userd=companymember.getId();
                    holder.addButton.setText("Remove");
                    Select_team_menber.SelectedMember.put(holder.txt_membername.getText().toString(),Userd);


                    int size =  Select_team_menber.SelectedMember.size();
                    Select_team_menber.followers_value.setText(String.valueOf(size));
                }
                else{
                    Select_team_menber.SelectedMember.remove(holder.txt_membername.getText().toString());
                    companymember.setSelected(false);
                    holder.addButton.setText("Add");

                    int size =  Select_team_menber.SelectedMember.size();
                    Select_team_menber.followers_value.setText(String.valueOf(size));
                }
            }
        });

        holder.txt_membername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

    }

    @Override
    public int getItemCount() {
        return memberlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txt_membername;
        public CircleImageView img_Member;
        public RelativeLayout Assied_User_button;
        public TextView addButton;
        public  RecyclerView view;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_membername=(TextView)itemView.findViewById(R.id.Membername_inList);
            img_Member=(CircleImageView)itemView.findViewById(R.id.img_member_image);
            view=(RecyclerView)itemView.findViewById(R.id.memberList);
            Assied_User_button=(RelativeLayout) itemView.findViewById(R.id.Assied_User_button);
            addButton=(TextView) itemView.findViewById(R.id.addButton);
        }
    }
}
