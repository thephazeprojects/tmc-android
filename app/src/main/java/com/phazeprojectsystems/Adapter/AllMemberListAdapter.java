package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Select_team_menber;
import com.phazeprojectsystems.Activities.Task_activity;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompanyDataResult;
import com.phazeprojectsystems.Model.DeleteMemberUser_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 1/2/18.
 */

public class AllMemberListAdapter extends RecyclerView.Adapter<AllMemberListAdapter.MyViewHolder> {
    private List<CompanyDataResult> memberlist=new ArrayList<>();
    private Activity activity;
    private Map<String,String> SelectedMember =  new HashMap<>();
    String s=null;


    public AllMemberListAdapter(Activity activity,List<CompanyDataResult> memberlist){
        this.activity=activity;
        this.memberlist=memberlist;
        SelectedMember.clear();
    }

    @Override
    public AllMemberListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_menber_list, parent, false);
        return new AllMemberListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AllMemberListAdapter.MyViewHolder holder, int position) {
        final CompanyDataResult companymember = memberlist.get(position);
        holder.itemView.setBackgroundColor(companymember.isSelected() ? Color.CYAN : Color.WHITE);
        holder.txt_membername.setText(companymember.getUsername());


        s = companymember.getUserimage();

        if (s!=null) {

            String menberimage = Constant.picuri.concat(s);
            Glide.with(activity).load(menberimage).into(holder.img_Member);
        }
        else {
            Glide.with(activity).load(R.drawable.user_icn_grey).into(holder.img_Member);
        }

        companymember.getUserimage();

        holder.option_icon_deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(activity,holder.option_icon_deleteUser);
                popupMenu.getMenuInflater().inflate(R.menu.user_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.Delete_user:
                                if (companymember.getId()!=null) {
                                    deleteUser(companymember.getId(),position);
                                }
                                break;
                            default:
                                Toast.makeText(activity, "You Click on  " + item.getTitle(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return memberlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_membername;
        public CircleImageView img_Member;
        public  RelativeLayout option_icon_deleteUser;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_membername=(TextView)itemView.findViewById(R.id.Membername_inList1);
            img_Member=(CircleImageView)itemView.findViewById(R.id.img_member_image1);
            option_icon_deleteUser=(RelativeLayout)itemView.findViewById(R.id.option_icon_deleteUser);
        }
    }

    private void deleteUser(String userid, int position){
        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        User_by_Company_Api api = rest.create(User_by_Company_Api.class);
        api.DeleteCompanyUser(userid, new Callback<DeleteMemberUser_modle>() {
            @Override
            public void success(DeleteMemberUser_modle deleteMemberUser_modle, Response response) {

                removeAt(position);
                Toast.makeText(activity, ""+deleteMemberUser_modle.getResult(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(activity, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private  void removeAt(int position){
        memberlist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, memberlist.size());
    }

}
