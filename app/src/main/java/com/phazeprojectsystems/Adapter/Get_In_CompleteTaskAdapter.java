package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.User_comment_screen;
import com.phazeprojectsystems.Model.Completetasklis;
import com.phazeprojectsystems.Model.Pendingtasklis;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.List;
/**
 * Created by Ravi on 9/11/2017.
 */

public class Get_In_CompleteTaskAdapter extends RecyclerView.Adapter<Get_In_CompleteTaskAdapter.MyViewHolder>{
    private List<Pendingtasklis> getInCompleteTaskResults;
    private Activity activity;

    public Get_In_CompleteTaskAdapter(Activity activity,List<Pendingtasklis> getInCompleteTaskResults) {

        if (!getInCompleteTaskResults.isEmpty()) {

            for (int i = 0; i < getInCompleteTaskResults.size(); i++) {
                for (int j = getInCompleteTaskResults.size() - 1; j > i; j--) {
                    if (getInCompleteTaskResults.get(i).getAddTag() > getInCompleteTaskResults.get(j).getAddTag()) {
                        Pendingtasklis tmp = getInCompleteTaskResults.get(i);
                        getInCompleteTaskResults.set(i, getInCompleteTaskResults.get(j));
                        getInCompleteTaskResults.set(j, tmp);
                    }
                }
            }

        }
        this.getInCompleteTaskResults = getInCompleteTaskResults;
        this.activity=activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView=  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_task_list, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Pendingtasklis listdata = getInCompleteTaskResults.get(position);
        holder.TaskTextView.setText(listdata.getTaskName());


        switch (listdata.getAddTag()) {
            case 1 :
                holder.tagimage.setImageResource(R.drawable.red1_circle);
                break;
            case 2 :
                holder.tagimage.setImageResource(R.drawable.yellow_circle);
                break;
            case 3 :
                holder.tagimage.setImageResource(R.drawable.green_circle);
                break;
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String gettaskname = listdata.getTaskName();
                String getduwdate = listdata.getDueDate();
                String getdescription = listdata.getDescription();
                String taskid = listdata.getTaskId();
                Integer add_tag = listdata.getAddTag();
                String getprojectid=listdata.getProjectId();
                String updatetime=listdata.getUpdatetime();
                MySharePrafranceClass.ProjectShare(activity,getprojectid,"");


                Intent intent = new Intent(holder.itemView.getContext(), User_comment_screen.class);
                intent.putExtra("gettaskname", gettaskname);
                intent.putExtra("getdewdate", getduwdate);
                intent.putExtra("getdescription", getdescription);
                intent.putExtra("gettaskid", taskid);
                intent.putExtra("getaddtag",""+ add_tag);
                intent.putExtra("getprojectid", getprojectid);
                intent.putExtra("updatetime",updatetime);
                intent.putExtra("comingintent", "other");
                holder.itemView.getContext().startActivity(intent);

            }
        });

        YoYo.with(Techniques.Landing).playOn(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return getInCompleteTaskResults.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView TaskTextView;
        public ImageView tagimage;
        public MyViewHolder(final View itemView) {
            super(itemView);
            TaskTextView=(TextView)itemView.findViewById(R.id.Taskname_inList);
            tagimage = (ImageView) itemView.findViewById(R.id.tag_image);
        }
    }
}
