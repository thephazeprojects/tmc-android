package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Activities.User_comment_screen;
import com.phazeprojectsystems.Model.Dtabase_Notification_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by user on 22/2/18.
 */

public class AdapterOldNoutification extends RecyclerView.Adapter<AdapterOldNoutification.MyViewHolder> {

    private Activity activity;
    private List<Dtabase_Notification_modle> notificationlist;
    private String[] list;

    public AdapterOldNoutification(Activity activity, List<Dtabase_Notification_modle> notificationlist){
        this.activity=activity;
        this.notificationlist=notificationlist;

        list= MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list, parent, false);

        return new AdapterOldNoutification.MyViewHolder (itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Dtabase_Notification_modle database = notificationlist.get(position);
        holder.notification_Msg.setText(database.getMessege());
        holder.notification_time.setText(database.getNotificationTime());
        holder.itemView.setOnClickListener((View v)->{

            if (database.getGettaskid()!=null){
                if (!database.getGettaskid().equals("")){

                    String role = list[4];
                    if (role.equals("1")){

                        Intent intent = new Intent(holder.itemView.getContext(),Comment_screen.class);
                        intent.putExtra("gettaskname", database.getGettaskname());
                        intent.putExtra("getdewdate", database.getGetduwdate());
                        intent.putExtra("getdescription", database.getGetdescription());
                        intent.putExtra("gettaskid", database.getGettaskid());
                        intent.putExtra("getaddtag", database.getGetaddtag());
                        intent.putExtra("comingintent", "other");
                        holder.itemView.getContext().startActivity(intent);
                    }
                    else {

                        Intent intent = new Intent(holder.itemView.getContext(),User_comment_screen.class);
                        intent.putExtra("gettaskname", database.getGettaskname());
                        intent.putExtra("getdewdate", database.getGetduwdate());
                        intent.putExtra("getdescription", database.getGetdescription());
                        intent.putExtra("gettaskid", database.getGettaskid());
                        intent.putExtra("getaddtag", database.getGetaddtag());
                        intent.putExtra("comingintent", "other");
                        holder.itemView.getContext().startActivity(intent);
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView notificationImage;
        public TextView notification_Msg;
        public  TextView notification_time;
        public MyViewHolder(View itemView) {
            super(itemView);
            notification_Msg = (TextView)itemView.findViewById(R.id.notification_msg);
            notification_time = (TextView)itemView.findViewById(R.id.notification_Time);
            notificationImage = (CircleImageView) itemView.findViewById(R.id.notification_image);
        }
    }
}
