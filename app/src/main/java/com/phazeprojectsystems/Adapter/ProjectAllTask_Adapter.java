package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.DeleteTask_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.DeleteTaskData;
import com.phazeprojectsystems.Model.ProjectAllTaskData__msg;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Vishal on 9/27/2017.
 */

public class ProjectAllTask_Adapter extends RecyclerView.Adapter<ProjectAllTask_Adapter.MyViewHolder>{

    List<ProjectAllTaskData__msg> tasklist= new ArrayList<>();
    private  Activity activity;
    private String get_add_tag;
    String userid;
    private String[] list;

    public class MyViewHolder extends RecyclerView.ViewHolder{
          TextView TaskTextView,txt_status;
         ImageView tagimage,img_taskincomplete;
        RelativeLayout img_icon;
         MyViewHolder(View itemView) {
            super(itemView);
            TaskTextView=(TextView)itemView.findViewById(R.id.Taskname_inList);
            tagimage= (ImageView) itemView.findViewById(R.id.tag_image);
             img_icon= (RelativeLayout) itemView.findViewById(R.id.relative_option_icon);
             img_taskincomplete= (ImageView) itemView.findViewById(R.id.img_taskincomplete);
             txt_status= (TextView) itemView.findViewById(R.id.txt_status);
         }
    }
    public ProjectAllTask_Adapter(Activity activity, List<ProjectAllTaskData__msg> tasklist){
        this.activity=activity;
        this.tasklist=tasklist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView=  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_list, parent, false);
        return new MyViewHolder(itemView);}

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProjectAllTaskData__msg  taskResult = tasklist.get(position);
        holder.TaskTextView.setText(taskResult.getTask().getTask_name());

        get_add_tag=taskResult.getTask().getAdd_tag();
          switch (get_add_tag) {
              case "1":
                holder.tagimage.setImageResource(R.drawable.red1_circle);
                  break;
              case "2":
                  holder.tagimage.setImageResource(R.drawable.yellow_circle);
                  break;
              case "3":
                  holder.tagimage.setImageResource(R.drawable.green_circle);
                  break;
          }


          holder.tagimage.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {



                  get_add_tag=taskResult.getTask().getAdd_tag();
                  switch (get_add_tag) {
                      case "1":
                          Toast.makeText(activity, "High Priority Task", Toast.LENGTH_SHORT).show();

                          break;
                      case "2":
                          Toast.makeText(activity, "Medium Priority Task", Toast.LENGTH_SHORT).show();
                          break;
                      case "3":
                          Toast.makeText(activity, "Low Priority Task", Toast.LENGTH_SHORT).show();
                          break;
                  }


              }
          });



          holder.txt_status.setText(taskResult.getTask().getDue_date());

          holder.img_taskincomplete.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {

                  holder.img_taskincomplete.setImageResource(R.drawable.task_complete);

                  final String gettaskid=taskResult.getTask().getTask_id();
                  list = MySharePrafranceClass.GetSharePrefrance(activity);

                  userid = list[0];

                  AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                  dialog.setCancelable(false);
                  dialog.setTitle("Phaze Complete");
                  dialog.setMessage("Are you sure you want to complete this Phaze?" );
                  dialog.setPositiveButton("Complete", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int id) {

                          Calendar cal = Calendar.getInstance();
                          SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                          String time  = sdf.format(cal.getTime());


                          RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                          Complete_Api api = rest.create(Complete_Api.class);

                          api.Complete(userid, gettaskid,time,new Callback<CompleteTaskModel>() {
                              @Override
                              public void success(CompleteTaskModel completeTaskModel, Response response) {

                                  if (completeTaskModel.getSuccess().equalsIgnoreCase("1")) {

                                      notifyDataSetChanged();
                                      Toast.makeText(activity, "Task Completed", Toast.LENGTH_SHORT).show();
                                  }
                              }

                              @Override
                              public void failure(RetrofitError error) {

                                  Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
                              }
                          });


                      }
                  })
                          .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                              @Override
                              public void onClick(DialogInterface dialog, int which) {
                                  dialog.cancel();
                                  holder.img_taskincomplete.setImageResource(R.drawable.task_incomplete);
                              }
                          });

                  final AlertDialog alert = dialog.create();
                  alert.show();




              }
          });


          holder.img_icon.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {





                  PopupMenu popupMenu = new PopupMenu(activity,holder.img_icon);
                  popupMenu.getMenuInflater().inflate(R.menu.task_menu,popupMenu.getMenu());

                  popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                      @Override
                      public boolean onMenuItemClick(MenuItem item) {

                          switch (item.getItemId())
                          {

                              case R.id.delete_task:


//                                  model2=data.get(position);

                                  final String task__id=taskResult.getTask().getTask_id();

                                  RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                  DeleteTask_Api api=rest.create(DeleteTask_Api.class);


                                  api.Delete(task__id, new Callback<DeleteTaskData>() {
                                      @Override
                                      public void success(DeleteTaskData deleteTaskData, Response response) {


                                          String status=deleteTaskData.getSuccess();

                                          if (status.equalsIgnoreCase("1"))

                                          {
                                              tasklist.remove(position);
                                              notifyDataSetChanged();
                                          }


                                      }

                                      @Override
                                      public void failure(RetrofitError error) {

                                      }
                                  });

                                  break;
                          }
                          return false;
                      }
                  });

                  popupMenu.show();
              }
          });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String gettaskname=taskResult.getTask().getTask_name();
                String getduwdate=taskResult.getTask().getDue_date();
                String getdescription=taskResult.getTask().getDescription();
                String taskid=taskResult.getTask().getTask_id();
                String add_tag=taskResult.getTask().getAdd_tag();

                Intent intent=new Intent(holder.itemView.getContext(),Comment_screen.class);
                intent.putExtra("gettaskname",gettaskname);
                intent.putExtra("getdewdate",getduwdate);
                intent.putExtra("getdescription",getdescription);
                intent.putExtra("gettaskid",taskid);
                intent.putExtra("getaddtag",add_tag);
                holder.itemView.getContext().startActivity(intent);

            }
        });
    }
    @Override
    public int getItemCount() {
        return tasklist.size();
    }
}
