package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Api.Edittask_api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.AssignModle;
import com.phazeprojectsystems.Model.AssigneeByTask_Result_model;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by user on 1/3/18.
 */

public class DialogUNAssignUserAdapter  extends RecyclerView.Adapter<DialogUNAssignUserAdapter.MyViewHolder> {

    private Activity activity;
    private String[] list;
    private String active;
    private String projectid;
    List<AssigneeByTask_Result_model> unassignee;

    public DialogUNAssignUserAdapter(Activity activity, List<AssigneeByTask_Result_model> unassignee, String active, String projectid) {
        this.activity = activity;
        this.unassignee = unassignee;
        this.active = active;
        this.projectid = projectid;
        list = MySharePrafranceClass.GetSharePrefrance(activity);

    }


    @Override
    public DialogUNAssignUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userlist, parent, false);

        return new DialogUNAssignUserAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DialogUNAssignUserAdapter.MyViewHolder holder, int position) {

        AssigneeByTask_Result_model data = unassignee.get(position);

        if (data != null) {

            String image = data.getThumb();
            String Name = data.getUsername();
            holder.AssigneeuserName.setText(Name);


            if (image != null) {
                String menberimage = Constant.picuri.concat(image);
                Glide.with(activity).load(menberimage).into(holder.AssigneeUserImage);
            } else {
                Glide.with(activity).load(R.drawable.user_icn_grey).into(holder.AssigneeUserImage);
            }


            holder.relative_option_iconofAssignee.setOnClickListener(v -> {


                if (active.equals("CommentScreen")) {

                    String taskid = Comment_screen.comment_screen.gettaskid;
                    String projectid = Comment_screen.comment_screen.get_projectid;
                    String Userid = data.getId();

                    PopupMenu popupMenu = new PopupMenu(activity, holder.relative_option_iconofAssignee);
                    popupMenu.getMenuInflater().inflate(R.menu.task_menu, popupMenu.getMenu());
                    popupMenu.getMenu().getItem(0).setVisible(false);

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {

                                case R.id.delete_task:
                                    break;

                                case R.id.add_task:

                                    Assigned(Userid, taskid, projectid, position);

                                    break;
                            }
                            return false;
                        }
                    });

                    popupMenu.show();

                } else {

                    String taskid = active;
                    String Userid = data.getId();
                    String projectid = this.projectid;
                    PopupMenu popupMenu = new PopupMenu(activity, holder.relative_option_iconofAssignee);
                    popupMenu.getMenuInflater().inflate(R.menu.task_menu, popupMenu.getMenu());
                    popupMenu.getMenu().getItem(0).setVisible(false);

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {

                                case R.id.delete_task:


                                    break;

                                case R.id.add_task:

                                    Assigned(Userid, taskid, projectid, position);

                                    break;
                            }
                            return false;
                        }
                    });

                    popupMenu.show();

                }

            });
        }
    }


    @Override
    public int getItemCount() {
        return unassignee.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView AssigneeuserName;
        CircleImageView AssigneeUserImage;
        RelativeLayout relative_option_iconofAssignee;

        public MyViewHolder(View itemView) {
            super(itemView);

            AssigneeuserName = (TextView) itemView.findViewById(R.id.AssigneeuserName);
            AssigneeUserImage = (CircleImageView) itemView.findViewById(R.id.AssigneeUserImage);
            relative_option_iconofAssignee = (RelativeLayout) itemView.findViewById(R.id.relative_option_iconofAssignee);

        }
    }

    public void removeAt(int position) {
        unassignee.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, unassignee.size());
    }

    private void Assigned(String userid, String Taskid, String projectId, int position) {
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Edittask_api api = rest.create(Edittask_api.class);
        api.assignedtask(userid, Taskid, projectId, new Callback<AssignModle>() {
            @Override
            public void success(AssignModle assignModle, Response response) {
                if (assignModle.getSuccess().equals("1")) {
                    removeAt(position);
                    Toast.makeText(activity, "" + assignModle.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "" + assignModle.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
