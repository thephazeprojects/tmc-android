package com.phazeprojectsystems.Adapter;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
//import com.jakewharton.retrofit.Ok3Client;
import com.phazeprojectsystems.Activities.Create_task;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.DeleteTask_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.DeleteTaskData;
import com.phazeprojectsystems.Model.GetAllTaskDataResult;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import java.util.ArrayList;
import java.util.List;
//import okhttp3.OkHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ravi on 9/5/2017.
 */

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.MyViewHolder> {

    List<GetAllTaskDataResult> taskList = new ArrayList<>();
     private Activity activity;
    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView TaskTextView;

        MyViewHolder(final View itemView) {
            super(itemView);
            TaskTextView=(TextView)itemView.findViewById(R.id.Taskname_inList);

            TaskTextView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    return false;
                }
            });

            TaskTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemView.getContext().startActivity(new Intent(itemView.getContext(),Create_task.class));
                }
            });

        }
    }

    public
    TaskListAdapter(Activity activity,List<GetAllTaskDataResult> result) {
        this.taskList = result;
        this.activity=activity;
         }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView=  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_list, parent, false);
        return new MyViewHolder(itemView);}

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GetAllTaskDataResult  listdata = taskList.get(position);
        holder.TaskTextView.setText(listdata.getTaskName());
        }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

}
