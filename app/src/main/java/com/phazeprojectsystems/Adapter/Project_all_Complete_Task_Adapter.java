package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Activities.Select_team_menber;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.DeleteTask_Api;
import com.phazeprojectsystems.Api.In_Complete_Api;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.AssigneeByTask_Result_model;
import com.phazeprojectsystems.Model.AssigneeByTask_model;
import com.phazeprojectsystems.Model.CommentLis;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.DeleteTaskData;
import com.phazeprojectsystems.Model.GetAssigneUser_Result_modle;
import com.phazeprojectsystems.Model.GetInCompleteProjectTask__result;
import com.phazeprojectsystems.Model.Get_Assigned_User_modle;
import com.phazeprojectsystems.Model.Get_Project_complete_task_result;
import com.phazeprojectsystems.Model.InCompleteTaskModle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 3/1/18.
 */

public class Project_all_Complete_Task_Adapter  extends RecyclerView.Adapter<Project_all_Complete_Task_Adapter.MyViewHolder> {

    List<Get_Project_complete_task_result> tasklist= new ArrayList<>();

    private Activity activity;
    private Integer get_add_tag;
    String userid;
    private String[] list;

    public Project_all_Complete_Task_Adapter(Activity activity, List<Get_Project_complete_task_result> tasklist){
        this.activity=activity;

if (!tasklist.isEmpty()) {

    for (int i = 0; i < tasklist.size(); i++) {
        for (int j = tasklist.size() - 1; j > i; j--) {
            if (tasklist.get(i).getAdd_tag() > tasklist.get(j).getAdd_tag()) {
                Get_Project_complete_task_result tmp = tasklist.get(i);
                tasklist.set(i, tasklist.get(j));
                tasklist.set(j, tmp);
            }
        }
    }

}
        this.tasklist=tasklist;

        list = MySharePrafranceClass.GetSharePrefrance(activity);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView TaskTextView,txt_status,listAssiedUserView;
        ImageView tagimage,img_taskincomplete;
        RelativeLayout img_icon;
        MyViewHolder(View itemView) {
            super(itemView);
            TaskTextView=(TextView)itemView.findViewById(R.id.Taskname_inList);
            tagimage= (ImageView) itemView.findViewById(R.id.tag_image);
            img_icon= (RelativeLayout) itemView.findViewById(R.id.relative_option_icon);
            img_taskincomplete= (ImageView) itemView.findViewById(R.id.img_taskincomplete);
            txt_status= (TextView) itemView.findViewById(R.id.txt_status);
            listAssiedUserView= (TextView) itemView.findViewById(R.id.listAssiedUserView);
        }
    }


    @Override
    public Project_all_Complete_Task_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView=  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_list, parent, false);

        return  new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Project_all_Complete_Task_Adapter.MyViewHolder holder, int position) {

        final Get_Project_complete_task_result  taskResult = tasklist.get(position);


        String taskid = taskResult.getTask_id();
        String companyName =list[2];
        String projectid = taskResult.getProject_id();




        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        User_by_Company_Api api = rest.create(User_by_Company_Api.class);
        api.getAssignedUser(taskid, new Callback<Get_Assigned_User_modle>() {
            @Override
            public void success(Get_Assigned_User_modle get_assigned_user_modle, Response response) {
                if (get_assigned_user_modle.getSuccess().equals("1")){

                    List<GetAssigneUser_Result_modle>  assigneuser = get_assigned_user_modle.getResult();


                    if (assigneuser!=null) {

                        for (int i = 0; i < assigneuser.size(); i++) {

                            for (int j = i + 1; j < assigneuser.size(); j++) {
                                if (assigneuser.get(i)!=null && assigneuser.get(j)!=null) {

                                    if (assigneuser.get(i).getId() != null && assigneuser.get(j).getId() != null) {
                                        if (assigneuser.get(i).getId().equals(assigneuser.get(j).getId())) {
                                            assigneuser.remove(j);
                                            j--;
                                        }

                                    }
                                }
                            }

                        }

                    }

                    if (assigneuser!=null) {

                        if (!assigneuser.isEmpty()) {

                            if (assigneuser.get(0)!=null) {

                                Integer size = assigneuser.size();
                                Integer more = size - 1;
                                String username = assigneuser.get(0).getUsername();

                                holder.listAssiedUserView.setText(username + " + " + more + " More");

                            }
                        }
                    }


                    holder.listAssiedUserView.setOnClickListener(v->{

                        android.app.AlertDialog alertDialog;

                        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(activity,R.style.DialogTheme);
                        LayoutInflater inflater = activity.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialogview, null);
                        RecyclerView   recycle = (RecyclerView) dialogView.findViewById(R.id.recycleViewides);

                        if (assigneuser!=null) {
                            if (!assigneuser.isEmpty()) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                                recycle.setLayoutManager(linearLayoutManager);
                                DialogAssignUserAdapter dialogassignUserAdapter = new DialogAssignUserAdapter(activity, assigneuser,taskid);
                                recycle.setAdapter(dialogassignUserAdapter);
                                dialogassignUserAdapter.notifyDataSetChanged();
                            }
                        }

                        TextView addmorassgin=(TextView)dialogView.findViewById(R.id.addmorassgin);
                        addmorassgin.setOnClickListener(view -> {




                                RestAdapter rest =  new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                            User_by_Company_Api api = rest.create(User_by_Company_Api.class);
                            api.AssigneeUserBuyTask(taskid, companyName, new Callback<AssigneeByTask_model>() {
                                @Override
                                public void success(AssigneeByTask_model assigneeByTask_model, Response response) {
                                    List<AssigneeByTask_Result_model> unassignee = assigneeByTask_model.getResult();


                                   if( unassignee!=null){

                                       if (!unassignee.isEmpty()) {
                                           LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                                           recycle.setLayoutManager(linearLayoutManager);
                                           DialogUNAssignUserAdapter dialogunassignUserAdapter = new DialogUNAssignUserAdapter(activity, unassignee,taskid,projectid);
                                           recycle.setAdapter(dialogunassignUserAdapter);
                                           dialogunassignUserAdapter.notifyDataSetChanged();
                                       }

                                   }else {
                                       Toast.makeText(activity, "No More  Assignee Have .", Toast.LENGTH_SHORT).show();
                                   }

                                }

                                @Override
                                public void failure(RetrofitError error) {

                                }

                 });

                        });
                        dialogBuilder.setTitle("Appointed to : ");
                        dialogBuilder.setView(dialogView);
                        alertDialog = dialogBuilder.create();
                        alertDialog.show();


                    });



                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });



        holder.TaskTextView.setText(taskResult.getTask_name());

        get_add_tag=taskResult.getAdd_tag();
        switch (get_add_tag) {
            case 1 :
                holder.tagimage.setImageResource(R.drawable.red1_circle);
                break;
            case 2 :
                holder.tagimage.setImageResource(R.drawable.yellow_circle);
                break;
            case 3 :
                holder.tagimage.setImageResource(R.drawable.green_circle);
                break;
        }

        holder.tagimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                get_add_tag=taskResult.getAdd_tag();
                switch (get_add_tag) {
                    case  1 :
                        Toast.makeText(activity, "High Priority Task", Toast.LENGTH_SHORT).show();

                        break;
                    case  2 :
                        Toast.makeText(activity, "Medium Priority Task", Toast.LENGTH_SHORT).show();
                        break;
                    case  3 :
                        Toast.makeText(activity, "Low Priority Task", Toast.LENGTH_SHORT).show();
                        break;
                }


            }
        });



        Calendar now = Calendar.getInstance();   // Gets the current date and time
        int year = now.get(Calendar.YEAR);

        int day=now.get(Calendar.DATE);
        int month1=now.get(Calendar.MONTH)+1;
        String todayDate=day+"/"+month1+"/"+year;
        String tomarrowDate=day+1+"/"+month1+"/"+year;
        String tomarrowafterDate=day+2+"/"+month1+"/"+year;

        String  comimg_date = taskResult.getDue_date();
        if (todayDate.equals(comimg_date))
        {
            holder.txt_status.setText("Today");
            holder.txt_status.setTextColor(Color.RED);
        }
        else if (tomarrowDate.equals(comimg_date)){
            holder.txt_status.setText("Tomorrow");
            holder.txt_status.setTextColor(Color.BLUE);
        }
        else if (tomarrowafterDate.equals(comimg_date)){
            holder.txt_status.setText(taskResult.getDue_date());
            holder.txt_status.setTextColor(Color.GREEN);
        }
        else {
            holder.txt_status.setText(taskResult.getDue_date());
        }

        holder.img_taskincomplete.setImageResource(R.drawable.task_complete);

        holder.img_taskincomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.img_taskincomplete.setImageResource(R.drawable.task_incomplete);

                final String gettaskid=taskResult.getTask_id();


                userid = list[0];

                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setCancelable(false);
                dialog.setTitle("Phaze In-Complete");
                dialog.setMessage("Are you sure you want to In-Complete this Phaze?" );
                dialog.setPositiveButton("In-Complete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                        String time  = sdf.format(cal.getTime());
                        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                        In_Complete_Api api = rest.create(In_Complete_Api.class);
                        api.InComplete(userid, gettaskid,time,new Callback<InCompleteTaskModle>() {
                            @Override
                            public void success(InCompleteTaskModle inCompleteTaskModle, Response response) {
                                if (inCompleteTaskModle.getSuccess().equalsIgnoreCase("1")) {

                                    removeAt(position);
                                    notifyDataSetChanged();
                                    Toast.makeText(activity, "Task In_Completed", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                })
                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                holder.img_taskincomplete.setImageResource(R.drawable.task_incomplete);
                            }
                        });

                final AlertDialog alert = dialog.create();
                alert.show();
            }
        });

        holder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                PopupMenu popupMenu = new PopupMenu(activity,holder.img_icon);
                popupMenu.getMenuInflater().inflate(R.menu.task_menu,popupMenu.getMenu());
                popupMenu.getMenu().getItem(1).setVisible(false);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId())
                        {

                            case R.id.delete_task:
                                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                dialog.setCancelable(false);
                                dialog.setTitle("Delete Phaze");
                                dialog.setMessage("Are you sure you want to Delete this Phaze ?" );
                                dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        final String task__id=taskResult.getTask_id();

                                        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                        DeleteTask_Api api=rest.create(DeleteTask_Api.class);


                                        api.Delete(task__id, new Callback<DeleteTaskData>() {
                                            @Override
                                            public void success(DeleteTaskData deleteTaskData, Response response) {


                                                String status=deleteTaskData.getSuccess();

                                                if (status.equalsIgnoreCase("1"))

                                                {
                                                    tasklist.remove(position);
                                                    notifyDataSetChanged();
                                                }


                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });




                                    }
                                })
                                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                                final AlertDialog alert = dialog.create();
                                alert.show();

                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String gettaskname = taskResult.getTask_name();
                String getduwdate = taskResult.getDue_date();
                String getdescription = taskResult.getDescription();
                String taskid = taskResult.getTask_id();
                Integer add_tag = taskResult.getAdd_tag();
                String updatetime=taskResult.getUpdate_time();

                Intent intent=new Intent(holder.itemView.getContext(),Comment_screen.class);
                intent.putExtra("gettaskname",gettaskname);
                intent.putExtra("getdewdate",getduwdate);
                intent.putExtra("getdescription",getdescription);
                intent.putExtra("gettaskid",taskid);
                intent.putExtra("getaddtag",""+add_tag);
                intent.putExtra("updatetime",updatetime);
                intent.putExtra("comingintent", "other");
                holder.itemView.getContext().startActivity(intent);

            }
        });
        YoYo.with(Techniques.Landing).playOn(holder.itemView);

    }


    @Override
    public int getItemCount() {
        return tasklist.size();
    }

    public void removeAt(int position) {
        tasklist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, tasklist.size());
    }

}
