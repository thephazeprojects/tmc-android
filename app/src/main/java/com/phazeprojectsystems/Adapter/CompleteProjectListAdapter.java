package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.Task_activity;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompleteProject;
import com.phazeprojectsystems.Model.CompleteProjectResult;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.DeleteProjectData;
import com.phazeprojectsystems.Model.ProjectList;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 5/1/18.
 */

public class CompleteProjectListAdapter extends RecyclerView.Adapter<CompleteProjectListAdapter.MyViewHolder>  {
    private List<CompleteProjectResult> projectcomplete;
    private Activity activity;

    public CompleteProjectListAdapter(Activity activity, List<CompleteProjectResult> projectcomplete) {
        this.activity = activity;
        this.projectcomplete = projectcomplete;
    }


    @Override
    public CompleteProjectListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompleteProjectListAdapter.MyViewHolder holder, int position) {

        final CompleteProjectResult listdata = projectcomplete.get(position);
        holder.ProjectTextView.setText(listdata.getProjactName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), Task_activity.class);
                MySharePrafranceClass.ProjectShare(holder.itemView.getContext(), projectcomplete.get(position).getProjectId(),
                        projectcomplete.get(position).getProjactName());
                        holder.itemView.getContext().startActivity(intent);
            }
        });


        YoYo.with(Techniques.Landing).playOn(holder.itemView);

        holder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(activity, holder.img_icon);
                popupMenu.getMenuInflater().inflate(R.menu.projectlistmenu, popupMenu.getMenu());
                popupMenu.getMenu().getItem(1).setVisible(false);
                popupMenu.getMenu().getItem(3).setVisible(false);
                popupMenu.getMenu().getItem(4).setVisible(false);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delete_project:

//

                                final String projectid = listdata.getProjectId();

                                RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                DeleteProject_Api api = rest.create(DeleteProject_Api.class);

                                api.DeleteProject(projectid, new Callback<DeleteProjectData>() {
                                    @Override
                                    public void success(DeleteProjectData deleteProjectData, Response response) {

                                        String status = deleteProjectData.getSuccess();
                                        if (status.equalsIgnoreCase("1")) {
                                            Toast.makeText(activity, "Project Deleted", Toast.LENGTH_SHORT).show();

                                            projectcomplete.remove(position);
                                            notifyDataSetChanged();
                                        }

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                        Toast.makeText(activity, "Failuree" + error, Toast.LENGTH_SHORT).show();
                                    }
                                });

                                break;


                            case R.id.Incomplete_project :

                                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                dialog.setCancelable(false);
                                dialog.setTitle("Project In_Complete");
                                dialog.setMessage("Are you sure you want to In_complete this Project ?" );
                                dialog.setPositiveButton("In_Complete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        RestAdapter rest1 = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                        Get_User_Project Api = rest1.create(Get_User_Project.class);
                                        Api.InCompleteProject(listdata.getUserId(),listdata.getProjectId(),new Callback<CompleteProject>() {
                                            @Override
                                            public void success(CompleteProject completeProject, Response response) {

                                                removeAt(position);

                                                notifyDataSetChanged();
                                                Toast.makeText(activity, ""+completeProject.getResult(), Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(activity, ""+error, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                })
                                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                                final AlertDialog alert = dialog.create();
                                alert.show();
                                break;

                            case R.id.Complete_project:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectcomplete.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ProjectTextView;
        RelativeLayout img_icon;
        public Context context;
        public MyViewHolder(View itemView) {
            super(itemView);

            ProjectTextView = (TextView) itemView.findViewById(R.id.projectname_inList);
            img_icon = (RelativeLayout) itemView.findViewById(R.id.relative_option_icon_deleteproject);
            context = itemView.getContext();
        }
    }
    public void removeAt(int position) {
        projectcomplete.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, projectcomplete.size());
    }
}
