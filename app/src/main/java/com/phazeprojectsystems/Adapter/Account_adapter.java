package com.phazeprojectsystems.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.phazeprojectsystems.Fragment.Account_fragment;

/**
 * Created by NAVJOT SINGH on 24-07-2017.
 */

public class Account_adapter extends FragmentStatePagerAdapter {

    int tabCount;

    public Account_adapter(FragmentManager fm, int tabCount) {
        super(fm);

        this.tabCount=tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:
                Account_fragment account_fragment=new Account_fragment();
                return account_fragment;

            case 1:
//                Extras_fragment extras_fragment=new Extras_fragment();
//                return extras_fragment;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }
}
