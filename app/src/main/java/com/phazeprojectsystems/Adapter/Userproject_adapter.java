package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.User_taskscreen;
import com.phazeprojectsystems.Api.Get_InCompletTask_Api;
import com.phazeprojectsystems.Api.Project_Incomplete_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.GetInCompleteProjectTask__result;
import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;
import com.phazeprojectsystems.Model.InCompletetasklistmodel;
import com.phazeprojectsystems.Model.Pendingtasklis;
import com.phazeprojectsystems.Model.Userprojec;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by NAVJOT SINGH on 03-11-2017.
 */

public class Userproject_adapter extends RecyclerView.Adapter<Userproject_adapter.MyViewHolder> {
    private List<Userprojec> ProjectList;
    private Activity activity;
    private String[] list;
    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView ProjectTextView;
        public Context context;
        public Button projectTaskIncompleteCount;
        public RelativeLayout img_icon;

        MyViewHolder(final View itemView)

        {
            super(itemView);
            int vn = Userproject_adapter.MyViewHolder.super.getPosition();
            ProjectTextView =(TextView)itemView.findViewById(R.id.projectname_inList);
            projectTaskIncompleteCount=(Button)itemView.findViewById(R.id.projectTaskIncompleteCount);
            img_icon = (RelativeLayout) itemView.findViewById(R.id.relative_option_icon_deleteproject);
            context=itemView.getContext();
        }
    }
    public Userproject_adapter(Activity activity, List<Userprojec> ProjectList) {
        this.activity = activity;
        this.ProjectList = ProjectList;
        list = MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Userprojec listdata = ProjectList.get(position);
        holder.ProjectTextView.setText(listdata.getProjactName());

        String projectid = listdata.getProjectId();
        String UserId = list[0];

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Get_InCompletTask_Api api = rest.create(Get_InCompletTask_Api.class);

        api.GetInCompletTask(UserId,projectid, new Callback<InCompletetasklistmodel>() {
            @Override
            public void success(InCompletetasklistmodel inCompletetasklistmodel, Response response) {

                List<Pendingtasklis> incompletetask = inCompletetasklistmodel.getPendingtasklist();

                if (incompletetask!=null)
                {

                    holder.projectTaskIncompleteCount.setText(Integer.toString(incompletetask.size()));
                }
                else
                    {
                       holder.projectTaskIncompleteCount.setText(0);
                    }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });



//        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
//        Project_Incomplete_Task_Api api =rest.create(Project_Incomplete_Task_Api.class);
//
//        api.getProjectAllIncompleteTask(projectid, new Callback<Get_In_complete_Project_task_modle>() {
//            @Override
//            public void success(Get_In_complete_Project_task_modle get_in_complete_project_task_modle, Response response) {
//                List<GetInCompleteProjectTask__result> incompletetask=get_in_complete_project_task_modle.getResult();
//
//                holder.projectTaskIncompleteCount.setText(Integer.toString(incompletetask.size()));
//            }
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//

        holder.img_icon.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), User_taskscreen.class);

                intent.putExtra("projectid",listdata.getProjectId());
                intent.putExtra("projectname",listdata.getProjactName());

                holder.itemView.getContext().startActivity(intent);
            }
        });
        YoYo.with(Techniques.Landing).playOn(holder.itemView);
    }
    @Override
    public int getItemCount() {
        return ProjectList.size();
    }}
