package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.phazeprojectsystems.Activities.Task_activity;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Api.Project_Incomplete_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.DeleteProjectData;
import com.phazeprojectsystems.Model.GetInCompleteProjectTask__result;
import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;
import com.phazeprojectsystems.Model.ProjectList;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

//import com.jakewharton.retrofit.Ok3Client;
//import okhttp3.OkHttpClient;

/**
 * Created by Ravi on 9/12/2017.
 */

public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.MyViewHolder> {
    private List<ProjectList> ProjectList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView ProjectTextView;
        RelativeLayout img_icon;
        Button projectTaskIncompleteCount;
        public Context context;

        MyViewHolder(final View itemView) {
            super(itemView);
            int vn = ProjectListAdapter.MyViewHolder.super.getPosition();
            ProjectTextView = (TextView) itemView.findViewById(R.id.projectname_inList);
            img_icon = (RelativeLayout) itemView.findViewById(R.id.relative_option_icon_deleteproject);
            projectTaskIncompleteCount=(Button)itemView.findViewById(R.id.projectTaskIncompleteCount);
            context = itemView.getContext();
        }
    }

    public ProjectListAdapter(Activity activity, List<ProjectList> ProjectList) {
        this.activity = activity;
        this.ProjectList = ProjectList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProjectList listdata = ProjectList.get(position);


        holder.ProjectTextView.setText(listdata.getProjactName());


               String projectid = listdata.getProjectId();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Project_Incomplete_Task_Api api =rest.create(Project_Incomplete_Task_Api.class);

        api.getProjectAllIncompleteTask(projectid, new Callback<Get_In_complete_Project_task_modle>() {
            @Override
            public void success(Get_In_complete_Project_task_modle get_in_complete_project_task_modle, Response response) {
                List<GetInCompleteProjectTask__result> incompletetask = get_in_complete_project_task_modle.getResult();

                if (incompletetask!=null){

                holder.projectTaskIncompleteCount.setText(Integer.toString(incompletetask.size()));
                }else {
                    holder.projectTaskIncompleteCount.setText(0);
                }
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), Task_activity.class);
                MySharePrafranceClass.ProjectShare(holder.itemView.getContext(), ProjectList.get(position).getProjectId(),
                        ProjectList.get(position).getProjactName());

                holder.itemView.getContext().startActivity(intent);
            }
        });


        YoYo.with(Techniques.Landing).playOn(holder.itemView);


        holder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(activity, holder.img_icon);
                popupMenu.getMenuInflater().inflate(R.menu.projectlistmenu, popupMenu.getMenu());
                popupMenu.getMenu().getItem(1).setVisible(false);
                popupMenu.getMenu().getItem(2).setVisible(false);
                popupMenu.getMenu().getItem(3).setVisible(false);
                popupMenu.getMenu().getItem(4).setVisible(false);
                //popupMenu.getMenu().getItem(R.id.In_completeProject).setVisible(false);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delete_project:

//                                  model2=data.get(position);

                                final String projectid = listdata.getProjectId();

                                RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                                DeleteProject_Api api = rest.create(DeleteProject_Api.class);

                                api.DeleteProject(projectid, new Callback<DeleteProjectData>() {
                                    @Override
                                    public void success(DeleteProjectData deleteProjectData, Response response) {

                                        String status = deleteProjectData.getSuccess();
                                        if (status.equalsIgnoreCase("1")) {
                                            Toast.makeText(activity, "Project Deleted", Toast.LENGTH_SHORT).show();

                                            ProjectList.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                        Toast.makeText(activity, "Failuree" + error, Toast.LENGTH_SHORT).show();
                                    }
                                });

                                break;
                            case R.id.Complete_project:
                                break;

                            case R.id.In_completeProject:
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ProjectList.size();
    }
}
