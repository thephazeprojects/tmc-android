package com.phazeprojectsystems.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.Commentdelete_model;
import com.phazeprojectsystems.Model.ComntLis;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NAVJOT SINGH on 03-10-2017.
 */

public class AllComment_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private LayoutInflater mInflater;
    public static List<ComntLis> data=new ArrayList<>();
    private  String[] list;
    ComntLis finallist;
    ComntLis finallist1;
    Dialog  dialog1;
    private Array cursor;

    public AllComment_adapter(Activity mActivity, List<ComntLis> data) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);


        for (int i = 0; i < data.size(); i++) {
            for (int j = data.size() - 1; j > i; j--) {
                if (data.get(i).getComentid() < data.get(j).getComentid()) {
                    ComntLis tmp = data.get(i);
                    data.set(i,data.get(j));
                    data.set(j,tmp);
                }
            }
        }


        this.data=data;

        list = MySharePrafranceClass.GetSharePrefrance(mActivity);

    }
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_ITEM;
        } else {
            return TYPE_ITEM;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view = mInflater.inflate(R.layout.comment_layout, parent, false);

        return new AllComment_adapter.FindHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AllComment_adapter.FindHolder) {


            finallist = (ComntLis) data.get(position);

            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();


            Log.d("Time zone: ", tz.getDisplayName());


            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(tz);

/* print your timestamp and double check it's the date you expect */
          
//            long timestamp = cursor.getLong(1);
//            String localTime = sdf.format(new Date(timestamp * 1000)); // I assume your timestamp is in seconds and you're converting to milliseconds?
//            Log.d("Time: ", localTime);

            if (finallist.getUserid().equals(list[0]))
            {

                // sender Side

                ((AllComment_adapter.FindHolder) holder).senderside.setVisibility(View.VISIBLE);

                ((FindHolder) holder).recivereside.setVisibility(View.GONE);

                String user_image=finallist.getUserthumb();
                String attach_image=finallist.getAttach();

                if (!(user_image==null)) {
                    if (!(user_image.equalsIgnoreCase(""))){
                        String image= Constant.picuri.concat(user_image);
                        Glide.with(mActivity).load(image).into(((FindHolder) holder).sendersideUserImage);
                    }

                }else
                {

                }

                if (!(attach_image==null)) {
                    if (!(attach_image.equalsIgnoreCase("")))
                    {
                        ((AllComment_adapter.FindHolder) holder).sendersideimageCArd.setVisibility(View.VISIBLE);
                        String image_attach= Constant.picuri.concat(attach_image);
                        Glide.with(mActivity).load(image_attach)
                                .into(((FindHolder) holder).sendersidecomment_image);
                    }
                }
                else
                {
                    ((FindHolder) holder).sendersidecomment_image.setVisibility(View.GONE);
                }



                ((FindHolder) holder).sendersidecomment_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        finallist1 = (ComntLis) data.get(position);
                        String fullimage=finallist1.getAttach();
                        dialog1 = new Dialog(mActivity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                        dialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
                        dialog1.setContentView(mActivity.getLayoutInflater().inflate(R.layout.image_dialog, null));ImageView imageattach = (ImageView) dialog1.findViewById(R.id.img_comment_image);
                        String image_attach_full= Constant.picuri.concat(fullimage);
                        Glide.with(mActivity).load(image_attach_full).into(imageattach);
                        dialog1.getWindow().setBackgroundDrawable(null);
                        dialog1.show();
                    }
                });

                ((AllComment_adapter.FindHolder) holder).SenderSidetxt_user_comment.setText(finallist.getComment());
                ((AllComment_adapter.FindHolder) holder).SenderSidetxt_comment_time.setText(finallist.getTime());
                ((FindHolder) holder).senderSidetxt_date.setText(finallist.getNdate());
                ((AllComment_adapter.FindHolder) holder).sendersidetxt_comment_name.setText(finallist.getUsername());



                holder.itemView.setOnClickListener(view -> {

                    finallist1 =  data.get(position);

                    final Integer commentid = finallist1.getComentid();



                    AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                    dialog.setCancelable(false);
                    dialog.setTitle("Delete Comment");
                    dialog.setMessage("Are you sure you want to delete this Comment?" );
                    dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {


                            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                            DeleteProject_Api api = rest.create(DeleteProject_Api.class);


                            api.Deletecomment(String.valueOf(commentid), new Callback<Commentdelete_model>() {
                                @Override
                                public void success(Commentdelete_model commentdelete_model, Response response) {

                                    if (commentdelete_model.getSuccess()==1)
                                    {

                                        data.remove(position);
                                        notifyItemRemoved(position);
                                        notifyDataSetChanged();
                                    }
                                    else {
                                        Toast.makeText(mActivity, ""+commentdelete_model.getResponse(), Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void failure(RetrofitError error) {

                                    Toast.makeText(mActivity, ""+error, Toast.LENGTH_SHORT).show();
                                }
                            });


                        }
                    })
                            .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                });
            }


            else {

                //reciver Side

                ((AllComment_adapter.FindHolder) holder).recivereside.setVisibility(View.VISIBLE);
                ((FindHolder) holder).senderside.setVisibility(View.GONE);


                String user_image=finallist.getUserthumb();
                String attach_image=finallist.getAttach();

                if (!(user_image==null)) {
                    if (!(user_image.equalsIgnoreCase(""))){
                        String image= Constant.picuri.concat(user_image);
                        Glide.with(mActivity).load(image).into(((FindHolder) holder).reciveresideUserImage);
                    }

                }else
                {

                }

                if (!(attach_image==null)) {
                    if (!(attach_image.equalsIgnoreCase("")))
                    {
                        ((AllComment_adapter.FindHolder) holder).rendersideimageCArd.setVisibility(View.VISIBLE);
                        String image_attach = Constant.picuri.concat(attach_image);
                        Glide.with(mActivity).load(image_attach).into(((FindHolder) holder).reciveresidecomment_image);
                    }
                }
                else
                {
                    ((FindHolder) holder).reciveresidecomment_image.setVisibility(View.GONE);
                }



                ((FindHolder) holder).reciveresidecomment_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        finallist1 = (ComntLis) data.get(position);
                        String fullimage=finallist1.getAttach();
                        dialog1 = new Dialog(mActivity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                        dialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
                        dialog1.setContentView(mActivity.getLayoutInflater().inflate(R.layout.image_dialog, null));ImageView imageattach = (ImageView) dialog1.findViewById(R.id.img_comment_image);
                        String image_attach_full= Constant.picuri.concat(fullimage);
                        Glide.with(mActivity).load(image_attach_full).into(imageattach);
                        dialog1.getWindow().setBackgroundDrawable(null);
                        dialog1.show();
                    }
                });


                        ((AllComment_adapter.FindHolder) holder).reciveresidetxt_user_comment.setText(finallist.getComment());
                       ((AllComment_adapter.FindHolder) holder).reciveresidetxt_comment_time.setText(finallist.getTime());
                      ((FindHolder) holder).reciveresidetxt_date.setText(finallist.getNdate());
                   ((FindHolder) holder).reciveresidetxt_comment_name.setText(finallist.getUsername());
                   holder.itemView.setOnClickListener(view -> {

                       finallist1 =  data.get(position);

                       final Integer commentid = finallist1.getComentid();



                       AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                       dialog.setCancelable(false);
                       dialog.setTitle("Delete Comment");
                       dialog.setMessage("Are you sure you want to delete this Comment?" );
                       dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialog, int id) {


                               RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                               DeleteProject_Api api = rest.create(DeleteProject_Api.class);


                               api.Deletecomment(String.valueOf(commentid), new Callback<Commentdelete_model>() {
                                   @Override
                                   public void success(Commentdelete_model commentdelete_model, Response response) {

                                       if (commentdelete_model.getSuccess()==1)
                                       {

                                           data.remove(position);
                                           notifyItemRemoved(position);
                                           notifyDataSetChanged();
                                       }
                                       else {
                                           Toast.makeText(mActivity, ""+commentdelete_model.getResponse(), Toast.LENGTH_SHORT).show();
                                       }

                                   }

                                   @Override
                                   public void failure(RetrofitError error) {

                                       Toast.makeText(mActivity, ""+error, Toast.LENGTH_SHORT).show();
                                   }
                               });


                           }
                       })
                               .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialog, int which) {
                                       dialog.cancel();
                                   }
                               });

                       final AlertDialog alert = dialog.create();
                       alert.show();

                   });

            }

        }
    }

    @Override
    public int getItemCount() {

        if (data==null)
        {
            return 0;
        }
        return data.size();
    }


    public class FindHolder extends RecyclerView.ViewHolder {

        RelativeLayout senderside,recivereside;
        CardView rendersideimageCArd , sendersideimageCArd;


        CircleImageView sendersideUserImage,reciveresideUserImage;

        TextView reciveresidetxt_comment_name,sendersidetxt_comment_name,SenderSidetxt_comment_time,reciveresidetxt_comment_time,
                 reciveresidetxt_user_comment,SenderSidetxt_user_comment,senderSidetxt_date,reciveresidetxt_date;

        ImageView sendersidecomment_image,reciveresidecomment_image;

        public FindHolder(View v) {
            super(v);

            sendersideUserImage= (CircleImageView) v.findViewById(R.id.sendersideUserImage);
            reciveresideUserImage= (CircleImageView) v.findViewById(R.id.reciveresideUserImage);

            senderside= (RelativeLayout) v.findViewById(R.id.senderside);
            recivereside= (RelativeLayout) v.findViewById(R.id.recivereside);

            sendersidecomment_image= (ImageView) v.findViewById(R.id.sendersidecomment_image);
            reciveresidecomment_image= (ImageView) v.findViewById(R.id.reciveresidecomment_image);

            reciveresidetxt_user_comment = (TextView) v.findViewById(R.id.reciveresidetxt_user_comment);
            SenderSidetxt_user_comment = (TextView) v.findViewById(R.id.SenderSidetxt_user_comment);

            SenderSidetxt_comment_time  = (TextView) v.findViewById(R.id.SenderSidetxt_comment_time);
            reciveresidetxt_comment_time  = (TextView) v.findViewById(R.id.reciveresidetxt_comment_time);

            senderSidetxt_date= (TextView) v.findViewById(R.id.senderSidetxt_date);
            reciveresidetxt_date= (TextView) v.findViewById(R.id.reciveresidetxt_date);




            reciveresidetxt_comment_name = (TextView) v.findViewById(R.id.reciveresidetxt_comment_name);
            sendersidetxt_comment_name = (TextView) v.findViewById(R.id.sendersidetxt_comment_name);
            sendersideimageCArd =(CardView)v.findViewById(R.id.sendersideimageCArd);
            rendersideimageCArd =(CardView)v.findViewById(R.id.rendersideimageCArd);

        }

    }


}