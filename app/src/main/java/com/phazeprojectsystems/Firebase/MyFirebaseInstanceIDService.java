package com.phazeprojectsystems.Firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by NAVJOT SINGH on 23-11-2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();


//        Toast.makeText(this, ""+refreshedToken, Toast.LENGTH_SHORT).show();


        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {


        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
