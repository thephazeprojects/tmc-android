package com.phazeprojectsystems.Firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.phazeprojectsystems.Activities.MainActivity;
import com.phazeprojectsystems.Model.Dtabase_Notification_modle;
import com.phazeprojectsystems.SharePool.NotificationData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 17/1/18.
 */

public class MyBroadcast extends BroadcastReceiver {
    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();
    @Override
    public void onReceive(Context context, Intent intent) {

        String msg = intent.getExtras().getString("msg");

        String gettaskname= intent.getExtras().getString("gettaskname");
        String getduwdate= intent.getExtras().getString("getduwdate");
        String  getdescription = intent.getExtras().getString("getdescription");
        String  gettaskid = intent.getExtras().getString("gettaskid");
        String  getaddtag = intent.getExtras().getString("getaddtag");



        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String datetime = formatter.format(date);

        NotificationData notificationDatabase = new NotificationData(context);
        SQLiteDatabase database = notificationDatabase.getWritableDatabase();
        notificationDatabase.NewAddNotification(database,msg,datetime,gettaskname,getduwdate,getdescription,gettaskid,getaddtag);

        notificationDatabase.close();
        readFromDatabase(context);

        MainActivity activity = new MainActivity();
        activity.senotification(notificationlist.size());

    }



    private void readFromDatabase(Context context){

        NotificationData notificationData = new NotificationData(context);

        SQLiteDatabase database = notificationData.getWritableDatabase();

        Cursor cursor = notificationData.NewGetNotification(database);
        if (cursor.getCount()>0){
            notificationlist.clear();
            while (cursor.moveToNext()){
                int id = cursor.getInt(cursor.getColumnIndex(NotificationData.NEWCOLUMN_id));
                String msg=cursor.getString(cursor.getColumnIndex(NotificationData.NEWCOLUMN_MSG));
                String time=cursor.getString(cursor.getColumnIndex(NotificationData.NEWTIMEOFNOTIFICATION));

                String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskname));
                String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetduwdate));
                String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetdescription));


                String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskid));
                String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetaddtag));


                notificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

            }

        }

    }
}
