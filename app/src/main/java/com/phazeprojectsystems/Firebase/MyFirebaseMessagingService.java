package com.phazeprojectsystems.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
//import android.support.v7.app.NotificationCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.phazeprojectsystems.Activities.Comment_screen;
import com.phazeprojectsystems.Activities.User_comment_screen;
import com.phazeprojectsystems.R;

/**
 * Created by NAVJOT SINGH on 23-11-2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    PendingIntent pendingIntent;
    String gettaskname,getduwdate,getdescription,gettaskid,getaddtag,messege;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        messege=remoteMessage.getData().get("message");
        gettaskname=remoteMessage.getData().get("gettaskname");
        getduwdate=remoteMessage.getData().get("getduedate");
        getdescription=remoteMessage.getData().get("getdescription");
        gettaskid=remoteMessage.getData().get("gettaskid");
        getaddtag=remoteMessage.getData().get("getadd_tag");


        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (true) {
                sendNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("role"));

                Intent intent = new Intent(getApplicationContext(),MyBroadcast.class);
                intent.putExtra("msg",messege);
                intent.putExtra("gettaskname",gettaskname);
                intent.putExtra("getduwdate",getduwdate);
                intent.putExtra("getdescription",getdescription);
                intent.putExtra("gettaskid",gettaskid);
                intent.putExtra("getaddtag",getaddtag);

                sendBroadcast(intent);

                Recievedata(remoteMessage);
            }
            else {
                handleNow();
            }
        }}


    private void Recievedata(RemoteMessage remoteMessage) {

    }

    private void handleNow() {
    }
    
    private void sendNotification(String messageBody,String role) {

        if (role!=null) {

            if (role.equals("2")) {
                Intent intent = new Intent(this, Comment_screen.class);

                intent.putExtra("gettaskname", gettaskname);
                intent.putExtra("getdewdate", getduwdate);
                intent.putExtra("getdescription", getdescription);
                intent.putExtra("gettaskid", gettaskid);
                intent.putExtra("getaddtag", getaddtag);
                intent.putExtra("comingintent", "notification");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            } else {

                Intent intent = new Intent(this, User_comment_screen.class);
                intent.putExtra("gettaskname", gettaskname);
                intent.putExtra("getdewdate", getduwdate);
                intent.putExtra("getdescription", getdescription);
                intent.putExtra("gettaskid", gettaskid);
                intent.putExtra("getaddtag", getaddtag);
                intent.putExtra("comingintent", "notification");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            }

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle("Phaze System")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}
