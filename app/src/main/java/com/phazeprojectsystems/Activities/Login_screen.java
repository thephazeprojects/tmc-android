package com.phazeprojectsystems.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Api.Forget_Password_Api;
import com.phazeprojectsystems.Api.Login_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.ForgetPasswordData;
import com.phazeprojectsystems.Model.LoginData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.LinkedList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Login_screen extends AppCompatActivity {

    private  Button forgetemail_enter, forget_Cancel,signin;
    private  TextView forgot_password, forgetemail;
    private  EditText loginEmail, loginPassword;
    private  String loginemail,loginpass;
    private  static String VALID_PATTERN = "[A-Z]+|[a-z]+"; //[0-9]+|
    private  List<String> parseString;
    AlertDialog alertDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_login_screen);

        FINDVIEWBYID();
        CLICKLISTINER();
    }


    private void FINDVIEWBYID() {
        loginPassword = (EditText) findViewById(R.id.loginPassword);
        loginEmail = (EditText) findViewById(R.id.loginEmailId);
        signin = (Button) findViewById(R.id.sign_in);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
    }

    private void CLICKLISTINER() {

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 loginemail = loginEmail.getText().toString();
                 loginpass = loginPassword.getText().toString();


                 if (!validate()){

                     Toast.makeText(Login_screen.this, "Your Request Has Failed", Toast.LENGTH_SHORT).show();
                 }else {
                     login(loginemail, loginpass);
                 }
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Login_screen.this);
                LayoutInflater inflater = Login_screen.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.forget_password, null);

                forgetemail = (TextView) dialogView.findViewById(R.id.forgetemail);
                forgetemail_enter = (Button) dialogView.findViewById(R.id.forgetemail_enter);

                forgetemail_enter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email = forgetemail.getText().toString();

                        if (email.trim().equalsIgnoreCase(""))
                        {
                            Toast.makeText(Login_screen.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                        } else
                            {
                            ForgetPassword(email);
                            alertDialog.dismiss();
                        }
                    }
                });

                forget_Cancel = (Button) dialogView.findViewById(R.id.forgetemail_cancel);
                forget_Cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
                dialogBuilder.setView(dialogView);
                alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    private void ForgetPassword(String email) {
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Forget_Password_Api api = rest.create(Forget_Password_Api.class);
        api.Forget_Password(email, new Callback<ForgetPasswordData>() {
            @Override
            public void success(ForgetPasswordData forgetPasswordData, Response response) {

                String status = forgetPasswordData.getSuccess();
                String message = forgetPasswordData.getMessange();
                if (status.equalsIgnoreCase("1")) {

                    Toast.makeText(Login_screen.this, "Mail Sent Seccussfull", Toast.LENGTH_SHORT).show();
                } else {

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(Login_screen.this, "Check Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void login(String loginemail, String loginpass) {
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Login_Api loginapi = rest.create(Login_Api.class);
        loginapi.userlogin(loginemail, loginpass, new Callback<LoginData>() {
            @Override
            public void success(LoginData loginData, Response response) {
                if (loginData.getSuccess().equals("1")) {

                    if (loginData.getRole().equals("1")){

                        MySharePrafranceClass.LoginShare(Login_screen.this,loginemail,loginpass);


                        String userid = loginData.getUserid();
                        String username = loginData.getUsername();
                        String company = loginData.getCompanyname();
                        String image = loginData.getThumb();
                        MySharePrafranceClass.Share(Login_screen.this, userid, username, company, image,"1");
                        Constant.toast(Login_screen.this, "" + loginData.getMessage());
                        Intent intent = new Intent(Login_screen.this, MainActivity.class);
                        intent.putExtra("imageGilde", loginData.getThumb());
                        startActivity(intent);
                        finish();
                    }
                    else {

                        String userid1 = loginData.getUserid();
                        String username1 = loginData.getUsername();
                        String company1 = loginData.getCompanyname();
                        String image1 = loginData.getThumb();

                        if (username1.contains("@")) {
                            String usernameRight="";
                            String username = username1.substring(0, username1.indexOf("@"));


                            parseString=parse(username);
                            for (String e: parseString){
                                usernameRight=usernameRight+e;
                            }

                            MySharePrafranceClass.Share(Login_screen.this, userid1,usernameRight, company1, image1,"2");


                        }else {

                            MySharePrafranceClass.Share(Login_screen.this, userid1,username1, company1, image1,"2");

                        }


                        Intent intent = new Intent(Login_screen.this, Userproject_screen.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(Login_screen.this, "" + loginData.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(Login_screen.this, "Check Connection"+error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Login_screen.this, Main_screen.class);
        startActivity(intent);
        super.onBackPressed();

    }
    private boolean validate(){
        boolean valid=true;

        if (loginemail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(loginemail).matches()){
            loginEmail.setError("Enter Valid Email");
            valid=false;
        }
        if (loginpass.isEmpty()||loginpass.length()<6){
            loginPassword.setError("Enter Valid Password");
            valid=false;
        }

        return valid;
    }


    private List<String> parse(String toParse){
        List<String> chunks = new LinkedList<String>();
        toParse = toParse + "$";
         int beginIndex = 0;
         int endIndex = 0;
         while(endIndex < toParse.length())
         {
             while(toParse.substring(beginIndex, endIndex + 1).matches(VALID_PATTERN))
             {
                 endIndex++;
             }
             if(beginIndex != endIndex)
             {
                 chunks.add(toParse.substring(beginIndex, endIndex));
             }
             else
                 {
                     endIndex++;
                 }
                 beginIndex = endIndex;
         }
         return chunks;
    }
}
