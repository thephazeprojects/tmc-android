package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Adapter.AssignToAdapter;
import com.phazeprojectsystems.Api.Create_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Model.CreateTaskData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

import static com.phazeprojectsystems.Activities.Create_project.DATE_PICKER_ID;

public class Create_task extends AppCompatActivity {

    private String projectname, get_description, get_taskname, get_dueDate;
    private String getMemberuserid="",ggetmemberid="";
    private String SelecterMember="";
    private TextView  due_Date, Project_name, AddTag, Member;
    private RelativeLayout datePicker, assignetask;
    private EditText txt_taskname, txt_description;
    private ImageView img_submit, AssigeToPlusImage,img_red, img_yellow, img_green;
    private RelativeLayout backarrow,image_img_submitedittask;
    private String[] list;
    private String[] projectitemlist;
    RelativeLayout addtag, imgcolor_tag;
    private static boolean redclicked = false,yellowclicked = false,greenclicked = false;
    private int color;
    private int year,month,day;
    public static Map<String,String> TaskMember =  new HashMap<>();
    private RecyclerView Assigntolist;
    private LinearLayoutManager linearLayoutManager;
    private AssignToAdapter assigntoAdapter;
    private List<String> MemberNAme = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        projectitemlist = MySharePrafranceClass.ProjectGetSharePrefrance(Create_task.this);
        list = MySharePrafranceClass.GetSharePrefrance(Create_task.this);
        TaskMember.clear();
        findView();
        clicklis();

        projectname = projectitemlist[1];
        Project_name.setText(projectname);


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        due_Date.setText(new StringBuilder().append(day)
                .append("/").append(month + 1).append("/").append(year)
                .append(""));



        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_PICKER_ID);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TaskMember!=null){
            if (!TaskMember.isEmpty()){

                MemberNAme.clear();


                for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                    String e = entry.getKey();

                    MemberNAme.add(e);
                }

                SetLayout();

            }
        }
    }


    private void SetLayout() {
        linearLayoutManager= new LinearLayoutManager(Create_task.this,LinearLayoutManager.HORIZONTAL, false);
        Assigntolist.setLayoutManager(linearLayoutManager);
        assigntoAdapter = new AssignToAdapter(Create_task.this,MemberNAme,"CreateTask");
        Assigntolist.setAdapter(assigntoAdapter);
        assigntoAdapter.notifyDataSetChanged();
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            due_Date.setText(new StringBuilder().append(day)
                    .append("/").append(month + 1).append("/").append(year)
                    .append(""));

        }
    };


    private void findView() {

        AssigeToPlusImage =(ImageView)findViewById(R.id.AssigeToPlusImage);
        Assigntolist = (RecyclerView)findViewById(R.id.assigned_member);
        backarrow= (RelativeLayout) findViewById(R.id.backarrow_edittask);
        image_img_submitedittask=(RelativeLayout) findViewById(R.id.image_img_submitedittask);
        datePicker = (RelativeLayout) findViewById(R.id.txt_edittext_date_Picker);
        AddTag = (TextView) findViewById(R.id.addtag);
        Project_name = (TextView) findViewById(R.id.text_edittext_projectname);
        due_Date = (TextView) findViewById(R.id.text_edittext_date);
        txt_taskname = (EditText) findViewById(R.id.txt_taskname_in_edit_task);
        txt_description = (EditText) findViewById(R.id.txt_edittext_description);
        img_submit = (ImageView) findViewById(R.id.img_submitedittask);
        assignetask = (RelativeLayout) findViewById(R.id.relative_assign_to_task);
        img_red = (ImageView) findViewById(R.id.imgtag_red);
        img_yellow = (ImageView) findViewById(R.id.imgtag_yellow);
        img_green = (ImageView) findViewById(R.id.imgtag_green);
        addtag = (RelativeLayout) findViewById(R.id.relative_tag);
        imgcolor_tag = (RelativeLayout) findViewById(R.id.relative_imgcolor);
        Member = (TextView) findViewById(R.id.Task_Assigned_member);
    }

    private void clicklis() {


        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        addtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imgcolor_tag.setVisibility(View.VISIBLE);
            }
        });
        img_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                color = isred();
            }
        });


        img_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = isyellow();

            }
        });

        img_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = isgreen();
            }
        });

        assignetask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Create_task.this, Select_team_menber.class));
            }
        });

        AssigeToPlusImage.setOnClickListener(v->{

            startActivity(new Intent(Create_task.this, Select_team_menber.class));

        });

        AddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        image_img_submitedittask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_taskname = txt_taskname.getText().toString().trim();
                get_description = txt_description.getText().toString().trim();
                get_dueDate = due_Date.getText().toString();

                if (get_taskname.trim().equalsIgnoreCase("")) {
                    Toast.makeText(Create_task.this, "Enter Task name", Toast.LENGTH_SHORT).show();
                }

                else {
                    String userid = list[0];
                    String projectid = projectitemlist[0];
                    Createtask(userid, projectid, get_taskname, get_description, get_dueDate);
                }

            }
        });

    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void Createtask(String s, final String projectid, String get_taskname, String description, String duedate) {


        if (Network_check.isNetWorking(Create_task.this)) {

            final ProgressDialog progressDoalog;
            progressDoalog = new ProgressDialog(Create_task.this);
            progressDoalog.setTitle("Please wait..");
            progressDoalog.show();



            SelecterMember="";
            getMemberuserid="";

            if (!(TaskMember.isEmpty())) {


                for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                    getMemberuserid =  entry.getValue()+ "," + getMemberuserid;


                    if(getMemberuserid.endsWith(",")) {

                        ggetmemberid= getMemberuserid.substring(0, getMemberuserid.length() - 1);
                    }
                }

            }

            if (color==0){
                color=4;
            }
            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);

            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();
            Create_Task_Api api = rest.create(Create_Task_Api.class);
            api.CreateTask(s, projectid, get_taskname, description, "", duedate, "" + color, ggetmemberid, new Callback<CreateTaskData>() {
                @Override
                public void success(CreateTaskData createTaskData, Response response) {
                    if (createTaskData.getSuccess().equals("1")) {

                        progressDoalog.dismiss();
                        Intent intent = new Intent(Create_task.this, Task_activity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Constant.toast(Create_task.this, "" + createTaskData.getMessage());
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    progressDoalog.dismiss();
                    Constant.toast(Create_task.this, "Check connection Please   " + error.getMessage());
                }
            });

        }
        else {
            Toast.makeText(Create_task.this, " Check Connection Please", Toast.LENGTH_SHORT).show();
        }


    }


    private int isgreen() {
        if (!isGreenclicked()) {
            setGreenclicked(true);
            setRedclicked(false);
            setYellowclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.cancel_green);
            return 3;
        } else {
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;

        }

    }

    private int isyellow() {
        if (!isYellowclicked()) {
            setYellowclicked(true);
            setRedclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow);
            img_green.setImageResource(R.drawable.green_circle);
            return 2;

        } else {
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }

    private int isred() {
        if (!isRedclicked()) {
            setRedclicked(true);
            setGreenclicked(false);
            setYellowclicked(false);
            img_red.setImageResource(R.drawable.red);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 1;
        } else {
            setRedclicked(false);
            setYellowclicked(false);
            setGreenclicked(false);
            img_red.setImageResource(R.drawable.red1_circle);
            img_yellow.setImageResource(R.drawable.yellow_circle);
            img_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }

    public static boolean isRedclicked() {
        return redclicked;
    }

    public static void setRedclicked(boolean redclicked) {
        Create_task.redclicked = redclicked;
    }

    public static boolean isYellowclicked() {
        return yellowclicked;
    }

    public static void setYellowclicked(boolean yellowclicked) {
        Create_task.yellowclicked = yellowclicked;
    }

    public static boolean isGreenclicked() {
        return greenclicked;
    }

    public static void setGreenclicked(boolean greenclicked) {
        Create_task.greenclicked = greenclicked;
    }

    public static void AddTaskMember( Map <String,String> member) {

        TaskMember=member;
        for (int index=0 ; index < member.size();index++){

        }

    }

}
