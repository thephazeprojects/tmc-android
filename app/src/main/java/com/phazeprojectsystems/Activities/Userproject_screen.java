package com.phazeprojectsystems.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.phazeprojectsystems.Adapter.Userproject_adapter;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Api.Token_submit_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Fragment.Fragment_notification;
import com.phazeprojectsystems.Model.ChangeEmailData;
import com.phazeprojectsystems.Model.Dtabase_Notification_modle;
import com.phazeprojectsystems.Model.Userprojec;
import com.phazeprojectsystems.Model.UserprojectModel;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.phazeprojectsystems.SharePool.NotificationData;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class Userproject_screen extends AppCompatActivity {

    private ImageView setingicon,Usererrorimage, UserProjectconnectionerrorimage;
    private RelativeLayout userProfileImage;
    private LinearLayout UsernotificationIconLayout;
    private String[] list;
    private String user_id,company_name,user_name,user_image,token;
    private CircleImageView imageuser;
    private TextView txt_username,txt_companyname;
    private RecyclerView recyclerView;
    private Userproject_adapter userprojectadapter;
    private List<Userprojec> projectList = new ArrayList();
    private ImageView img_notification;
    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();
    private List<Dtabase_Notification_modle> newnotificationlist = new ArrayList<>();

    private SwipeRefreshLayout refreshActivity;
    private Button notificationCount;
    private Timer myTimer;
    public static int mNotifCount1 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userproject_screen);

        findview();
        clickciew();
        getproject();


        OldreadFromDatabase();
        readFromDatabase();


        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                getProject12();

            }

        }, 0, 3000);

        if (notificationlist==null){

            notificationCount.setVisibility(View.GONE);

        }
        else if (notificationlist.size()==0){
            notificationCount.setVisibility(View.GONE);
        }else
            {
            setnotificationcount(notificationlist.size());
        }


        token = FirebaseInstanceId.getInstance().getToken();


        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Token_submit_Api api=rest.create(Token_submit_Api.class);
        api.tokensubmit(token, user_id, new Callback<ChangeEmailData>() {
            @Override
            public void success(ChangeEmailData changeEmailData, Response response) {


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


        refreshActivity.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        refreshActivity.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshActivity.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshActivity.setRefreshing(false);
                        getproject();
                    }
                },3000);
            }
        });


    }


    public void setnotificationcount(int mNotifCount){
        this.mNotifCount1=notificationlist.size();
        if (mNotifCount==0){
            notificationCount.setVisibility(View.GONE);
        }else {
            this.notificationCount.setText(String.valueOf(mNotifCount));
        }
    }

    private void findview() {

        UsernotificationIconLayout=(LinearLayout)findViewById(R.id.UsernotificationIconLayout);
        notificationCount=(Button)findViewById(R.id.notif_count1);
        list = MySharePrafranceClass.GetSharePrefrance(Userproject_screen.this);
        txt_companyname= (TextView) findViewById(R.id.company_name);
        txt_username= (TextView) findViewById(R.id.username);
        imageuser= (CircleImageView) findViewById(R.id.user_image);
        recyclerView= (RecyclerView) findViewById(R.id.project_name_list);
        userProfileImage=(RelativeLayout)findViewById(R.id.userProfileImage);
        img_notification=(ImageView)findViewById(R.id.user_img_notification);
        refreshActivity=(SwipeRefreshLayout)findViewById(R.id.user_project_name_list_refresh);
        Usererrorimage=(ImageView) findViewById(R.id.Usererrorimage);
        UserProjectconnectionerrorimage=(ImageView) findViewById(R.id.UserProjectconnectionerrorimage);


        user_id = list[0];
        user_name=list[1];
        company_name = list[2];
        user_image=list[3];

        txt_companyname.setText(company_name);
        txt_username.setText(user_name);


        if (!(user_image == null)) {
            if (!(user_image.equals(""))){
                String image = Constant.picuri.concat(user_image);
                Glide.with(this).load(image).fitCenter().centerCrop().into(imageuser);}else{}
        } else {

        }



    }
    private void clickciew() {

        UsernotificationIconLayout.setOnClickListener((View v)->{
            setnotificationcount(0);
            OldreadFromDatabase();
            readFromDatabase();
            Fragment_notification fragment_notification_Dialog  = new Fragment_notification(notificationlist,newnotificationlist);
            fragment_notification_Dialog.show(getSupportFragmentManager(),"tag");
        });

        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Userproject_screen.this, Container_account_activity.class);
                startActivity(intent);
            }
        });

    }

    private void getproject() {

        if (Network_check.isNetWorking(Userproject_screen.this)) {

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);


            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();

            Get_User_Project Api = rest.create(Get_User_Project.class);

            Api.Getuserproject(user_id, new Callback<UserprojectModel>() {
                @Override
                public void success(UserprojectModel userprojectModel, Response response) {

                    if (userprojectModel.getSuccess() == 1) {
                        UserProjectconnectionerrorimage.setVisibility(View.GONE);

                        Usererrorimage.setVisibility(View.GONE);
                        if (!userprojectModel.getRecords().equals("0")) {
                            projectList = userprojectModel.getUserproject();
                            recyclerView.setVisibility(View.VISIBLE);
                            RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Userproject_screen.this);
                            recyclerView.setLayoutManager(mlayoutManeger);
                            userprojectadapter = new Userproject_adapter(Userproject_screen.this, projectList);
                            recyclerView.setAdapter(userprojectadapter);
                            userprojectadapter.notifyDataSetChanged();

                        }
                    } else {
                        UserProjectconnectionerrorimage.setVisibility(View.GONE);

                        Usererrorimage.setVisibility(View.VISIBLE);
                        Toast.makeText(Userproject_screen.this, "No Any Task Assign to You", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getMessage().contains("phaze.in.gl")) {

                        UserProjectconnectionerrorimage.setVisibility(View.VISIBLE);
                    }
                }
            });
        }else {
            Toast.makeText(Userproject_screen.this, "Please Check Connection .. !", Toast.LENGTH_SHORT).show();
            UserProjectconnectionerrorimage.setVisibility(View.VISIBLE);
        }
    }

    private void getProject12(){

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);


        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Get_User_Project Api = rest.create(Get_User_Project.class);

        Api.Getuserproject(user_id, new Callback<UserprojectModel>() {
            @Override
            public void success(UserprojectModel userprojectModel, Response response) {

                UserProjectconnectionerrorimage.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage().contains("phaze.in.gl")){

                    UserProjectconnectionerrorimage.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void OldreadFromDatabase(){

        NotificationData notificationData = new NotificationData(Userproject_screen.this);

        SQLiteDatabase database = notificationData.getWritableDatabase();

        Cursor cursor = notificationData.GetNotification(database);

        if (cursor.getCount()>0){

            newnotificationlist.clear();

            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex(NotificationData.COLUMN_id));
                String msg=cursor.getString(cursor.getColumnIndex(NotificationData.COLUMN_MSG));
                String time=cursor.getString(cursor.getColumnIndex(NotificationData.TIMEOFNOTIFICATION));

                String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.gettaskname));
                String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.getduwdate));
                String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.getdescription));


                String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.gettaskid));
                String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.getaddtag));

                newnotificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

            }

        }

    }

    private void readFromDatabase(){

        NotificationData notificationData = new NotificationData(Userproject_screen.this);

        SQLiteDatabase database = notificationData.getWritableDatabase();

        Cursor cursor = notificationData.NewGetNotification(database);

        if (cursor.getCount()>0){

            notificationlist.clear();
            while (cursor.moveToNext()){
                int id = cursor.getInt(cursor.getColumnIndex(NotificationData.NEWCOLUMN_id));
                String msg=cursor.getString(cursor.getColumnIndex(NotificationData.NEWCOLUMN_MSG));
                String time=cursor.getString(cursor.getColumnIndex(NotificationData.NEWTIMEOFNOTIFICATION));

                String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskname));
                String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetduwdate));
                String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetdescription));


                String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskid));
                String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetaddtag));


                notificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

            }

        }

    }

}
