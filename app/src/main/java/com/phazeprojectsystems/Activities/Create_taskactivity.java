package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.phazeprojectsystems.Api.Create_Task_Api;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CreateTaskData;
import com.phazeprojectsystems.Model.ProjectList;
import com.phazeprojectsystems.Model.Project_Data;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Create_taskactivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView createtask,Task_name;
    private TextView Discription,task,forName;
    private ImageView Create_task_calender;
    private Spinner inProject;
    private int Year_X,Month_X,Day_X;
    private static  final  int DIALOG_ID=1;
    private String[] list;
    private List<ProjectList> project = new ArrayList();
    private final static Hashtable<String,String> projectname = new Hashtable<>();
    private ImageView Attachment;
    private static final int REQ_CODE=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_taskactivity);
        list = MySharePrafranceClass.GetSharePrefrance(Create_taskactivity.this);

        FindView();
        forName.setText(list[1]);
        GetProject(list[0]);
        createtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userid=list[0];
                String description = Discription.getText().toString();
                String name = inProject.getSelectedItem().toString();
                String projectid = projectname.get(name);
                String task_name = Task_name.getText().toString();
                String dueDate = getDay_X()+"/"+getMonth_X()+"/"+getYear_X();
                if(task_name.trim().equals("")) {Constant.toast(Create_taskactivity.this,"Please Enter task First");}
                else if(description.trim().equals("")){Constant.toast(Create_taskactivity.this,"Please Enter Task_Description");}
                else if(projectname.isEmpty()){Constant.toast(Create_taskactivity.this,"Select Project Plz");}
                else{
                    String createdate = Calendar.getInstance().getTime().toString();
                    Date createdate1 = Calendar.getInstance().getTime();
                    int  date = createdate1.getDate();
                    int month = createdate1.getMonth();
                    int  year = createdate1.getYear();
                    Createtask(userid,projectid,task_name,description,createdate,dueDate);
                     }

            }
        });
        Create_task_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 showDialog(DIALOG_ID);
            }
        });
        Attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("\"application/pdf\",\"application/msword\",\"application/vnd.ms-powerpoint\",\"application/vnd.ms-excel\",\"text/plain");
                startActivityForResult(intent,REQ_CODE);
            }
        });
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id==DIALOG_ID){return  new DatePickerDialog(Create_taskactivity.this,datepickerlistner,Year_X,Month_X,Day_X);}else{return null;}
    }
    private DatePickerDialog.OnDateSetListener datepickerlistner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            setYear_X(year);
            setMonth_X(month);
            setDay_X(dayOfMonth);
            Constant.toast(Create_taskactivity.this,"Year is :"+getDay_X() +"\n Month is :"+getMonth_X()+"\n Day is : "+getDay_X());
        }
    };
    private void Createtask(String userid, String projectid, String task_name , String description, String createdate,String dueDate) {
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Create_Task_Api api = rest.create(Create_Task_Api.class);
        api.CreateTask(userid,projectid,task_name,description,createdate,dueDate,"","",new Callback<CreateTaskData>() {
            @Override
            public void success(CreateTaskData createTaskData, Response response) {
               String status = createTaskData.getSuccess();
                if(status.equals("1")){
                    Task_name.setText("");
                    Discription.setText("");
                    Intent intent= new Intent(Create_taskactivity.this,MainActivity.class);
                    intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                else{
                    Constant.toast(Create_taskactivity.this,""+createTaskData.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                               Constant.toast(Create_taskactivity.this,error.getMessage());
            }
        });
    }

    void FindView()
    {
        Attachment=(ImageView)findViewById(R.id.attachment_newtask);
        Create_task_calender=(ImageView)findViewById(R.id.calender_newtask);
        inProject=(Spinner)findViewById(R.id.In_project);
        forName=(TextView)findViewById(R.id.For_name);
        createtask= (TextView) findViewById(R.id.create_newtask);
        Task_name=(TextView)findViewById(R.id.task_name);
        Discription=(TextView)findViewById(R.id.add_description);
        inProject.setPrompt("Select---Project");
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQ_CODE){if(resultCode==RESULT_OK){
           Uri dat =  data.getData();
            String Path = dat.getPath();
            Toast.makeText(this, "Working, Path is = "+Path, Toast.LENGTH_LONG).show();
        }}
    }

    private void GetProject(String userid){
       RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
       Get_User_Project Api= rest.create(Get_User_Project.class);
       Api.GetProject(userid, new Callback<Project_Data>() {
           @Override
           public void success(Project_Data project_data, Response response) {
               String status=project_data.getSuccess();
               if(status.equals("1")){

                   project = project_data.getProjectList();
                   for(int i= 0;i<project.size();i++){

                       String Nameofproject = project.get(i).getProjactName();
                       String Idofproject = project.get(i).getProjectId();
                       projectname.put(Nameofproject,Idofproject);
                                      //----key---------value----//
                   }
                    TreeSet<String> Arry = new TreeSet<>(projectname.keySet());
                   ArrayAdapter projectarrayAdapter= new ArrayAdapter(Create_taskactivity.this,R.layout.support_simple_spinner_dropdown_item,Arry.toArray());
                   projectarrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                   inProject.setAdapter(projectarrayAdapter);
               }else{
                   Constant.toast(Create_taskactivity.this,""+project_data.getMessage());
               }
           }

           @Override
           public void failure(RetrofitError error) {
              Constant.toast(Create_taskactivity.this,"Check Connectrion"+error.getMessage());
           }
       });

    }

    public int getYear_X() {
        return Year_X;
    }

    public void setYear_X(int year_X) {
        this.Year_X = year_X;
    }

    public int getMonth_X() {
        return Month_X;
    }

    public void setMonth_X(int month_X) {
        this.Month_X = month_X;
    }

    public int getDay_X() {
        return Day_X;
    }

    public void setDay_X(int day_X) {
        this.Day_X = day_X;
    }
}
