package com.phazeprojectsystems.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.phazeprojectsystems.Api.Register_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.RegisterData;
import com.phazeprojectsystems.R;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Register_screen extends AppCompatActivity {

    private Button signup;
    private EditText user,emailId,password,confirmPassword,company;
    private  String User,Email,pass,confpass,comp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);

        FINDVIEWBYID();
        CLICKLISTINER();
    }


    private void FINDVIEWBYID() {

        signup = (Button) findViewById(R.id.sign_up);
        user = (EditText) findViewById(R.id.User);
        emailId = (EditText) findViewById(R.id.Email);
        password = (EditText) findViewById(R.id.Password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        company = (EditText) findViewById(R.id.company);

    }



    private void CLICKLISTINER() {

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                 User = user.getText().toString();
                 Email = emailId.getText().toString();
                 pass = password.getText().toString();
                 confpass = confirmPassword.getText().toString();
                 comp = company.getText().toString();


                 if (!validate()){
                     Toast.makeText(Register_screen.this, "Your Request Has Failed", Toast.LENGTH_SHORT).show();
                 }else {
                     Registeruser(User, Email, comp, pass, confpass, "", "");
                 }
            }
        });
    }

    private void Registeruser(final String user, String email, String comp, String pass, String confpass, String facebookId, String googleid) {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Register_Api api = adapter.create(Register_Api.class);
        api.getregister(user, email, comp, pass, confpass, facebookId, googleid, new Callback<RegisterData>() {
            @Override
            public void success(RegisterData registerData, Response response) {
                String status = registerData.getSuccess();
                if (status.equals("1")) {
                    String userid = registerData.getUserid();


                    Toast.makeText(Register_screen.this, "" + registerData.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Register_screen.this, Login_screen.class);
                    intent.putExtra("name", user);
                    startActivity(intent);
                    finish();
                } else {
                    Constant.toast(Register_screen.this, "" + registerData.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(Register_screen.this, "Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onBackPressed() {
        Intent intent = new Intent(Register_screen.this, Main_screen.class);
        startActivity(intent);
        super.onBackPressed();

    }

    private boolean validate(){
        boolean valid=true;

        if (User.isEmpty()){
            user.setError("Enter Valid User Name");
            valid=false;
        }
        if (Email.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(Email).matches()){
            emailId.setError("Enter Valid Email");
            valid=false;
        }
        if (pass.isEmpty()||pass.length()<6){
            password.setError("Enter Valid Password");
            valid=false;
        }
        if (pass.isEmpty()||confpass.isEmpty()||!pass.equals(confpass)||confpass.length()<6){
            confirmPassword.setError("Enter Valid Comfirm Password");
            valid=false;
        }
        if (comp.isEmpty()){
            company.setError("Enter Valid Company Name");
            valid=false;
        }

        return valid;
    }
}
