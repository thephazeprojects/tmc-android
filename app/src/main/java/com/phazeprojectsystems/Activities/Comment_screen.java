package com.phazeprojectsystems.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Adapter.AllComment_adapter;
import com.phazeprojectsystems.Adapter.Comment_adapter;
import com.phazeprojectsystems.Adapter.DialogAssignUserAdapter;
import com.phazeprojectsystems.Adapter.DialogUNAssignUserAdapter;
import com.phazeprojectsystems.Api.Comment_api;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.DeleteTask_Api;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.AllcommentModel;
import com.phazeprojectsystems.Model.AssigneeByTask_Result_model;
import com.phazeprojectsystems.Model.AssigneeByTask_model;
import com.phazeprojectsystems.Model.CommentLis;
import com.phazeprojectsystems.Model.CommentModel;
import com.phazeprojectsystems.Model.ComntLis;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.DeleteTaskData;
import com.phazeprojectsystems.Model.GetAssigneUser_Result_modle;
import com.phazeprojectsystems.Model.Get_Assigned_User_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
import static com.phazeprojectsystems.R.id.comment_fileattachment;

public class Comment_screen extends AppCompatActivity {

    private ImageView send_comment, backarrow;
    private String SelecterMember="";
    private String getMemberuserid="";
    RelativeLayout edittask;
    TextView txt_UpdateTime,AssiedUserView,txt_taskname, txt_createdby, txt_duedate, txt_description;
    EditText txt_comment;
    RecyclerView recycler;
    public String gettaskname, getduedate, getdescription, getcomment, CompanyName,gettaskid,comingintent,getmember, userid, project_id, get_projectid,getUpdatetime,getadd_tag;
    private RelativeLayout fileattachemnt, task_complete ,search;
    private LinearLayout progress;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Uri fileUri;
    private static String userChoosenTask, getuser_name, get_user_image;
    private static String picturePath;
    private String[] list;
    private String[] projectItemList;
    List<ComntLis> comntLists = new ArrayList<ComntLis>();
    List<CommentLis> comntList2 = new ArrayList<CommentLis>();
    AllComment_adapter commentadapter;
    Comment_adapter commentadapter2;
    CircleImageView img_user;
    private List<GetAssigneUser_Result_modle> assigneuser;
    private Map<String,String>taskMember =  new HashMap<>();
    public static  Comment_screen comment_screen;
    private RecyclerView recycle;
    private LinearLayoutManager linearLayoutManager;
    private DialogAssignUserAdapter dialogassignUserAdapter;
    private AlertDialog alertDialog;
    private SwipeRefreshLayout refreshcomment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_screen);

        Intent extra = getIntent();

        if (extra!=null) {

            gettaskname = extra.getStringExtra("gettaskname");
            getduedate = extra.getStringExtra("getdewdate");
            getdescription = extra.getStringExtra("getdescription");
            gettaskid = extra.getStringExtra("gettaskid");
            getadd_tag = extra.getStringExtra("getaddtag");
            getUpdatetime = extra.getStringExtra("updatetime");
            getmember = extra.getStringExtra("member");
            comingintent=  extra.getStringExtra("comingintent");
//            String[]  Member  = getmember.split("\\s*,\\s*");
            if (gettaskid!=null)
            {
                getTaskAssignedUser(gettaskid);
            }


        }

        comment_screen=this;
        findview();
        ShareObject();
        clicklistiner();
        getandsetdata();
        allcoments();
        CompanyName = list[2];

        refreshcomment.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        refreshcomment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshcomment.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshcomment.setRefreshing(false);
                        allcoments();
                    }
                },1000);
            }
        });
    }

    public Map<String, String> getTaskMember() {
        return taskMember;
    }

    public void setTaskMember(Map<String, String> taskMember) {
        this.taskMember = taskMember;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (gettaskid!=null)
        {
            getTaskAssignedUser(gettaskid);
        }
    }

    private void getTaskAssignedUser(String Taskid){
    RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
    User_by_Company_Api api = rest.create(User_by_Company_Api.class);
    api.getAssignedUser(Taskid, new Callback<Get_Assigned_User_modle>() {
        @Override
        public void success(Get_Assigned_User_modle get_assigned_user_modle, Response response) {
            if (get_assigned_user_modle.getSuccess().equals("1")){

                assigneuser = get_assigned_user_modle.getResult();


                if (assigneuser!=null) {

                    for (int i = 0; i < assigneuser.size(); i++) {

                        for (int j = i + 1; j < assigneuser.size(); j++) {

                            if (assigneuser.get(i) != null && assigneuser.get(j).getId() != null) {
                                if (assigneuser.get(i).getId().equals(assigneuser.get(j).getId())) {
                                    assigneuser.remove(j);
                                    j--;
                                }

                            }
                        }

                    }

                }


                for (GetAssigneUser_Result_modle e:assigneuser){
                    if (e!=null) {


                        taskMember.put(e.getUsername(),e.getId());

                 if (!getMemberuserid.contains(e.getId())) {

                     SelecterMember = e.getUsername() + "," + SelecterMember;
                     getMemberuserid = e.getId() + "," + getMemberuserid;
                 }

                    setTaskMember(taskMember);
                    }
                }

            }

        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(Comment_screen.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    });

}
    private void findview() {

        progress = (LinearLayout)findViewById(R.id.progress);
        send_comment = (ImageView) findViewById(R.id.send_comment);
        txt_taskname = (TextView) findViewById(R.id.txt_commenttaskname);
        txt_createdby = (TextView) findViewById(R.id.txt_comment_createdby);
        txt_duedate = (TextView) findViewById(R.id.txt_commentduedate);
        txt_description = (TextView) findViewById(R.id.comment_description);
        txt_comment = (EditText) findViewById(R.id.txt_comment);
        recycler = (RecyclerView) findViewById(R.id.comment_listiview);
        refreshcomment=(SwipeRefreshLayout)findViewById(R.id.refreshcomment);
        fileattachemnt = (RelativeLayout) findViewById(comment_fileattachment);
        edittask = (RelativeLayout) findViewById(R.id.edit_task_icon);
        img_user = (CircleImageView) findViewById(R.id.user_icon);
        task_complete = (RelativeLayout) findViewById(R.id.task_complete_icon);
        search  = (RelativeLayout) findViewById(R.id.search);
        backarrow = (ImageView) findViewById(R.id.backarrow_Button_commentscreen);
        txt_UpdateTime = (TextView) findViewById(R.id.Task_CompleteStatus);
        AssiedUserView = (TextView) findViewById(R.id.AssiedUserView);
    }

    private void ShareObject() {
        list = MySharePrafranceClass.GetSharePrefrance(Comment_screen.this);
        projectItemList = MySharePrafranceClass.ProjectGetSharePrefrance(Comment_screen.this);
    }

    private void getandsetdata() {

        getuser_name = list[1];
        get_user_image = list[3];
        userid = list[0];

        txt_taskname.setText(gettaskname);
        txt_duedate.setText(getduedate);
        txt_description.setText(getdescription);
        txt_createdby.setText(getuser_name);
        txt_UpdateTime.setText(getUpdatetime);



        if (!(get_user_image==null)) {

            if (!get_user_image.equalsIgnoreCase("")) {
                String image= Constant.picuri.concat(get_user_image);
                Glide.with(this).load(image).into(img_user);
            }
        }
        else {
            Glide.with(this).load(R.drawable.user_icn).into(img_user);
        }
        CharSequence Current_date = currentDate().trim();
        char first = Current_date.charAt(0);
        String duedate = getduedate.trim();

        String cur_date = Current_date.toString();

        if (duedate.trim().equals(cur_date))
        {
            txt_duedate.setTextColor(Color.RED);
            txt_duedate.setText("Today");
        }
        else {
            txt_duedate.setTextColor(Color.BLACK);
        }

    }

    private void clicklistiner() {

        search.setOnClickListener(v->{

            RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
            DeleteTask_Api api=rest.create(DeleteTask_Api.class);
            api.Delete(gettaskid, new Callback<DeleteTaskData>() {
                @Override
                public void success(DeleteTaskData deleteTaskData, Response response) {


                    String status=deleteTaskData.getSuccess();

                    if (status.equalsIgnoreCase("1"))

                    {
                        finish();
                        Toast.makeText(Comment_screen.this, " Task  Delete Sucessfully ", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });


        });


        AssiedUserView.setOnClickListener(v->{

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Comment_screen.this,R.style.DialogTheme);
            LayoutInflater inflater = Comment_screen.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialogview, null);
            recycle = (RecyclerView) dialogView.findViewById(R.id.recycleViewides);

            if (assigneuser!=null) {
                if (!assigneuser.isEmpty())
                {
                    linearLayoutManager = new LinearLayoutManager(Comment_screen.this, LinearLayoutManager.VERTICAL, false);
                    recycle.setLayoutManager(linearLayoutManager);
                    dialogassignUserAdapter = new DialogAssignUserAdapter(Comment_screen.this, assigneuser,"CommentScreen");
                    recycle.setAdapter(dialogassignUserAdapter);
                    dialogassignUserAdapter.notifyDataSetChanged();
                }
            }



            TextView addmorassgin=(TextView)dialogView.findViewById(R.id.addmorassgin);
            addmorassgin.setOnClickListener(view -> {




                RestAdapter rest =  new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                User_by_Company_Api api = rest.create(User_by_Company_Api.class);
                api.AssigneeUserBuyTask(gettaskid, CompanyName, new Callback<AssigneeByTask_model>() {
                    @Override
                    public void success(AssigneeByTask_model assigneeByTask_model, Response response) {
                        List<AssigneeByTask_Result_model> unassignee = assigneeByTask_model.getResult();


                        if( unassignee!=null){

                            if (!unassignee.isEmpty()) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Comment_screen.this, LinearLayoutManager.VERTICAL, false);
                                recycle.setLayoutManager(linearLayoutManager);
                                DialogUNAssignUserAdapter dialogunassignUserAdapter = new DialogUNAssignUserAdapter(Comment_screen.this, unassignee,"CommentScreen",project_id);
                                recycle.setAdapter(dialogunassignUserAdapter);
                                dialogunassignUserAdapter.notifyDataSetChanged();
                            }

                        }else {
                            Toast.makeText(Comment_screen.this, "No More  Assignee Have .", Toast.LENGTH_SHORT).show();
                        }



                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }

                });

            });

            dialogBuilder.setTitle("Appointed to : ");
            dialogBuilder.setView(dialogView);
            alertDialog = dialogBuilder.create();
            alertDialog.show();


        });


        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getcomment = txt_comment.getText().toString().trim();

                if (getcomment.trim().equalsIgnoreCase("")) {
                    Toast.makeText(Comment_screen.this, "Enter Comment", Toast.LENGTH_SHORT).show();
                } else {
                    final String picture = null;
                    Comment_register(getcomment, picture);
                }

            }
        });


        txt_comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_SEND) {

                    String comment = txt_comment.getText().toString().trim();

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
                    Date currentLocalTime = cal.getTime();
                    DateFormat date = new SimpleDateFormat("HH:mm a");
                    date.setTimeZone(TimeZone.getTimeZone("Canada/Pacific"));

                    String localTime = date.format(currentLocalTime);


                    if (comment.trim().equalsIgnoreCase("")) {
                        Toast.makeText(Comment_screen.this, "Enter Comment", Toast.LENGTH_SHORT).show();
                    } else {
                        final String picture = null;
                        Comment_register(comment, picture);
                    }

                }

                return false;
            }
        });

        edittask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Comment_screen.this, Edit_task.class);
                intent.putExtra("gettaskname", gettaskname);
                intent.putExtra("getdewdate", getduedate);
                intent.putExtra("getdescription", getdescription);
                intent.putExtra("gettaskid", gettaskid);
                intent.putExtra("getuserid", userid);
                intent.putExtra("getprojectid", get_projectid);
                intent.putExtra("getaddtag", getadd_tag);
                intent.putExtra("assigedusername",SelecterMember);
                intent.putExtra("assigneIds", getMemberuserid);
                startActivity(intent);
            }
        });


        fileattachemnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();
            }
        });


        task_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String time  = sdf.format(cal.getTime());


                RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                Complete_Api api = rest.create(Complete_Api.class);

                api.Complete(userid, gettaskid,time,new Callback<CompleteTaskModel>() {
                    @Override
                    public void success(CompleteTaskModel completeTaskModel, Response response) {

                        if (completeTaskModel.getSuccess().equalsIgnoreCase("1")) {
                            Toast.makeText(Comment_screen.this, "Task Completed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Toast.makeText(Comment_screen.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    private void allcoments() {
        userid = list[0];
        get_projectid = projectItemList[0];

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Comment_api api = rest.create(Comment_api.class);

        api.all_comments(userid, gettaskid, get_projectid, new Callback<AllcommentModel>() {
            @Override
            public void success(AllcommentModel allcommentModel, Response response) {

                Long status = allcommentModel.getSuccess();

                if (status == 1) {
                    Toast.makeText(Comment_screen.this, "Status 1", Toast.LENGTH_SHORT).show();


                    comntLists = allcommentModel.getComntList();

                    recycler.setVisibility(View.VISIBLE);
                    commentadapter = new AllComment_adapter(Comment_screen.this, comntLists);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(Comment_screen.this);
                    mLayoutManager.setReverseLayout(true);
                    recycler.setLayoutManager(mLayoutManager);
                    recycler.setAdapter(commentadapter);
                    commentadapter.notifyDataSetChanged();

                }
                else {

                    Toast.makeText(Comment_screen.this, "Status 0", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private String currentDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        String todayAsString = dateFormat.format(today);

        return todayAsString;
    }

    private void Comment_register(String get_comment, String UploadedFile1) {

        project_id = projectItemList[0];

        TypedFile UploadedFile = null;


        if (UploadedFile1 != null) {
            UploadedFile = new TypedFile("image/*", new File(String.valueOf(UploadedFile1)));
            progress.setVisibility(View.VISIBLE);

        }

        txt_comment.setText("");

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Comment_api api = rest.create(Comment_api.class);

        api.comment(userid,gettaskid, project_id, get_comment, UploadedFile, new Callback<CommentModel>() {
            @Override
            public void success(CommentModel commentModel, Response response) {


                Long status = commentModel.getSuccess();

                progress.setVisibility(View.GONE);

                if (status == 1) {

                    Toast.makeText(Comment_screen.this, "Sucessfull", Toast.LENGTH_SHORT).show();

                    comntList2 = commentModel.getCommentList();
                    recycler.setVisibility(View.VISIBLE);
                    commentadapter2 = new Comment_adapter(Comment_screen.this,comntList2);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(Comment_screen.this);
                    mLayoutManager.setReverseLayout(true);
                    recycler.setLayoutManager(mLayoutManager);
                    comntLists.clear();
                    recycler.setAdapter(commentadapter2);
                    commentadapter2.notifyDataSetChanged();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                progress.setVisibility(View.GONE);

                Toast.makeText(Comment_screen.this, "Failureee :-  " + error, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.phazeprojectsystems.Constant.Utility.checkPermission(Comment_screen.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = FileProvider.getUriForFile(Comment_screen.this,
                "com.phazeprojectsystems.FileProvider",
                getOutputMediaFile(MEDIA_TYPE_IMAGE));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {

            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        } else {
            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }


        startActivityForResult(intent, REQUEST_CAMERA);



//        if (file_navigation != null) {
//
//            photofile_navigation = FileProvider.getUriForFile(Home_screen.this, BuildConfig.APPLICATION_ID + ".provider", file_navigation);
//
//            cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, photofile_navigation);
//
//
//
//            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
//
//                cameraintent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//
//            } else {
//                List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraintent, PackageManager.MATCH_DEFAULT_ONLY);
//                for (ResolveInfo resolveInfo : resInfoList) {
//                    String packageName = resolveInfo.activityInfo.packageName;
//                    Home_screen.this.grantUriPermission(packageName, photofile_navigation,
//                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                }
//            }
//
//
//            startActivityForResult(cameraintent, CAMERACODE);
//        }

    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, SELECT_FILE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {

                Comment_register("", picturePath);
            }

        } else if (requestCode == SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                onSelectFromGalleryResult(data);

                Comment_register("", picturePath);

            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


        }
    }


    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/.PhazeSystem/");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }
        File mediaFile;
        java.util.Date date = new java.util.Date();
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "IMG_" + new Timestamp(date.getTime()).toString() + ".jpg");
            picturePath = mediaFile.getAbsolutePath();

        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "VID_" + new Timestamp(date.getTime()).toString() + ".mp4");
        } else {
            return null;
        }

        return mediaFile;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (comingintent!=null) {

            if (comingintent.equals("notification")) {

                startActivity(new Intent(Comment_screen.this, MainActivity.class));
            } else {
                finish();
            }
        }else {

              startActivity(new Intent(Comment_screen.this, MainActivity.class));

        }
    }
}
