package com.phazeprojectsystems.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Adapter.GetCompleteTaskAdapter;
import com.phazeprojectsystems.Adapter.Get_In_CompleteTaskAdapter;
import com.phazeprojectsystems.Adapter.Usertask_adapter;
import com.phazeprojectsystems.Api.GetCompleteTask_Api;
import com.phazeprojectsystems.Api.Get_InCompletTask_Api;
import com.phazeprojectsystems.Api.ProjectAllTask_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.Completetasklis;
import com.phazeprojectsystems.Model.Completetasklistmodel;
import com.phazeprojectsystems.Model.InCompletetasklistmodel;
import com.phazeprojectsystems.Model.Pendingtasklis;
import com.phazeprojectsystems.Model.Usertasklis;
import com.phazeprojectsystems.Model.Usertasklistmodel;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class User_taskscreen extends AppCompatActivity {

    private ImageView backarrow,Usertaskconnectionerrorimage,Usertaskerrorimage;
    private RelativeLayout useroption_icon , ActivePhaseTextUserScreen;
    private TextView companyname;
    private RecyclerView recyclerView;
    private Usertask_adapter usertask_adapter;
    private GetCompleteTaskAdapter completetaskadapter;
    private Get_In_CompleteTaskAdapter incompleteadapter;
    private String[] itemlist;
    private Timer myTimer;
    private String user_id, compnay_name;
    private List<Usertasklis> projecttask = new ArrayList<>();
    private List<Completetasklis> completetask=new ArrayList<>();
    private List<Pendingtasklis> pendinglist=new ArrayList<>();
    private String[] list;
    private String getprojectid,getprojectname;
    private SwipeRefreshLayout usertaskRefreshActivity;
    private Integer ActivePhase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_taskscreen);
        getprojectid = getIntent().getExtras().getString("projectid");
        getprojectname = getIntent().getExtras().getString("projectname");

        findbyid();

        user_id = list[0];
        compnay_name = list[2];

        clickview();

        companyname.setText(getprojectname);

        tasklist();
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                GetCompletTask(user_id);
                GetInCompletTask(user_id);

            }

        }, 0, 3000);




        usertaskRefreshActivity.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        usertaskRefreshActivity.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                usertaskRefreshActivity.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        usertaskRefreshActivity.setRefreshing(false);
                        if (getActivePhase()==null){
                            setActivePhase( R.id.In_completeTask);
                        }

                        switch (getActivePhase()) {

                            case R.id.Complete_task:
                                if (completetask!=null) {setprojectCompleteTaskAdapter();}
                                break;

                            case R.id.In_completeTask:
                                if (pendinglist!=null) {setProjectIncompleteTaskAdapter();}
                                break;

                            case R.id.Task_by_due_date:

                                break;

                            default:
                                break;
                        }
                    }
                },3000);
            }
        });

    }

    private void findbyid() {
        Usertaskconnectionerrorimage =  (ImageView) findViewById(R.id.Usertaskconnectionerrorimage);
        list = MySharePrafranceClass.GetSharePrefrance(User_taskscreen.this);
        backarrow= (ImageView) findViewById(R.id.backarrow_usertaskscreen);
        Usertaskerrorimage=(ImageView) findViewById(R.id.Usertaskerrorimage);
        companyname = (TextView) findViewById(R.id.user_Project_name);
        recyclerView = (RecyclerView) findViewById(R.id.user_recyclerview_tasklist);
        useroption_icon = (RelativeLayout) findViewById(R.id.user_option_icon);

        ActivePhaseTextUserScreen = (RelativeLayout) findViewById(R.id.ActivePhaseTextUserScreen);
        usertaskRefreshActivity=(SwipeRefreshLayout)findViewById(R.id.user_taskRefreshActivity);
    }

    private void clickview() {

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });


        ActivePhaseTextUserScreen.setOnClickListener(v->{

            tasklist();

        });

        useroption_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(User_taskscreen.this, useroption_icon);
                popupMenu.getMenuInflater().inflate(R.menu.menu_main, popupMenu.getMenu());
                popupMenu.getMenu().getItem(2).setVisible(false);
                if (completetask!=null) {
                    popupMenu.getMenu().getItem(0).setTitle("Completed Task   (" + completetask.size() + ")");
                }else {
                    popupMenu.getMenu().getItem(0).setTitle("Completed Task   (" + 0 + ")");
                }
                if (pendinglist!=null) {
                    popupMenu.getMenu().getItem(1).setTitle("Incomplete Task  (" + pendinglist.size() + ")");
                }else {
                    popupMenu.getMenu().getItem(1).setTitle("Incomplete Task  (" + 0 + ")");
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String[] list = MySharePrafranceClass.GetSharePrefrance(User_taskscreen.this);
                        switch (item.getItemId()) {
                            case R.id.Complete_task:
                                setActivePhase(R.id.Complete_task);
                                if (completetask!=null) {
                                    if (completetask.isEmpty()){
                                        Usertaskerrorimage.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        Usertaskerrorimage.setVisibility(View.GONE);
                                        setprojectCompleteTaskAdapter();
                                    }
                                }
                                break;
                            case R.id.In_completeTask:
                                setActivePhase(R.id.In_completeTask);
                                if (pendinglist!=null) {

                                    if (pendinglist.isEmpty()){
                                        Usertaskerrorimage.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        Usertaskerrorimage.setVisibility(View.GONE);
                                        setProjectIncompleteTaskAdapter();
                                    }
                                }
                                break;
                         case R.id.Task_by_due_date:
//                                Toast.makeText(User_taskscreen.this, "You Click on  " + item.getTitle(), Toast.LENGTH_SHORT).show();
                                break;
                            default:
//                                Toast.makeText(User_taskscreen.this, "You Click on  " + item.getTitle(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }

    private void GetInCompletTask(String user_id)
    {

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Get_InCompletTask_Api api = rest.create(Get_InCompletTask_Api.class);

        api.GetInCompletTask(user_id,getprojectid, new Callback<InCompletetasklistmodel>() {
            @Override
            public void success(InCompletetasklistmodel inCompletetasklistmodel, Response response) {



                if (inCompletetasklistmodel.getSuccess() == 1)
                {
                    Usertaskconnectionerrorimage.setVisibility(View.GONE);
                    projecttask.clear();
                    completetask.clear();
                    pendinglist= inCompletetasklistmodel.getPendingtasklist();

                }
                else
                    {
                    Usertaskconnectionerrorimage.setVisibility(View.GONE);
                  }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void GetCompletTask(String user_id) {

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        GetCompleteTask_Api api = rest.create(GetCompleteTask_Api.class);

        api.get_complete_task(user_id,getprojectid, new Callback<Completetasklistmodel>() {
            @Override
            public void success(Completetasklistmodel completetasklistmodel, Response response) {

                if (completetasklistmodel.getSuccess() == 1) {
                    Usertaskconnectionerrorimage.setVisibility(View.GONE);


                    projecttask.clear();
                    completetask = completetasklistmodel.getCompletetasklist();

                } else {
                    Usertaskconnectionerrorimage.setVisibility(View.GONE);
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }


    private void tasklist() {

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        ProjectAllTask_Api api = rest.create(ProjectAllTask_Api.class);

        api.usertasklist(user_id,getprojectid, new Callback<Usertasklistmodel>() {
            @Override
            public void success(Usertasklistmodel usertasklistmodel, Response response) {


                if (usertasklistmodel.getSuccess() == 1) {


                    completetask.clear();
                    projecttask.clear();
                    pendinglist.clear();

                    projecttask = usertasklistmodel.getUsertasklist();
                    Usertaskconnectionerrorimage.setVisibility(View.GONE);

                    Usertaskerrorimage.setVisibility(View.GONE);

                    recyclerView.setVisibility(View.VISIBLE);
                    RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(User_taskscreen.this, LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(mlayoutManeger);
                    usertask_adapter = new Usertask_adapter(User_taskscreen.this, projecttask);
                    recyclerView.setAdapter(usertask_adapter);
                    usertask_adapter.notifyDataSetChanged();
                }
                else {

                    Usertaskerrorimage.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage().contains("phaze.in.gl")){
                    Usertaskconnectionerrorimage.setVisibility(View.VISIBLE);

                }


            }
        });

    }


    //===============================================================================================================================================


    private void setProjectIncompleteTaskAdapter() {
        recyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(User_taskscreen.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mlayoutManeger);
        incompleteadapter = new Get_In_CompleteTaskAdapter(User_taskscreen.this, pendinglist);
        recyclerView.setAdapter(incompleteadapter);
        incompleteadapter.notifyDataSetChanged();
    }

    private void setprojectCompleteTaskAdapter() {
        recyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(User_taskscreen.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mlayoutManeger);
        completetaskadapter = new GetCompleteTaskAdapter(User_taskscreen.this, completetask);
        recyclerView.setAdapter(completetaskadapter);
        completetaskadapter.notifyDataSetChanged();
    }

    public Integer getActivePhase() {
        return ActivePhase;
    }

    public void setActivePhase(Integer activePhase) {ActivePhase = activePhase;}
}
