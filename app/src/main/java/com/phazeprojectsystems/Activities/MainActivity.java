package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.phazeprojectsystems.Adapter.CompleteProjectListAdapter;
import com.phazeprojectsystems.Adapter.DeleteProjectListAdapter;
import com.phazeprojectsystems.Adapter.IncompleteProjectListAdapter;
import com.phazeprojectsystems.Adapter.ProjectListAdapter;
import com.phazeprojectsystems.Api.DueDateAlert_Api;
import com.phazeprojectsystems.Api.GetProfilepic_Api;
import com.phazeprojectsystems.Api.Get_User_Project;
import com.phazeprojectsystems.Api.Token_submit_Api;
import com.phazeprojectsystems.Constant.CacheManager;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Fragment.Fragment_notification;
import com.phazeprojectsystems.Model.ChangeEmailData;
import com.phazeprojectsystems.Model.CompleteProjectModle;
import com.phazeprojectsystems.Model.CompleteProjectResult;
import com.phazeprojectsystems.Model.DeleteProjectModel;
import com.phazeprojectsystems.Model.DeleteProject_Result_Model;
import com.phazeprojectsystems.Model.Dtabase_Notification_modle;
import com.phazeprojectsystems.Model.DuedateAlert_modle;
import com.phazeprojectsystems.Model.GetProfilePicData;
import com.phazeprojectsystems.Model.InCompleteProjectResult;
import com.phazeprojectsystems.Model.IncompleteProject_modle;
import com.phazeprojectsystems.Model.Project_Data;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.phazeprojectsystems.SharePool.NotificationData;
import com.squareup.okhttp.OkHttpClient;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity {

    private TextView username,ActiveList,task_name,companyname;
    private CircleImageView userImage;
    String userid, userName, company,role;
    private RecyclerView listView;
    private int year_X, month_X, day_X;
    private static final int DILOG_ID = 0;
    private ProjectListAdapter projectListAdapter;
    private DeleteProjectListAdapter deleteprojectListAdapter;
    private CompleteProjectListAdapter completeProjectListAdapter;
    private IncompleteProjectListAdapter incompleteProjectListAdapter;
    private List<com.phazeprojectsystems.Model.ProjectList>projectList = new ArrayList();
    private List<CompleteProjectResult> projectcomplete = new ArrayList();
    private List<InCompleteProjectResult> projectincomplete = new ArrayList();
    private List<DeleteProject_Result_Model> projectDelete = new ArrayList();

    private ImageView setting_icon,errorimage,add_team,projectconnectionerrorimage;
    private RelativeLayout action_icon,profile;
    private SwipeRefreshLayout projectnamelistrefresh;
    private ImageView img_notification;
    private  String[] list;
    public static int mNotifCount = 0;
    protected Button notificationCount;
    CacheManager mCacheManager;
    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();
    private List<Dtabase_Notification_modle> newnotificationlist = new ArrayList<>();
    private   ProgressDialog  progressDoalog;



    String token;
    private Timer myTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findView();
        list = MySharePrafranceClass.GetSharePrefrance(MainActivity.this);

        mCacheManager = new CacheManager(MainActivity.this);
        ShareData();
        listGetProject(userid);
        displayFirebaseRegId();


        readFromDatabase();
        OldreadFromDatabase();



        if (notificationlist==null){

            notificationCount.setVisibility(View.GONE);

        }
        else if (notificationlist.size()==0){
            notificationCount.setVisibility(View.GONE);
        }else {
            setnotificationcount(notificationlist.size());
        }


        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
                             @Override
                             public void run() {

                                 listGetProject12(userid);
                                 GetDeletedProject(list[0]);
                                 GetInCompletProject(list[0]);
                                 GetCompleteProject(list[0]);

                               //  readFromDatabase();
                               //  OldreadFromDatabase();

                             }

                         }, 0, 3000);
                //Alertduedate();

        projectnamelistrefresh.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        projectnamelistrefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                projectnamelistrefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        projectnamelistrefresh.setRefreshing(false);
                        listGetProject(userid);
                    }
                },2000);
            }
        });



        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Token_submit_Api api=rest.create(Token_submit_Api.class);

        api.tokensubmit(token, userid, new Callback<ChangeEmailData>() {
            @Override
            public void success(ChangeEmailData changeEmailData, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });




        add_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MainActivity.this,MyTeam_member.class);
                startActivity(intent);
            }
        });


        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        final View newprojectfab = findViewById(R.id.newproject_fab);

        newprojectfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Create_project.class);
                startActivity(intent);
                menuMultipleActions.collapse();
            }
        });


        img_notification.setOnClickListener((View v)->{

            setnotificationcount(0);
            readFromDatabase();
            OldreadFromDatabase();

            Fragment_notification fragment_notification_Dialog  = new Fragment_notification(notificationlist,newnotificationlist);
            fragment_notification_Dialog.show(getSupportFragmentManager(),"tag");
        });

        action_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(MainActivity.this, action_icon);
                popupMenu.getMenuInflater().inflate(R.menu.project_menu, popupMenu.getMenu());
                if (projectList!=null) {
                    popupMenu.getMenu().getItem(0).setTitle("Active Projects           (" + projectList.size() + ")");
                }else {
                    popupMenu.getMenu().getItem(0).setTitle("Active Projects           (" + 0 + ")");
                }

                if (projectcomplete!=null) {
                    popupMenu.getMenu().getItem(1).setTitle("Completed Projects   (" + projectcomplete.size() + ")");
                }else {
                    popupMenu.getMenu().getItem(1).setTitle("Completed Projects   (" + 0 + ")");
                }
                if (projectincomplete!=null){
                    popupMenu.getMenu().getItem(2).setTitle("Incomplete Projects  ("+projectincomplete.size()+")");
                }else{
                    popupMenu.getMenu().getItem(2).setTitle("Incomplete Projects  ("+ 0 +")");
                }
                if (projectDelete!=null){
                    popupMenu.getMenu().getItem(3).setTitle("Deleted Projects        ("+projectDelete.size()+")");
                }else{
                    popupMenu.getMenu().getItem(3).setTitle("Deleted Projects        ("+ 0 +")");
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.All_project:

                                if(projectList!=null){
                                    if (projectList.isEmpty()) {
                                        ActiveList.setText("All Projects");
                                        errorimage.setVisibility(View.VISIBLE);

                                    }else {
                                        errorimage.setVisibility(View.GONE);
                                        ActiveList.setText("All Projects");
                                        setAllProjectAdapter();
                                    }

                                }
                                else {
                                    errorimage.setVisibility(View.VISIBLE);

                                }
                                break;


                            case R.id.Complete_project:

                                if(projectcomplete!=null){
                                    if (projectcomplete.isEmpty()) {
                                        ActiveList.setText("Complete Projects");
                                        errorimage.setVisibility(View.VISIBLE);

                                    }else {
                                        errorimage.setVisibility(View.GONE);
                                        ActiveList.setText("Complete Projects");
                                        setGetCompleteProject();
                                    }

                            }
                            else {
                                    errorimage.setVisibility(View.VISIBLE);

                                }
                                break;
                            case R.id.In_completeProject:

                                if (projectincomplete!=null) {
                                    if (projectincomplete.isEmpty()){
                                        ActiveList.setText("In-Complete Projects");
                                        errorimage.setVisibility(View.VISIBLE);
                                    }else {
                                    errorimage.setVisibility(View.GONE);
                                        ActiveList.setText("In-Complete Projects");
                                   setGetInCompletProjectAdapter();
                                    }
                                }else {
                                    errorimage.setVisibility(View.VISIBLE);
                                }
                                break;

                            case R.id.DeletedProject:

                                if (projectDelete!=null) {
                                    if (projectDelete.isEmpty()){
                                        ActiveList.setText("Deleted Projects");
                                        errorimage.setVisibility(View.VISIBLE);
                                    }else {
                                        errorimage.setVisibility(View.GONE);
                                        ActiveList.setText("Deleted Projects");
                                        setDeleteProjectAdapter();
                                    }
                                }
                                else {
                                    errorimage.setVisibility(View.VISIBLE);
                                }
                                break;

                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Container_account_activity.class);
                startActivity(intent);
            }
        });

    }

    private void displayFirebaseRegId() {
         FirebaseMessaging.getInstance().subscribeToTopic("news");
        token = FirebaseInstanceId.getInstance().getToken();

        Log.e("myid", "Firebase reg id: " + token);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DILOG_ID) {
            return new DatePickerDialog(MainActivity.this, dpickerlistner, year_X, month_X, day_X);
        } else {
            return null;
        }
    }





    private DatePickerDialog.OnDateSetListener dpickerlistner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_X = year;
            month_X = month;
            day_X = dayOfMonth;
            Constant.toast(MainActivity.this, "Year is :" + year_X + "\n Month is :" + month_X + "\n Day is : " + day_X);
        }
    };

    private void findView() {
        ActiveList = (TextView) findViewById(R.id.ActiveList);
        task_name = (TextView) findViewById(R.id.task_name);
        listView = (RecyclerView) findViewById(R.id.project_name_list);
        add_team= (ImageView) findViewById(R.id.add_team);
        username = (TextView) findViewById(R.id.username);
        userImage = (CircleImageView) findViewById(R.id.user_image);
        action_icon = (RelativeLayout) findViewById(R.id.option_icon);
        img_notification=(ImageView)findViewById(R.id.img_notification);
        notificationCount=(Button)findViewById(R.id.notif_count);

             profile= (RelativeLayout) findViewById(R.id.profile);
             companyname = (TextView) findViewById(R.id.company_name);
             projectnamelistrefresh = (SwipeRefreshLayout) findViewById(R.id.project_name_list_refresh);
             errorimage=(ImageView)findViewById(R.id.errorimage);
             projectconnectionerrorimage=(ImageView)findViewById(R.id.projectconnectionerrorimage);

      }



     private void  ShowDialog(){

         progressDoalog = new ProgressDialog(MainActivity.this);
         progressDoalog.setTitle("Please wait..");
         progressDoalog.show();
      }


      private void dismissDaalog(){

         if (progressDoalog!=null) {
             if (progressDoalog.isShowing()) {
                 progressDoalog.dismiss();
             }
         }
      }
    private void listGetProject12(String userid)
    {

        if (Network_check.isNetWorking(MainActivity.this)) {

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);


            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();

            Get_User_Project Api = rest.create(Get_User_Project.class);
            Api.GetProject(userid, new Callback<Project_Data>() {
                @Override
                public void success(Project_Data project_data, Response response) {
                    String status = project_data.getSuccess();
                    if (status.equals("1")) {

                        projectconnectionerrorimage.setVisibility(View.GONE);

                        projectList = project_data.getProjectList();

                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            projectconnectionerrorimage.setVisibility(View.VISIBLE);
        }
    }

    private void listGetProject(String userid)
    {

        if (Network_check.isNetWorking(MainActivity.this)) {

            ShowDialog();

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);


            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();

            Get_User_Project Api = rest.create(Get_User_Project.class);
            Api.GetProject(userid, new Callback<Project_Data>() {
                @Override
                public void success(Project_Data project_data, Response response) {
                    String status = project_data.getSuccess();
                    if (status.equals("1")) {
                        projectconnectionerrorimage.setVisibility(View.GONE);

                        projectList = project_data.getProjectList();


                        ActiveList.setText("All Projects");

                        errorimage.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MainActivity.this);
                        listView.setLayoutManager(mlayoutManeger);
                        projectListAdapter = new ProjectListAdapter(MainActivity.this, projectList);
                        listView.setAdapter(projectListAdapter);
                        projectListAdapter.notifyDataSetChanged();


                    } else {


                        errorimage.setVisibility(View.VISIBLE);


                    }
                    dismissDaalog();
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissDaalog();

                    String msg = error.getMessage();
                    if (msg != null) {

                        if (msg.contains("phaze.in.gl")) {
                            projectconnectionerrorimage.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

        }else {
            projectconnectionerrorimage.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "Please Check Connection.. !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissDaalog();
    }

    private void setAllProjectAdapter(){
        errorimage.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MainActivity.this);
        listView.setLayoutManager(mlayoutManeger);
        projectListAdapter = new ProjectListAdapter(MainActivity.this, projectList);
        listView.setAdapter(projectListAdapter);
        projectListAdapter.notifyDataSetChanged();
    }


    private void setGetInCompletProjectAdapter(){

        listView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MainActivity.this);
        listView.setLayoutManager(mlayoutManeger);

        incompleteProjectListAdapter = new IncompleteProjectListAdapter(MainActivity.this, projectincomplete);
        listView.setAdapter(incompleteProjectListAdapter);
        incompleteProjectListAdapter.notifyDataSetChanged();

    }

    private void GetDeletedProject(String userid){

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Get_User_Project Api = rest.create(Get_User_Project.class);
        Api.GetDeletedProject(userid, new Callback<DeleteProjectModel>() {
            @Override
            public void success(DeleteProjectModel deleteProjectModel, Response response) {
                projectconnectionerrorimage.setVisibility(View.GONE);
                projectDelete = deleteProjectModel.getResult();


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }


    private void setDeleteProjectAdapter(){

        listView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MainActivity.this);
        listView.setLayoutManager(mlayoutManeger);

        deleteprojectListAdapter = new DeleteProjectListAdapter(MainActivity.this, projectDelete);
        listView.setAdapter(deleteprojectListAdapter);
        deleteprojectListAdapter.notifyDataSetChanged();

    }

    private void GetInCompletProject(String userid) {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Get_User_Project Api = rest.create(Get_User_Project.class);
        Api.GetInCompleteProject(userid, new Callback<IncompleteProject_modle>() {
            @Override
            public void success(IncompleteProject_modle incompleteProject_modle, Response response) {
                String status = incompleteProject_modle.getSuccess();
                if (status.equals("1")) {
                    projectconnectionerrorimage.setVisibility(View.GONE);
                    projectincomplete = incompleteProject_modle.getProjectList();

                }
                else {
                }
            }
            @Override
            public void failure(RetrofitError error) {
            }
        });

    }

    private void setGetCompleteProject(){

        listView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MainActivity.this);
        listView.setLayoutManager(mlayoutManeger);
        completeProjectListAdapter = new CompleteProjectListAdapter(MainActivity.this, projectcomplete);
        listView.setAdapter(completeProjectListAdapter);
        completeProjectListAdapter.notifyDataSetChanged();
    }


    private void GetCompleteProject(String userid) {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Get_User_Project Api = rest.create(Get_User_Project.class);
        Api.GetCompleteProject(userid, new Callback<CompleteProjectModle>() {
            @Override
            public void success(CompleteProjectModle completeProjectModle, Response response) {
                String status = completeProjectModle.getSuccess();
                if (status.equals("1")) {
                    projectconnectionerrorimage.setVisibility(View.GONE);
                    projectcomplete = completeProjectModle.getProjectList();
                }
                else {

                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void GetImage(final String Userid, final String UserName, final String Companyname, final String role) {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        GetProfilepic_Api api = restAdapter.create(GetProfilepic_Api.class);
        api.GetProfilePic(Userid, new Callback<GetProfilePicData>() {
            @Override
            public void success(GetProfilePicData getProfilePicData, Response response) {
                if (getProfilePicData!=null) {

                    if (getProfilePicData.getSuccess() == 1) {
                        MySharePrafranceClass.ClearAll(MainActivity.this);
                        String userImage = getProfilePicData.getThumb();
                        MySharePrafranceClass.Share(MainActivity.this, Userid, UserName, Companyname, userImage, role);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void ShareData() {
        String[] list = MySharePrafranceClass.GetSharePrefrance(MainActivity.this);
        userid = list[0];
        userName = list[1];
        company = list[2];
        role=list[4];

        GetImage(userid, userName, company,role);


        companyname.setText(list[2]);
        username.setText(list[1]);

        String s = list[3];

        if (!(s == null)) {
            if (!(s.equals(""))){
                String image = Constant.picuri.concat(s);
                Glide.with(this).load(image).into(userImage);
            }else{

            }

        } else {

        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        ShareData();
    }

    private  void Alertduedate(){

        RestAdapter restful= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        DueDateAlert_Api ap = restful.create(DueDateAlert_Api.class);
        ap.DueDateAlert(new Callback<DuedateAlert_modle>() {

            @Override
            public void success(DuedateAlert_modle duedateAlert_modle, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void senotification(int a){
        this.mNotifCount= a ;
    }

    public void setnotificationcount(int mNotifCount){
        this.mNotifCount=notificationlist.size();
        if (mNotifCount==0){
            notificationCount.setVisibility(View.GONE);
        }else {
            this.notificationCount.setText(String.valueOf(mNotifCount));
        }
    }


    private void OldreadFromDatabase(){

        NotificationData notificationData = new NotificationData(MainActivity.this);

        try {
            SQLiteDatabase  database   = notificationData.getWritableDatabase();
            Cursor cursor = notificationData.GetNotification(database);



            if (cursor.getCount()>0){

                newnotificationlist.clear();

                while (cursor.moveToNext())
                {
                    int id = cursor.getInt(cursor.getColumnIndex(NotificationData.COLUMN_id));
                    String msg=cursor.getString(cursor.getColumnIndex(NotificationData.COLUMN_MSG));
                    String time=cursor.getString(cursor.getColumnIndex(NotificationData.TIMEOFNOTIFICATION));

                    String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.gettaskname));
                    String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.getduwdate));
                    String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.getdescription));


                    String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.gettaskid));
                    String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.getaddtag));

                    newnotificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

                }

            }

        } catch (SQLException s) {

            new Exception("Error with DB Open");
        }
    }

    private void readFromDatabase(){

        NotificationData notificationData = new NotificationData(MainActivity.this);

        SQLiteDatabase database = notificationData.getWritableDatabase();

        Cursor cursor = notificationData.NewGetNotification(database);
        if (cursor.getCount()>0){
            notificationlist.clear();
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex(NotificationData.NEWCOLUMN_id));
                String msg=cursor.getString(cursor.getColumnIndex(NotificationData.NEWCOLUMN_MSG));
                String time=cursor.getString(cursor.getColumnIndex(NotificationData.NEWTIMEOFNOTIFICATION));

                String gettaskname = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskname));
                String getduwdate =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetduwdate));
                String getdescription =cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetdescription));


                String gettaskid = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgettaskid));
                String getaddtag = cursor.getString(cursor.getColumnIndex(NotificationData.NEWgetaddtag));

                notificationlist.add(new Dtabase_Notification_modle(id,msg,time,gettaskname,getduwdate,getdescription,gettaskid,getaddtag));

            }

        }

    }
}
