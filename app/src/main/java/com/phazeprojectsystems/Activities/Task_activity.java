package com.phazeprojectsystems.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.phazeprojectsystems.Adapter.ProjectAllTask_Adapter;
import com.phazeprojectsystems.Adapter.Project_all_Complete_Task_Adapter;
import com.phazeprojectsystems.Adapter.Project_all_Incomplete_Task_Adapter;
import com.phazeprojectsystems.Adapter.Project_due_Date_Adapter;
import com.phazeprojectsystems.Api.ProjectAllTask_Api;
import com.phazeprojectsystems.Api.ProjectDetail_Api;
import com.phazeprojectsystems.Api.Project_Complete_task_Api;
import com.phazeprojectsystems.Api.Project_DueDate_task_Api;
import com.phazeprojectsystems.Api.Project_Incomplete_Task_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.GetInCompleteProjectTask__result;
import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;
import com.phazeprojectsystems.Model.Get_Project_CompleteTask_modle;
import com.phazeprojectsystems.Model.Get_Project_complete_task_result;
import com.phazeprojectsystems.Model.ProjectAllTaskData;
import com.phazeprojectsystems.Model.ProjectAllTaskData__msg;
import com.phazeprojectsystems.Model.ProjectDetailData;
import com.phazeprojectsystems.Model.Project_Due_Date_Task_Result;
import com.phazeprojectsystems.Model.Project_Due_date_Task_modle;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class Task_activity extends AppCompatActivity {

    private ProjectAllTask_Adapter projectAllTask_Adapter;
    private LinearLayout Add_Task_IN_Active_phase, ActivePhasetext;
    private Project_due_Date_Adapter ProjectdueDateAdapter;
    private Project_all_Complete_Task_Adapter Project_allCompleteTaskAdapter;
    private Project_all_Incomplete_Task_Adapter inCompleteTaskAdapter;
    private SwipeRefreshLayout tasklistrefreshlayout;
    private RecyclerView tasklistView;
    private TextView username, Project_name,txt_dummy,txt_projectcreater,txt_projectdatecreater;
    private ImageView EditProjectButton,project_delete;
    private Integer ActivePhase;
    private FloatingActionButton NewTask_feb;
    private Timer myTimer;
    private GoogleApiClient client;
    private RelativeLayout backarrow;
    static  String projectid,userid;
    private String[] itemlist,list;
    private String projectname;
    private ImageView taskerrorimage,taskconnectionerrorimage;

    private List<ProjectAllTaskData__msg> projecttask = new ArrayList<>();
    private List<GetInCompleteProjectTask__result> incompletetask = new ArrayList<>();
    private List<Project_Due_Date_Task_Result> dueDateTask= new ArrayList<>();
    private List<Get_Project_complete_task_result> completeTask= new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_activity);
        FindView();

        itemlist=MySharePrafranceClass.ProjectGetSharePrefrance(Task_activity.this);

            projectid = itemlist[0];
            projectname=itemlist[1];
            Project_name.setText(projectname);
            userid=list[0];

            projectDetail(projectid);
            projectInCompleteTask1(projectid);


        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                projectInCompleteTask(projectid);
                projectCompleteTask(projectid);
                projectDueDateTask(projectid);
            }

        }, 0, 3000);

     if (projecttask.size()==0)
        {
            txt_dummy.setVisibility(View.VISIBLE);
            tasklistView
                    .setVisibility(View.GONE);

        }else {
            txt_dummy.setVisibility(View.GONE);
            tasklistView
                    .setVisibility(View.VISIBLE);
        }


        NewTask_feb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Task_activity.this,Create_task.class);
                startActivity(intent);
            }
        });
        EditProjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Task_activity.this,Edit_project.class));
            }
        });


        ActivePhasetext.setOnClickListener(v->{

            projectInCompleteTask1(projectid);

        });


        Add_Task_IN_Active_phase.setOnClickListener((View v)->{

            {
                PopupMenu popupMenu = new PopupMenu(Task_activity.this,Add_Task_IN_Active_phase);
                popupMenu.getMenuInflater().inflate(R.menu.menu_main, popupMenu.getMenu());

               if (completeTask!=null) {
                   popupMenu.getMenu().getItem(0).setTitle("Completed Task   (" + completeTask.size() + ")");
               }else {
                   popupMenu.getMenu().getItem(0).setTitle("Completed Task   (" + 0 + ")");
               }
                if (incompletetask!=null){
                popupMenu.getMenu().getItem(1).setTitle("Incomplete Task  ("+incompletetask.size()+")");
                }else {
                    popupMenu.getMenu().getItem(1).setTitle("Incomplete Task  ("+ 0 +")");
                }
                if (dueDateTask!=null) {
                    popupMenu.getMenu().getItem(2).setTitle("Task By Due Date (" + dueDateTask.size() + ")");
                }else {
                    popupMenu.getMenu().getItem(2).setTitle("Task By Due Date (" + 0 + ")");
                }
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String[] list = MySharePrafranceClass.GetSharePrefrance(Task_activity.this);
                        switch (item.getItemId()) {

                            case R.id.Complete_task:
                                setActivePhase(R.id.Complete_task);
                                if (completeTask!=null) {

                                    if (completeTask.isEmpty()) {
                                        taskerrorimage.setVisibility(View.VISIBLE);

                                    }
                                    else {
                                        taskerrorimage.setVisibility(View.GONE);
                                        setprojectCompleteTaskAdapter();
                                    }
                                }
                                break;

                            case R.id.In_completeTask:
                                setActivePhase(R.id.In_completeTask);
                                if (incompletetask!=null) {


                                    if (incompletetask.isEmpty()) {
                                        taskerrorimage.setVisibility(View.VISIBLE);

                                    }else
                                        {
                                        taskerrorimage.setVisibility(View.GONE);
                                        setProjectIncompleteTaskAdapter();
                                    }
                                }
                                break;

                            case R.id.Task_by_due_date:
                                setActivePhase(R.id.Task_by_due_date);
                                if (dueDateTask!=null) {
                                    if (dueDateTask.isEmpty()) {
                                        taskerrorimage.setVisibility(View.VISIBLE);

                                    }
                                    else {
                                        taskerrorimage.setVisibility(View.GONE);
                                        setDueDateTaskadapter();
                                    }
                                }
                                else {
                                    taskerrorimage.setVisibility(View.VISIBLE);
                                }
                                break;

                            default:
                                Toast.makeText(Task_activity.this, "You Click on  " + item.getTitle(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }

        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final View newtastfab = findViewById(R.id.newtask_fab1);
        newtastfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Task_activity.this, Create_task.class);
                startActivity(intent);  }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        tasklistrefreshlayout.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        tasklistrefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                tasklistrefreshlayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tasklistrefreshlayout.setRefreshing(false);
                        if (getActivePhase()==null){
                            setActivePhase( R.id.In_completeTask);
                        }

                        switch (getActivePhase()) {

                            case R.id.Complete_task:
                                if (completeTask!=null) {
                                    setprojectCompleteTaskAdapter();
                                }
                                break;

                            case R.id.In_completeTask:
                                if (incompletetask!=null) {setProjectIncompleteTaskAdapter();}
                                break;

                            case R.id.Task_by_due_date:
                                if (dueDateTask!=null) {setDueDateTaskadapter();}
                                break;

                            default:
                                break;
                        }
                    }
                },3000);
            }
        });

    }

    private void  projectAllTask(String userid, String projectid) {
        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        ProjectAllTask_Api api =rest.create(ProjectAllTask_Api.class);

        api.projectAlltask(userid, projectid, new Callback<ProjectAllTaskData>() {
            @Override
            public void success(ProjectAllTaskData projectAllTaskData, Response response) {
              if(projectAllTaskData.getSuccess().equals("1")){


                  projecttask=  projectAllTaskData.getMsg();
                  tasklistView.setVisibility(View.VISIBLE);
                  RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Task_activity.this,LinearLayoutManager.VERTICAL,false);
                  tasklistView.setLayoutManager(mlayoutManeger);
                  projectAllTask_Adapter =new ProjectAllTask_Adapter(Task_activity.this,projecttask);
                  tasklistView.setAdapter(projectAllTask_Adapter);
                  projectAllTask_Adapter.notifyDataSetChanged();
              }else{
                  Constant.toast(Task_activity.this,"have no task");
              }
            }

            @Override
            public void failure(RetrofitError error) {
                Constant.toast(Task_activity.this,"Check Connection Please"+error.getMessage());
            }
        });
    }

    private void FindView() {
        ActivePhasetext = (LinearLayout)findViewById(R.id.ActivePhasetext);
        list = MySharePrafranceClass.GetSharePrefrance(Task_activity.this);
        EditProjectButton=(ImageView)findViewById(R.id.img_Edit_project_detail);
        taskerrorimage=(ImageView)findViewById(R.id.taskerrorimage);
        taskconnectionerrorimage=(ImageView)findViewById(R.id.taskconnectionerrorimage);

        tasklistView = (RecyclerView) findViewById(R.id.recyclerview_tasklist);
        NewTask_feb = (FloatingActionButton) findViewById(R.id.newtask_fab1);

        String[] list = MySharePrafranceClass.GetSharePrefrance(Task_activity.this);
        Project_name = (TextView) findViewById(R.id.Project_name);
        txt_dummy= (TextView) findViewById(R.id.txt_dummy);
//       action_icon = (RelativeLayout) findViewById(R.id.option_icon);
        backarrow= (RelativeLayout) findViewById(R.id.backarrow_Button_taskactivity);
        txt_projectcreater = (TextView) findViewById(R.id.txt_project_creater_name);
        txt_projectdatecreater = (TextView) findViewById(R.id.txt_project_date_creater);
        Add_Task_IN_Active_phase =(LinearLayout)findViewById(R.id.Add_Task_IN_Active_phase);
        tasklistrefreshlayout=(SwipeRefreshLayout)findViewById(R.id.tasklist_refresh_layout);
        txt_projectcreater.setText(list[1]);
    }


    public Action getIndexApiAction() {
        Thing object = new Thing.Builder().setName("Task_activity Page")
                // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        client.connect();

        AppIndex.AppIndexApi.start(client, getIndexApiAction());

    }

    @Override
    public void onStop() {
        super.onStop();

        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MySharePrafranceClass.ClearAllProjectShare(Task_activity.this);

        startActivity(new Intent(Task_activity.this,MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }



    private void projectDetail(final String projectid) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        ProjectDetail_Api api = rest.create(ProjectDetail_Api.class);
        api.ProjectDetil(projectid, new Callback<ProjectDetailData>() {
            @Override
            public void success(ProjectDetailData projectDetailData, Response response) {
                if (projectDetailData.getSuccess() == 1)
                {
                    String discripton = projectDetailData.getResult().get(0).getDescription();
                    String address = projectDetailData.getResult().get(0).getAddress();
                    String project = projectDetailData.getResult().get(0).getProjactName();
                    String date = projectDetailData.getResult().get(0).getDue_date();

                    txt_projectdatecreater.setText(date);

                } else {
                    Toast.makeText(Task_activity.this, "Some Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
//=================================Helper=====================================================


    public Integer getActivePhase() {
        return ActivePhase;
    }

    public void setActivePhase(Integer activePhase) {
        ActivePhase = activePhase;
    }

    //====================================All data Adapter Seter============================

    private void setDueDateTaskadapter(){

        tasklistView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Task_activity.this, LinearLayoutManager.VERTICAL, false);
        tasklistView.setLayoutManager(mlayoutManeger);
        ProjectdueDateAdapter = new Project_due_Date_Adapter(Task_activity.this, dueDateTask);
        tasklistView.setAdapter(ProjectdueDateAdapter);
        ProjectdueDateAdapter.notifyDataSetChanged();

    }

    private  void setProjectIncompleteTaskAdapter(){

        tasklistView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Task_activity.this,LinearLayoutManager.VERTICAL,false);
        tasklistView.setLayoutManager(mlayoutManeger);
        inCompleteTaskAdapter= new Project_all_Incomplete_Task_Adapter(Task_activity.this,incompletetask);
        tasklistView.setAdapter(inCompleteTaskAdapter);
        inCompleteTaskAdapter.notifyDataSetChanged();
    }

    private void setprojectCompleteTaskAdapter(){

        tasklistView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Task_activity.this,LinearLayoutManager.VERTICAL,false);
        tasklistView.setLayoutManager(mlayoutManeger);
        Project_allCompleteTaskAdapter=new Project_all_Complete_Task_Adapter(Task_activity.this,completeTask);
        tasklistView.setAdapter(Project_allCompleteTaskAdapter);
        Project_allCompleteTaskAdapter.notifyDataSetChanged();

    }
    //================================== All Api funtion =================================


    private void  projectCompleteTask(String projectid) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Project_Complete_task_Api api =rest.create(Project_Complete_task_Api.class);
        api.getProjectcompleteTask(projectid, new Callback<Get_Project_CompleteTask_modle>() {
            @Override
            public void success(Get_Project_CompleteTask_modle get_project_completeTask_modle, Response response) {
                taskconnectionerrorimage.setVisibility(View.GONE);
                completeTask = get_project_completeTask_modle.getResult();
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void  projectInCompleteTask(String projectid) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Project_Incomplete_Task_Api api =rest.create(Project_Incomplete_Task_Api.class);

        api.getProjectAllIncompleteTask(projectid, new Callback<Get_In_complete_Project_task_modle>() {
            @Override
            public void success(Get_In_complete_Project_task_modle get_in_complete_project_task_modle, Response response) {
                incompletetask=get_in_complete_project_task_modle.getResult();
                taskconnectionerrorimage.setVisibility(View.GONE);
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }



    private void  projectInCompleteTask1(String projectid) {
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Task_activity.this);
        progressDoalog.setTitle("Please wait..");
        progressDoalog.show();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Project_Incomplete_Task_Api api =rest.create(Project_Incomplete_Task_Api.class);

        api.getProjectAllIncompleteTask(projectid,new Callback<Get_In_complete_Project_task_modle>() {
            @Override
            public void success(Get_In_complete_Project_task_modle get_in_complete_project_task_modle, Response response) {
                if (get_in_complete_project_task_modle.getSuccess().equals("1")) {
                    progressDoalog.dismiss();
                    taskerrorimage.setVisibility(View.GONE);
                    taskconnectionerrorimage.setVisibility(View.GONE);
                    incompletetask = get_in_complete_project_task_modle.getResult();
                    setProjectIncompleteTaskAdapter();
                }else {
                    taskconnectionerrorimage.setVisibility(View.GONE);
                    taskerrorimage.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void failure(RetrofitError error) {
                progressDoalog.dismiss();

                String  msg = error.getMessage();
                if (msg!= null) {

                    if (error.toString().contains("phaze.in.gl")) {
                        taskconnectionerrorimage.setVisibility(View.VISIBLE);
                    }
                }


            }
        });
    }

    private void  projectDueDateTask(String projectid) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        Project_DueDate_task_Api api =rest.create(Project_DueDate_task_Api.class);
        api.getProjectDueDateTask(projectid, new Callback<Project_Due_date_Task_modle>() {
            @Override
            public void success(Project_Due_date_Task_modle project_due_date_task_modle, Response response) {

                if (project_due_date_task_modle.getSuccess().equals("1")){
                    dueDateTask = project_due_date_task_modle.getResult();

                    taskconnectionerrorimage.setVisibility(View.GONE);
                    if (dueDateTask!=null) {

                    }
                    else {

                    }
                }else {
                    taskconnectionerrorimage.setVisibility(View.GONE);
                    Toast.makeText(Task_activity.this, "Have Some Error", Toast.LENGTH_SHORT).show();
                }}
            @Override
            public void failure(RetrofitError error) {



            }
        });
    }

}
