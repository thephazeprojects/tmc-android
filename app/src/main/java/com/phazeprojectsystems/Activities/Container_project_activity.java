package com.phazeprojectsystems.Activities;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.FrameLayout;

import com.phazeprojectsystems.Fragment.Task_Give_With_Project_fragment;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

public class Container_project_activity  extends FragmentActivity {
    FrameLayout frameLayout;
    static Container_project_activity act1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_project_activity);


        act1=this;
        frameLayout= (FrameLayout) findViewById(R.id.task_fragment);
        projectFragment(new Task_Give_With_Project_fragment(),"task");
    }
    public static void projectFragment(Fragment fragment, String Tag){
          android.support.v4.app.FragmentManager manager = act1.getSupportFragmentManager();
          android.support.v4.app.FragmentTransaction transaction = manager.beginTransaction()
                  .setCustomAnimations(R.anim.slide_up_animation,R.anim.slide_down_animation);
          transaction.replace(R.id.task_fragment,fragment, Tag);
          transaction.addToBackStack(Tag);
          transaction.commit();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MySharePrafranceClass.ClearAllProjectShare(Container_project_activity.this);
        finish();
    }
}
