package com.phazeprojectsystems.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Adapter.AllSeletedMemveListAdapter;
import com.phazeprojectsystems.Adapter.MemberListAdapter;
import com.phazeprojectsystems.Api.DeleteProject_Api;
import com.phazeprojectsystems.Api.Forget_Password_Api;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Fragment.Task_Give_With_Project_fragment;
import com.phazeprojectsystems.Model.AssigneeByTask_Result_model;
import com.phazeprojectsystems.Model.AssigneeByTask_model;
import com.phazeprojectsystems.Model.Commentdelete_model;
import com.phazeprojectsystems.Model.CompanyData;
import com.phazeprojectsystems.Model.CompanyDataResult;
import com.phazeprojectsystems.Model.ForgetPasswordData;
import com.phazeprojectsystems.Model.InviteFriendData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class Select_team_menber extends AppCompatActivity {
    private ImageView delete_user,submit_edit_task;
    RelativeLayout arrow;
    private TextView name,nouserfound;
    public  static  TextView followers_value;
    private android.support.v7.widget.SearchView search;
    private MemberListAdapter menberAdapter;
    private RecyclerView Memb_list;
    private CircleImageView userselecterimage;
    private String getMemberuserid="",ggetmemberid="";
    public static Map<String,String> SelectedMember =  new HashMap<>();
    public static Map<String,String> SelectedMemberedittask =  new HashMap<>();
    private List<CompanyDataResult> CompanResult= new ArrayList<>();
    private Button invitemembers;
    private EditText txt_dialogemail,txt_emailaddress;
    private Button btn_send,btn_cancel;
    private AlertDialog alertDialog;
    private RelativeLayout submit;
    private String getuserid,companyName, gettaskid;
    private ProgressDialog dialog;
    private List<AssigneeByTask_Result_model> TaskAssigeeUser =  new ArrayList<>();;
    private AllSeletedMemveListAdapter allSeletedMemveListAdapter;
    public static  Select_team_menber select_team_menber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_team_menber);
        Edit_task.TaskMember.clear();
        Create_task.TaskMember.clear();
        Task_Give_With_Project_fragment.TaskMember.clear();
        SelectedMember.clear();
        FINDVIEW();
        shareAble();
        Clickable();

        Intent extra = getIntent();
        if (extra!=null) {
            gettaskid = extra.getStringExtra("get_taskid");
            if (gettaskid!=null) {
                AssigneeUserBuyTask(gettaskid,companyName);
            }else {
                UserbyCompany(companyName);
            }
        }
        
      select_team_menber=this;



    }

    private void shareAble() {

        String[] list= MySharePrafranceClass.GetSharePrefrance(Select_team_menber.this);

        companyName=list[2];
        String UserName=list[1];
        getuserid=list[0];
        name.setText(UserName);
        String s = list[3];



        if (!(s==null)) {

        if (!s.equalsIgnoreCase("")) {
            String image= Constant.picuri.concat(s);
            Glide.with(this).load(image).into(userselecterimage);
        }
        }
        else {
            Glide.with(this).load(R.drawable.user_icn).into(userselecterimage);
        }
        int followerscount = 0;
        followers_value.setText(String.valueOf(followerscount));
    }


    private void AssigneeUserBuyTask(String Taskid , String companyName) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        User_by_Company_Api api = rest.create(User_by_Company_Api.class);
        api.AssigneeUserBuyTask(Taskid, companyName, new Callback<AssigneeByTask_model>() {
            @Override
            public void success(AssigneeByTask_model assigneeByTask_model, Response response) {

               TaskAssigeeUser= assigneeByTask_model.getResult();


                RecyclerView.LayoutManager mlayoutManeger =
                        new LinearLayoutManager(Select_team_menber.this,LinearLayoutManager.VERTICAL,false);

                Memb_list.setLayoutManager(mlayoutManeger);
                menberAdapter= new MemberListAdapter(Select_team_menber.this,TaskAssigeeUser);
                Memb_list.setAdapter(menberAdapter);
                menberAdapter.notifyDataSetChanged();
                if (TaskAssigeeUser.size()==0)
                {
                    nouserfound.setVisibility(View.VISIBLE);
                }
                else {

                    nouserfound.setVisibility(View.GONE);
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    private void UserbyCompany(String companyName) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();
        User_by_Company_Api api = rest.create(User_by_Company_Api.class);
        api.UserbyCompany(companyName, new Callback<CompanyData>() {
            @Override
            public void success(CompanyData companyData, Response response) {
                if(companyData.getSuccess()==1)


                CompanResult=companyData.getResult();

                RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(Select_team_menber.this,LinearLayoutManager.VERTICAL,false);
                Memb_list.setLayoutManager(mlayoutManeger);
                allSeletedMemveListAdapter= new AllSeletedMemveListAdapter(Select_team_menber.this,CompanResult);
                Memb_list.setAdapter(allSeletedMemveListAdapter);
                allSeletedMemveListAdapter.notifyDataSetChanged();


                if (CompanResult.size()==0)
                {
                    nouserfound.setVisibility(View.VISIBLE);
                }
                else {

                    nouserfound.setVisibility(View.GONE);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Constant.toast(Select_team_menber.this,""+error.getMessage());

            }
        });

    }


    private void Clickable() {




        delete_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!(SelectedMember.isEmpty())) {

                    for(Map.Entry<String,String> entry : SelectedMember.entrySet()){
                        getMemberuserid =  entry.getValue()+ "," + getMemberuserid;

                        if(getMemberuserid.endsWith(",")) {

                            ggetmemberid= getMemberuserid.substring(0, getMemberuserid.length() - 1);
                        }
                    }

                }



                RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
                DeleteProject_Api api=rest.create(DeleteProject_Api.class);

                api.deleteuser(ggetmemberid, new Callback<Commentdelete_model>() {
                    @Override
                    public void success(Commentdelete_model commentdelete_model, Response response) {

                        if (commentdelete_model.getSuccess()==1)
                        {
                            UserbyCompany(companyName);

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });


            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Create_task.AddTaskMember(SelectedMember);
                Edit_task.AddTaskMember(SelectedMember);
                Task_Give_With_Project_fragment.AddTaskMember(SelectedMember);
//                   Create_task.AddTaskMember(holder.txt_membername.getText().toString(),Userd);
                 finish();

            }
        });


        invitemembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                String email=txt_emailaddress.getText().toString();

                if (email.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txt_emailaddress.setError("Enter Valid Email");
                    Toast.makeText(Select_team_menber.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                }else{
                    invitefirend(getuserid,email);
                    }

            }
        });

    }

    private void invitefirend(String userid, String email) {


        dialog = new ProgressDialog(Select_team_menber.this);
        dialog.setMessage("Loading, please wait.");
        dialog.show();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Forget_Password_Api api=rest.create(Forget_Password_Api.class);

        api.invitefrinds(userid, email, new Callback<InviteFriendData>() {
            @Override
            public void success(InviteFriendData inviteFriendData, Response response) {
                String status=inviteFriendData.getSuccess();
                String message=inviteFriendData.getMessage();
                if (status.equals("1")){
                    txt_emailaddress.setText("");

                    UserbyCompany(companyName);

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    Toast.makeText(Select_team_menber.this, ""+message, Toast.LENGTH_SHORT).show();

                }else {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(Select_team_menber.this, ""+message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(Select_team_menber.this, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void FINDVIEW() {

        name=(TextView)findViewById(R.id.username_projectTeam);
        arrow=(RelativeLayout) findViewById(R.id.back_arrow_select_team);
        Memb_list=(RecyclerView)findViewById(R.id.memberList);
        userselecterimage=(CircleImageView)findViewById(R.id.img_user_selected_image);
        invitemembers= (Button) findViewById(R.id.invite_memebers);
        followers_value= (TextView) findViewById(R.id.followers_value);
        submit= (RelativeLayout) findViewById(R.id.relative_submit);
        nouserfound= (TextView) findViewById(R.id.txt_no_userdound);

        delete_user= (ImageView) findViewById(R.id.delete_user);
        submit_edit_task= (ImageView) findViewById(R.id.submit_edit_task);
        txt_emailaddress= (EditText) findViewById(R.id.text_emailaddres);


    }

    @Override
    public void onBackPressed() {

        SelectedMember.clear();
        super.onBackPressed();
    }
}
