package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Api.Create_project_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Model.Create_ProjectData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class Create_project extends AppCompatActivity {


    private EditText project, address, discreption;
    private RelativeLayout create_projectcancel;
    TextView date;
    String projid;
    private int year,month,day;
    static final int DATE_PICKER_ID = 1111;
    ImageView backarrow;
    private com.getbase.floatingactionbutton.FloatingActionButton taskFloatingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_project);

        address = (EditText) findViewById(R.id.Project_Address);
        discreption = (EditText) findViewById(R.id.Project_discription);
        date = (TextView) findViewById(R.id.Project_Date);
        project = (EditText) findViewById(R.id.project_name);
        backarrow= (ImageView) findViewById(R.id.backarrow_createtask);
        create_projectcancel=(RelativeLayout)findViewById(R.id.create_projectcancel);
        taskFloatingButton = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.taskFloatingButton);
        final Calendar c = Calendar.getInstance();

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);



        c.setTimeInMillis(System.currentTimeMillis() - 1000);

        date.setText(new StringBuilder().append(day)
                .append("/").append(month + 1).append("/").append(year)
                .append(""));


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_PICKER_ID);
            }
        });


     //   project.setInputType(InputType.TYPE_CLASS_TEXT);

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        create_projectcancel.setOnClickListener(v->{

                onBackPressed();
        });

        taskFloatingButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (project.getText().toString().trim().equals("")) {
                    Constant.toast(Create_project.this, "Please Enter Name Of Project");
                } else {
                    String[] list = MySharePrafranceClass.GetSharePrefrance(Create_project.this);
                    String userid = list[0];
                    String projectname = project.getText().toString();
                    String project_Due_date = date.getText().toString();
                    String add = address.getText().toString();
                    String dis = discreption.getText().toString();
                    createproject(userid, projectname, add, dis, project_Due_date);

                }
            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {



            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;


            date.setText(new StringBuilder().append(day)
                    .append("/").append(month + 1).append("/").append(year)
                    .append(""));

        }
    };


    private void createproject(String userid, final String projectname, String discription, String address, String project_Due_date) {



        if (Network_check.isNetWorking(Create_project.this)) {

            final ProgressDialog progressDoalog;
            progressDoalog = new ProgressDialog(Create_project.this);
            progressDoalog.setTitle("Please wait..");
            progressDoalog.show();

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(10, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);

            RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                    .build();

            Create_project_Api api = rest.create(Create_project_Api.class);
            api.Createproject(userid, projectname, discription, address, project_Due_date, new Callback<Create_ProjectData>() {
                @Override
                public void success(Create_ProjectData create_projectData, Response response) {
                    String status = create_projectData.getSuccess();
                    if (status.equals("1")) {
                        projid = create_projectData.getProject_id();
                        MySharePrafranceClass.ProjectShare(Create_project.this, projid, projectname);
                        startActivity(new Intent(Create_project.this, Container_project_activity.class));
                        progressDoalog.dismiss();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    progressDoalog.dismiss();
                    Constant.toast(Create_project.this, "Check Connection" +error.getMessage());
                }
            });


        }else {
            Toast.makeText(Create_project.this, " Check Connection Please", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MySharePrafranceClass.ClearAllProjectShare(Create_project.this);
    }
}
