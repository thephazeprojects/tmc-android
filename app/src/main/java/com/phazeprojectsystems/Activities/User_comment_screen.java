package com.phazeprojectsystems.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.phazeprojectsystems.Adapter.AllComment_adapter;
import com.phazeprojectsystems.Adapter.Comment_adapter;
import com.phazeprojectsystems.Api.Comment_api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.AllcommentModel;
import com.phazeprojectsystems.Model.CommentLis;
import com.phazeprojectsystems.Model.CommentModel;
import com.phazeprojectsystems.Model.ComntLis;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class User_comment_screen extends AppCompatActivity {

    private ImageView sendbutton,img_deletecomment,backarrow;
    private RelativeLayout edittask;
    private TextView txt_taskname, txt_createdby, txt_duedate, txt_description,userTask_CompleteStatus;
    private EditText txt_comment;
    private RecyclerView recycler;
    private String gettaskname, getduedate,comingintent,getUpdatetime,getdescription, getcomment, gettaskid, userid, project_id, get_projectid,getadd_tag;
    private ArrayList<String> listItems;
    private ArrayAdapter<String> adapter;
    private RelativeLayout fileattachemnt;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Uri gelleryUri, fileUri, ur;
    private static String userChoosenTask,getuser_name,get_user_image,picturePath;
    private String[] list;
    private String[] projectItemList;
    private LinearLayout userprogress;
    private TypedFile UploadedFile = null;
    private List<ComntLis> comntLists = new ArrayList<ComntLis>();
    private List<CommentLis> comntList2 = new ArrayList<CommentLis>();
    private AllComment_adapter commentadapter;
    private Comment_adapter commentadapter2;
    private CircleImageView img_user;
    private SwipeRefreshLayout usercomtSwipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_comment_screen);
         findview();

        Intent extra = getIntent();
        ShareObject();
        getuser_name=list[1];
        get_user_image=list[3];

        if (extra!=null)
        {
            gettaskname = extra.getStringExtra("gettaskname");
            getduedate = extra.getStringExtra("getdewdate");
            getdescription = extra.getStringExtra("getdescription");
            gettaskid = extra.getStringExtra("gettaskid");
            getadd_tag = extra.getStringExtra("getaddtag");
            getUpdatetime = extra.getStringExtra("updatetime");
            comingintent=  extra.getStringExtra("comingintent");
            txt_taskname.setText(gettaskname);
            txt_duedate.setText(getduedate);
            txt_description.setText(getdescription);
            txt_createdby.setText(getuser_name);
            userTask_CompleteStatus.setText(getUpdatetime);
        }

        if (!(get_user_image==null)) {

             if (!(get_user_image.equalsIgnoreCase(""))){

                 String user_image= Constant.picuri.concat(get_user_image);
                 Glide.with(User_comment_screen.this).load(user_image).into(img_user);}
             }
             else
        {
        }



        CharSequence Current_date = currentDate().trim();
        char first = Current_date.charAt(0);
        String duedate = getduedate.trim();

        String cur_date = Current_date.toString();

        if (duedate.trim().equals(cur_date))
        {
            txt_duedate.setTextColor(Color.RED);
            txt_duedate.setText("Today");
        }
        else {
            txt_duedate.setTextColor(Color.BLACK);
        }



        allcoments();
        clickview();


        usercomtSwipe.setColorSchemeResources(R.color.bluecolor,R.color.red,R.color.green);
        usercomtSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                usercomtSwipe.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        usercomtSwipe.setRefreshing(false);
                        allcoments();
                    }
                },1000);
            }
        });


    }



    private void findview() {

        userprogress = (LinearLayout)findViewById(R.id.userprogress);

        img_deletecomment= (ImageView) findViewById(R.id.img_deletecomment);
        sendbutton= (ImageView) findViewById(R.id.send_comment);
        txt_taskname = (TextView) findViewById(R.id.txt_user_commenttaskname);
        txt_createdby = (TextView) findViewById(R.id.txt_comment_createdby);
        txt_duedate = (TextView) findViewById(R.id.txt_commentduedate);
        txt_description = (TextView) findViewById(R.id.comment_description);
        txt_comment = (EditText) findViewById(R.id.txt_comment);
        recycler = (RecyclerView) findViewById(R.id.comment_listiview);
        fileattachemnt = (RelativeLayout) findViewById(R.id.usercomment_fileattachment);
        img_user= (CircleImageView) findViewById(R.id.user_icon);
        backarrow= (ImageView) findViewById(R.id.backarrow_usercommentscreen);
        usercomtSwipe = (SwipeRefreshLayout)findViewById(R.id.usercomtSwipe);
        userTask_CompleteStatus=(TextView)findViewById(R.id.userTask_CompleteStatus);

    }

    private void ShareObject() {

        list = MySharePrafranceClass.GetSharePrefrance(User_comment_screen.this);
        projectItemList = MySharePrafranceClass.ProjectGetSharePrefrance(User_comment_screen.this);

    }

    private void allcoments() {

        userid = list[0];

        get_projectid = projectItemList[0];

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Comment_api api = rest.create(Comment_api.class);

        api.all_comments(userid, gettaskid, get_projectid, new Callback<AllcommentModel>() {
            @Override
            public void success(AllcommentModel allcommentModel, Response response) {

                Long status = allcommentModel.getSuccess();

                if (status == 1) {
                    Toast.makeText(User_comment_screen.this, "Status 1", Toast.LENGTH_SHORT).show();
                    comntLists = allcommentModel.getComntList();
                    recycler.setVisibility(View.VISIBLE);
                    commentadapter = new AllComment_adapter(User_comment_screen.this, comntLists);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(User_comment_screen.this);
                    mLayoutManager.setReverseLayout(true);
                    recycler.setLayoutManager(mLayoutManager);
                    recycler.setAdapter(commentadapter);
                    commentadapter.notifyDataSetChanged();
                }
                else
                    {
                       Toast.makeText(User_comment_screen.this, "Status 0", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void failure(RetrofitError error) {

//                Toast.makeText(User_comment_screen.this, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clickview() {

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

        sendbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getcomment = txt_comment.getText().toString().trim();

                if (getcomment.trim().equalsIgnoreCase("")) {
                    Toast.makeText(User_comment_screen.this, "Enter Comment", Toast.LENGTH_SHORT).show();
                } else {
                    final String picture = null;
                    Comment_register(getcomment,picture);
                }


            }
        });


        txt_comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_SEND) {

                    getcomment = txt_comment.getText().toString().trim();

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
                    Date currentLocalTime = cal.getTime();
                    DateFormat date = new SimpleDateFormat("HH:mm a");
                    date.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));

                    String localTime = date.format(currentLocalTime);



                    if (getcomment.trim().equalsIgnoreCase("")) {
                        Toast.makeText(User_comment_screen.this, "Enter Comment", Toast.LENGTH_SHORT).show();
                    } else {
                        final String picture = null;
                        Comment_register(getcomment,picture);
                    }

                }

                return false;
            }
        });

        fileattachemnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();
            }
        });

    }
    private void Comment_register(String get_comment,String UploadedFile1 ) {

        project_id = projectItemList[0];


        if (UploadedFile1 != null) {
            UploadedFile = new TypedFile("image/*", new File(String.valueOf(UploadedFile1)));
            userprogress.setVisibility(View.VISIBLE);
        }
//
//        final ProgressDialog pd = new ProgressDialog(Comment_screen.this);
//        pd.setMessage("Upload Image");
//        pd.show();


        txt_comment.setText("");

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Comment_api api = rest.create(Comment_api.class);

        api.comment(userid, gettaskid, project_id, get_comment,UploadedFile, new Callback<CommentModel>() {
            @Override
            public void success(CommentModel commentModel, Response response) {


                Long status = commentModel.getSuccess();
                userprogress.setVisibility(View.GONE);

                if (status == 1) {

                    Toast.makeText(User_comment_screen.this, "Sucessfull", Toast.LENGTH_SHORT).show();



                    comntList2 = commentModel.getCommentList();
                    recycler.setVisibility(View.VISIBLE);
                    commentadapter2 = new Comment_adapter(User_comment_screen.this, comntList2);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(User_comment_screen.this);
                    mLayoutManager.setReverseLayout(true);
                    recycler.setLayoutManager(mLayoutManager);
                    comntLists.clear();
                    recycler.setAdapter(commentadapter2);
                    commentadapter2.notifyDataSetChanged();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                userprogress.setVisibility(View.GONE);

//                Toast.makeText(User_comment_screen.this, "Failuree", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.phazeprojectsystems.Constant.Utility.checkPermission(User_comment_screen.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = FileProvider.getUriForFile(User_comment_screen.this,
                "com.phazeprojectsystems.FileProvider",
                getOutputMediaFile(MEDIA_TYPE_IMAGE));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {

            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        } else {
            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }



        startActivityForResult(intent, REQUEST_CAMERA);

    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, SELECT_FILE);
    }

    private String currentDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        String todayAsString = dateFormat.format(today);

        return todayAsString;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {

                Comment_register("",picturePath);
            }

        } else if (requestCode == SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                onSelectFromGalleryResult(data);
                Comment_register("",picturePath);

            }
        }}

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage=data.getData();
            String[] filePathColumn={MediaStore.Images.Media.DATA};
            Cursor cursor=getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


        }
    }


    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/.PhazeSystem/");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }
        File mediaFile;
        java.util.Date date = new java.util.Date();
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "IMG_" + new Timestamp(date.getTime()).toString() + ".jpg");
            picturePath=mediaFile.getAbsolutePath();

        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "VID_" + new Timestamp(date.getTime()).toString() + ".mp4");
        } else {
            return null;
        }

        return mediaFile;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

         if (comingintent!=null) {

             if (comingintent.equals("notification")) {

                 startActivity(new Intent(User_comment_screen.this, Userproject_screen.class));
             } else {
                 finish();
             }
         }else {
             startActivity(new Intent(User_comment_screen.this, Userproject_screen.class));
         }
    }
}
