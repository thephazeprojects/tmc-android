package com.phazeprojectsystems.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.phazeprojectsystems.Fragment.Edit_profile;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

public class Container_account_activity extends AppCompatActivity {

    private FrameLayout frameLayout;
    String[] list;
    String role;

    static Container_account_activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container_account_activity);
        act = this;
        list = MySharePrafranceClass.GetSharePrefrance(Container_account_activity.this);
        role = list[4];
        frameLayout = (FrameLayout) findViewById(R.id.container_frame_account_activity);
        showFragment(new Edit_profile(), "");
    }

    public static void showFragment(Fragment fragment, String Tag) {
        FragmentManager manager = act.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container_frame_account_activity, fragment, Tag);
        transaction.addToBackStack(null);
        transaction.commit();
        act.getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

        } else {
            if (role.equals("1")) {
                Intent intent = new Intent(Container_account_activity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(Container_account_activity.this, Userproject_screen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }

        }
    }
}