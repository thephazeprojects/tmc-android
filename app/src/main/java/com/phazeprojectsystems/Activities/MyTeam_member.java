package com.phazeprojectsystems.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Adapter.AllMemberListAdapter;
import com.phazeprojectsystems.Adapter.MemberListAdapter;
import com.phazeprojectsystems.Api.Forget_Password_Api;
import com.phazeprojectsystems.Api.User_by_Company_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.CompanyData;
import com.phazeprojectsystems.Model.CompanyDataResult;
import com.phazeprojectsystems.Model.InviteFriendData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class MyTeam_member extends AppCompatActivity {

    RelativeLayout arrow;
    private TextView name,followers_value,nouserfound;
    private AllMemberListAdapter menberAdapter;
    private RecyclerView Memb_list;
    List<CompanyDataResult> CompanResult=new ArrayList<>();
    private String companyName,getuserid;
    private EditText txt_emailaddress;
    private Button invitemembers;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_team_member);
        findview();
        onClick();

        UserbyCompany(companyName);

    }

    private void onClick() {

        invitemembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=txt_emailaddress.getText().toString();
                if (email.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txt_emailaddress.setError("Enter Valid Email");
                    Toast.makeText(MyTeam_member.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
                }

                else {
                    invitefirend(getuserid, email);

                }
            }
        });
    }
    private void invitefirend(String userid, String email) {



        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        dialog = new ProgressDialog(MyTeam_member.this);
        dialog.setMessage("Please wait......");
        dialog.show();

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri)
                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(client))
                .build();

        Forget_Password_Api api=rest.create(Forget_Password_Api.class);

        api.invitefrinds(userid, email, new Callback<InviteFriendData>() {
            @Override
            public void success(InviteFriendData inviteFriendData, Response response) {
                String status=inviteFriendData.getSuccess();
                String message=inviteFriendData.getMessage();
                if (status.equals("1")){
                    txt_emailaddress.setText("");

                    UserbyCompany(companyName);

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    Toast.makeText(MyTeam_member.this, ""+message, Toast.LENGTH_SHORT).show();

                }else {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(MyTeam_member.this, ""+message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(MyTeam_member.this, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void findview() {


        String[] list= MySharePrafranceClass.GetSharePrefrance(MyTeam_member.this);
        companyName=list[2];
        getuserid=list[0];
        arrow=(RelativeLayout) findViewById(R.id.back_arrow_select_team);
        Memb_list=(RecyclerView)findViewById(R.id.memberList);
        followers_value= (TextView) findViewById(R.id.followers_value);
        nouserfound= (TextView) findViewById(R.id.txt_no_userdound);

        invitemembers= (Button) findViewById(R.id.ininvite_memebers);
        txt_emailaddress = (EditText) findViewById(R.id.intext_emailaddres);


        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

    }

    private void UserbyCompany(String companyName) {
        RestAdapter rest= new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        User_by_Company_Api api = rest.create(User_by_Company_Api.class);
        api.UserbyCompany(companyName, new Callback<CompanyData>() {
            @Override
            public void success(CompanyData companyData, Response response) {
                if(companyData.getSuccess()==1)
                    CompanResult=companyData.getResult();
                RecyclerView.LayoutManager mlayoutManeger = new LinearLayoutManager(MyTeam_member.this,LinearLayoutManager.VERTICAL,false);
                Memb_list.setLayoutManager(mlayoutManeger);
                menberAdapter= new AllMemberListAdapter(MyTeam_member.this,CompanResult);
                Memb_list.setAdapter(menberAdapter);
                menberAdapter.notifyDataSetChanged();

                int followerscount=CompanResult.size();

                followers_value.setText(String.valueOf(followerscount));
                if (CompanResult.size()==0)
                {
                    nouserfound.setVisibility(View.VISIBLE);
                }
                else {

                    nouserfound.setVisibility(View.GONE);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Constant.toast(MyTeam_member.this,""+error.getMessage());

            }
        });

    }
}
