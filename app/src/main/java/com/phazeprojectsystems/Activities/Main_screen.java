package com.phazeprojectsystems.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.phazeprojectsystems.R;

public class Main_screen extends AppCompatActivity {


    Button signup, signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        ANIMATION();
        FINDVIEWBYID();
        CLICKLISTINER();
    }

    private void ANIMATION() {
    }


    private void FINDVIEWBYID() {
        signup = (Button) findViewById(R.id.sign_up_main);
        signin = (Button) findViewById(R.id.sign_in_main);

    }

    private void CLICKLISTINER() {

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Main_screen.this, Login_screen.class);
                startActivity(intent);
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Main_screen.this, Register_screen.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
