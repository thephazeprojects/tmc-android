package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Adapter.AssignToAdapter;
import com.phazeprojectsystems.Adapter.Comment_adapter;
import com.phazeprojectsystems.Api.Comment_api;
import com.phazeprojectsystems.Api.Complete_Api;
import com.phazeprojectsystems.Api.Edittask_api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Constant.Network_check;
import com.phazeprojectsystems.Model.CommentModel;
import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.Create_ProjectData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.phazeprojectsystems.R.id.img_deletecomment;
import static com.phazeprojectsystems.R.id.txt_editdescription;

public class Edit_task extends AppCompatActivity {

    private TextView text_projectname, text_duedate,AssignedMember;
    private String SelecterMember="";
    private String getMemberuserid="";
    private String ggetmemberid="";
    private EditText txt_taskname, txt_description;
    private RelativeLayout submittask, due_date;
    private String member, get_duedate, get_taskname, get_description, get_taskid, getuser_id, get_projectid,get_AssignedName,get_AssignedId,getmemberid;
    private RelativeLayout edit_tag, edit_color_tag,assigned_to;
    private String  get_addtag;
    private ImageView AssignePlussIconBlue,edit_image_red, edit_image_yellow, edit_image_green,backarrow;
    private static boolean redclickedinEdit = false;
    private static boolean yellowclickedinEdit = false;
    private static boolean greenclickedinEdit = false;
    public static int EditTagCount;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;
    private RecyclerView Assigntolist;
    private LinearLayoutManager linearLayoutManager;
    private AssignToAdapter assigntoAdapter;
    public static Edit_task edit_task;

    private static ArrayList<String> selectedmember =  new ArrayList();
    public static Map<String,String> TaskMember =  new HashMap<>();
    private   List<String> MemberNAme = new ArrayList<>();
    private String[] list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task2);
        findview();
        edit_task = this;

        Intent extra = getIntent();
        if (extra!=null) {

            get_taskname = extra.getStringExtra("gettaskname");
            get_duedate = extra.getStringExtra("getdewdate");
            get_description = extra.getStringExtra("getdescription");
            get_taskid = extra.getStringExtra("gettaskid");
            getuser_id = extra.getStringExtra("getuserid");
            get_projectid = extra.getStringExtra("getprojectid");
            get_addtag = extra.getStringExtra("getaddtag");
            get_AssignedName = extra.getStringExtra("assigedusername");
            get_AssignedId = extra.getStringExtra("assigneIds");

            TaskMember.clear();

            TaskMember = Comment_screen.comment_screen.getTaskMember();

            MemberNAme.clear();

            for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                String e = entry.getKey();

                MemberNAme.add(e);
            }


try {
    settag(Integer.valueOf(get_addtag));
}catch (Exception e){}



            txt_taskname.setText(get_taskname);
            txt_description.setText(get_description);
            text_duedate.setText(get_duedate);
            AssignedMember.setText(get_AssignedName);

        }

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        clicklistiner();
        SetLayout();

    }

    public String getGet_taskid() {
        return get_taskid;
    }

    public void setGet_taskid(String get_taskid) {
        this.get_taskid = get_taskid;
    }

    private void SetLayout() {

        linearLayoutManager= new LinearLayoutManager(Edit_task.this,LinearLayoutManager.HORIZONTAL, false);
        Assigntolist.setLayoutManager(linearLayoutManager);
        assigntoAdapter = new AssignToAdapter(Edit_task.this,MemberNAme,"EditTask");
        Assigntolist.setAdapter(assigntoAdapter);
        assigntoAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();


        if (TaskMember!=null){
            if (!TaskMember.isEmpty()){

                MemberNAme.clear();


                for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                    String e = entry.getKey();

                    MemberNAme.add(e);
                }

                SetLayout();

            }
        }
    }

    private void settag(Integer get_addtag) {
        switch (get_addtag) {
            case 1 :
                setRedclickedinEdit(true);
                edit_image_red.setImageResource(R.drawable.red);
                edit_image_yellow.setImageResource(R.drawable.yellow_circle);
                edit_image_green.setImageResource(R.drawable.green_circle);
                break;
            case  2 :
                setYellowclickedinEdit(true);
                edit_image_red.setImageResource(R.drawable.red1_circle);
                edit_image_yellow.setImageResource(R.drawable.yellow);
                edit_image_green.setImageResource(R.drawable.green_circle);
                break;
            case  3 :
                setGreenclickedinEdit(true);
                edit_image_red.setImageResource(R.drawable.red1_circle);
                edit_image_yellow.setImageResource(R.drawable.yellow_circle);
                edit_image_green.setImageResource(R.drawable.cancel_green);
                break;
            case  4  :
                edit_image_red.setImageResource(R.drawable.red1_circle);
                edit_image_yellow.setImageResource(R.drawable.yellow_circle);
                edit_image_green.setImageResource(R.drawable.green_circle);
                break;
        }
    }

    private void findview() {
        AssignePlussIconBlue =(ImageView)findViewById(R.id.AssignePlussIconBlue);
        Assigntolist = (RecyclerView)findViewById(R.id.assigned_member1);
        text_projectname = (TextView) findViewById(R.id.edittask_projectname);
        text_duedate = (TextView) findViewById(R.id.edittask_date);
        txt_taskname = (EditText) findViewById(R.id.txt_editask);
        txt_description = (EditText) findViewById(txt_editdescription);
        submittask = (RelativeLayout) findViewById(R.id.submitedit_task);
        due_date = (RelativeLayout) findViewById(R.id.txt_edittext_date_Picker);
        edit_tag = (RelativeLayout) findViewById(R.id.relative_edit_tag_edittask);
        edit_color_tag = (RelativeLayout) findViewById(R.id.relative_imgcolor_edittask);
        edit_image_red = (ImageView) findViewById(R.id.imgtag_red_edittask);
        edit_image_yellow = (ImageView) findViewById(R.id.imgtag_yellow_edittask);
        edit_image_green = (ImageView) findViewById(R.id.imgtag_green_edittask);
        assigned_to= (RelativeLayout) findViewById(R.id.edit_assignedto);
        AssignedMember= (TextView) findViewById(R.id.assigned_member);
        backarrow= (ImageView) findViewById(R.id.backarrow_edittask);
        list = MySharePrafranceClass.GetSharePrefrance(Edit_task.this);
    }

    public static boolean isRedclickedinEdit() {
        return redclickedinEdit;
    }

    public static void setRedclickedinEdit(boolean redclickedinEdit) {
        Edit_task.redclickedinEdit = redclickedinEdit;
    }

    public static boolean isYellowclickedinEdit() {
        return yellowclickedinEdit;
    }

    public static void setYellowclickedinEdit(boolean yellowclickedinEdit) {
        Edit_task.yellowclickedinEdit = yellowclickedinEdit;
    }

    public static boolean isGreenclickedinEdit() {
        return greenclickedinEdit;
    }

    public static void setGreenclickedinEdit(boolean greenclickedinEdit) {
        Edit_task.greenclickedinEdit = greenclickedinEdit;
    }

    private void clicklistiner() {


        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskMember.clear();
                onBackPressed();
            }
        });

        assigned_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(Edit_task.this,Select_team_menber.class);
                intent.putExtra("get_taskid",get_taskid);
                startActivity(intent);
            }
        });

        AssignePlussIconBlue.setOnClickListener(v->{


            Intent intent=new Intent(Edit_task.this,Select_team_menber.class);
            intent.putExtra("get_taskid",get_taskid);
            startActivity(intent);
        });
        edit_image_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditTagCount=isredinEdit();
            }
        });
        edit_image_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditTagCount=isyellowinEdit();
            }
        });
        edit_image_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditTagCount=isgreeninEdit();
            }
        });
        due_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(DATE_PICKER_ID);
            }
        });


        submittask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_taskname = txt_taskname.getText().toString();
                get_duedate = text_duedate.getText().toString();
                get_description = txt_description.getText().toString().trim();

                if (get_taskname.trim().equalsIgnoreCase("")) {
                    Toast.makeText(Edit_task.this, "Enter Task name", Toast.LENGTH_SHORT).show();
                }
                else {

                    edit_task();
                }
            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.

        @Override
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay)
        {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            text_duedate.setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year).append(""));
        }
    };

    private void edit_task() {


        if (Network_check.isNetWorking(Edit_task.this)) {



        }else {
            Toast.makeText(edit_task, " Check Connection Please", Toast.LENGTH_SHORT).show();
        }

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Edit_task.this);
        progressDoalog.setTitle("Please wait..");
        progressDoalog.show();


        SelecterMember="";
        getMemberuserid="";

        if (!(TaskMember.isEmpty())) {


            for(Map.Entry<String,String> entry : TaskMember.entrySet()){

                getMemberuserid =  entry.getValue()+ "," + getMemberuserid;


                if(getMemberuserid.endsWith(",")) {

                    ggetmemberid= getMemberuserid.substring(0, getMemberuserid.length() - 1);
                }
            }

        }

        if (EditTagCount==0){
            EditTagCount=4;
        }

        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Edittask_api api = rest.create(Edittask_api.class);

        api.update_task(getuser_id, get_projectid, get_taskid, get_taskname, get_duedate,
                get_description, "",""+EditTagCount,ggetmemberid, new Callback<Create_ProjectData>()
        {
            @Override
            public void success(Create_ProjectData create_projectData, Response response) {

                String Name  = list[1];
                progressDoalog.dismiss();
                comment(getuser_id,get_taskid,get_projectid," New Task Assignee To You By  "+Name,null);
                Intent intent = new Intent(Edit_task.this, Task_activity.class);
                startActivity(intent);
            }

            @Override
            public void failure(RetrofitError error) {

                progressDoalog.dismiss();
                Toast.makeText(Edit_task.this, "Failureee", Toast.LENGTH_SHORT).show();
            }
        });
    }

private void comment(String user_id, String Task_Id, String ProjectId,String Comment,String UploadedFile1) {

    TypedFile UploadedFile = null;
    if (UploadedFile1 != null) {
        UploadedFile = new TypedFile("image/*", new File(String.valueOf(UploadedFile1)));

    }

    RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
    Comment_api api = rest.create(Comment_api.class);

    api.comment(user_id, Task_Id,ProjectId, Comment,UploadedFile, new Callback<CommentModel>() {
        @Override
        public void success(CommentModel commentModel, Response response) {


            Long status = commentModel.getSuccess();

            if (status == 1) {

                Toast.makeText(Edit_task.this, "Sucessfull", Toast.LENGTH_SHORT).show();



//                    pd.dismiss();
//
//                comntList2 = commentModel.getCommentList();
//                recycler.setVisibility(View.VISIBLE);
//                commentadapter2 = new Comment_adapter(Comment_screen.this, comntList2);
//                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Comment_screen.this);
//                recycler.setLayoutManager(mLayoutManager);
//                comntLists.clear();
//                recycler.setAdapter(commentadapter2);
//                commentadapter2.notifyDataSetChanged();
            }

        }

        @Override
        public void failure(RetrofitError error) {

            Toast.makeText(Edit_task.this, "Failureee" + error, Toast.LENGTH_SHORT).show();
        }
    });
}
    private int isgreeninEdit() {
        if (!isGreenclickedinEdit()) {

            setGreenclickedinEdit(true);
            setRedclickedinEdit(false);
            setYellowclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red1_circle);
            edit_image_yellow.setImageResource(R.drawable.yellow_circle);
            edit_image_green.setImageResource(R.drawable.cancel_green);
            return 3;
        } else {

            setRedclickedinEdit(false);
            setYellowclickedinEdit(false);
            setGreenclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red1_circle);
            edit_image_yellow.setImageResource(R.drawable.yellow_circle);
            edit_image_green.setImageResource(R.drawable.green_circle);
            return 4;

        }
    }
    private int isyellowinEdit(){
        if(!isYellowclickedinEdit()){
            setYellowclickedinEdit(true);
            setRedclickedinEdit(false);
            setGreenclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red1_circle);
            edit_image_yellow.setImageResource(R.drawable.yellow);
            edit_image_green.setImageResource(R.drawable.green_circle);
            return 2;

        }else{
            setRedclickedinEdit(false);
            setYellowclickedinEdit(false);
            setGreenclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red1_circle);
            edit_image_yellow.setImageResource(R.drawable.yellow_circle);
            edit_image_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }
    private int  isredinEdit(){
        if(!isRedclickedinEdit()){
            setRedclickedinEdit(true);
            setGreenclickedinEdit(false);
            setYellowclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red);
            edit_image_yellow.setImageResource(R.drawable.yellow_circle);
            edit_image_green.setImageResource(R.drawable.green_circle);
            return 1;
        }
        else{
            setRedclickedinEdit(false);
            setYellowclickedinEdit(false);
            setGreenclickedinEdit(false);
            edit_image_red.setImageResource(R.drawable.red1_circle);
            edit_image_yellow.setImageResource(R.drawable.yellow_circle);
            edit_image_green.setImageResource(R.drawable.green_circle);
            return 4;
        }
    }
    public static void AddTaskMember( Map <String,String> member) {

        TaskMember=member;
        for (int index=0 ; index < member.size();index++){
        }}

     public static void AddMember(String menber,String userid){
        selectedmember.add(menber);
         selectedmember.add(userid);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TaskMember.clear();
    }
}
