package com.phazeprojectsystems.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phazeprojectsystems.Api.Edit_project_api;
import com.phazeprojectsystems.Api.ProjectDetail_Api;
import com.phazeprojectsystems.Constant.Constant;
import com.phazeprojectsystems.Model.Create_ProjectData;
import com.phazeprojectsystems.Model.ProjectDetailData;
import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.phazeprojectsystems.R.id.img_Edit_project;

public class Edit_project extends AppCompatActivity {
    EditText txt_projectName, txt_discription, txt_address;
    private TextView TitleProjectName, txt_date;
    private RelativeLayout Editproject;
    private String[] itemList;
    String get_projectname, get_projectdate, get_projectdescription, get_projectaddress,projectid;
    private int year,month,day;
    private String[] list;
    static final int DATE_PICKER_ID = 1111;
    RelativeLayout backarrow_editproject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);
        findView();
        list = MySharePrafranceClass.GetSharePrefrance(Edit_project.this);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        txt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_PICKER_ID);
            }
        });

        Onclick();
        itemList = MySharePrafranceClass.ProjectGetSharePrefrance(Edit_project.this);
        TitleProjectName.setText(itemList[1]);
        projectid = itemList[0];
        projectDetail(projectid);
    }

    private void findView() {
        txt_projectName = (EditText) findViewById(R.id.Edit_project_name);
        txt_discription = (EditText) findViewById(R.id.Edit_Project_discription);
        txt_address = (EditText) findViewById(R.id.Edit_Project_Address);
        Editproject = (RelativeLayout) findViewById(img_Edit_project);
        TitleProjectName = (TextView) findViewById(R.id.Edit_project_title);
        txt_date = (TextView) findViewById(R.id.Edit_Project_Date);
        backarrow_editproject= (RelativeLayout) findViewById(R.id.backarrow_Button_editproject);
    }

    private void projectDetail(final String projectid) {
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        ProjectDetail_Api api = rest.create(ProjectDetail_Api.class);
        api.ProjectDetil(projectid, new Callback<ProjectDetailData>() {
            @Override
            public void success(ProjectDetailData projectDetailData, Response response) {
                if (projectDetailData.getSuccess() == 1) {
                    String discripton = projectDetailData.getResult().get(0).getDescription();
                    String address = projectDetailData.getResult().get(0).getAddress();
                    String project = projectDetailData.getResult().get(0).getProjactName();
                    String date = projectDetailData.getResult().get(0).getDue_date();
                    txt_discription.setText(discripton);
                    txt_address.setText(address);
                    txt_projectName.setText(project);
                    txt_projectName.setText(itemList[1]);
                    txt_date.setText(date);
                } else {
                    Toast.makeText(Edit_project.this, "Some Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void Onclick() {

        backarrow_editproject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(Edit_project.this,Task_activity.class);
                startActivity(intent);
            }
        });


        Editproject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_projectname = txt_projectName.getText().toString();
                get_projectdate = txt_date.getText().toString();
                get_projectdescription = txt_discription.getText().toString().trim();
                get_projectaddress = txt_address.getText().toString();

                if (get_projectname.trim().equalsIgnoreCase("")) {
                    Toast.makeText(Edit_project.this, "Enter Project name", Toast.LENGTH_SHORT).show();
                } else {
                    edit_project();
                }
            }
        });
    }

    private void edit_project() {


        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Edit_project.this);
        progressDoalog.setTitle("Please wait..");
        progressDoalog.show();




        String userid = list[0];
        RestAdapter rest = new RestAdapter.Builder().setEndpoint(Constant.Uri).build();
        Edit_project_api api = rest.create(Edit_project_api.class);

        api.editproject(userid, projectid, get_projectname, get_projectdate, get_projectdescription, get_projectaddress, new Callback<Create_ProjectData>() {
            @Override
            public void success(Create_ProjectData create_projectData, Response response) {

                String status = create_projectData.getSuccess();

                if (status.equalsIgnoreCase("1")) {

                    progressDoalog.dismiss();
                    Intent intent = new Intent(Edit_project.this, MainActivity.class);
                    startActivity(intent);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                progressDoalog.dismiss();
                Toast.makeText(Edit_project.this, "Please check Network Conneciton", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;


            txt_date.setText(new StringBuilder().append(month + 1)
                    .append("/").append(day).append("/").append(year)
                    .append(""));

        }
    };
}
