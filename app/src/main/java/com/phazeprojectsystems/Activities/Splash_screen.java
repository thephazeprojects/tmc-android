package com.phazeprojectsystems.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.phazeprojectsystems.R;
import com.phazeprojectsystems.SharePool.MySharePrafranceClass;

import static android.os.Build.VERSION_CODES.M;

public class Splash_screen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    static final int REQUEST_CODE_STORAGE_PERMS = 101;
    String userid;
    String userrole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);


        String[] list = MySharePrafranceClass.GetSharePrefrance(Splash_screen.this);
        userid = list[0];
        userrole = list[4];

        if (hasPermissions()) {
            nextScreen();
        } else {
            requestNecessaryPermissions();

        }

    }

    private void nextScreen() {

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {


                if (!(userid == null)) {

                    if (!(userrole == null)) {

                        if (userrole.equals("1")) {
                            Intent intent = new Intent(Splash_screen.this, MainActivity.class);
                            startActivity(intent);
                        } else {

                            Intent intent = new Intent(Splash_screen.this, Userproject_screen.class);
                            startActivity(intent);
                        }
                    }
                } else {


                    Intent intent = new Intent(Splash_screen.this, Main_screen.class);
                    startActivity(intent);
                }
                finish();


            }
        }, SPLASH_TIME_OUT);

    }

    @SuppressLint("WrongConstant")
    private boolean hasPermissions() {
        int res = 0;

        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        for (String perms : permissions) {
            res = checkCallingOrSelfPermission(perms);
            if (!(res == PackageManager.PERMISSION_GRANTED)) {

                return false;
            }

        }

        return true;
    }

    private void requestNecessaryPermissions() {

        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= M) {

            requestPermissions(permissions, REQUEST_CODE_STORAGE_PERMS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grandResults) {

        boolean allowed = true;
        switch (requestCode) {
            case REQUEST_CODE_STORAGE_PERMS:
                for (int res : grandResults) {

                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:

                allowed = false;
                break;
        }
        if (allowed) {
            nextScreen();

        } else {

            if (Build.VERSION.SDK_INT >= M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(getApplicationContext(), "Camera Permissions denied", Toast.LENGTH_SHORT).show();
                } else if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getApplicationContext(), "Storage Permissions denied", Toast.LENGTH_SHORT).show();
                }

            }

        }

    }
}
