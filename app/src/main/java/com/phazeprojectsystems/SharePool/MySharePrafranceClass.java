package com.phazeprojectsystems.SharePool;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.stream.Stream;

/**
 * Created by Ravi on 8/3/2017.
 */

public class MySharePrafranceClass {

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String id = "key";
    public static final String name="name";
    public static final String company="company";
    public static final String userimage="image";
    public static final String Userrole="role";


    public static final String[] list = new String[6];


    public static final String MyProjectPREFERENCES = "MyProjectPrefs";
    public static final String Projectid = "projectkey";
    public static final String Projectname="projectname";
    public static final String[] itemlist = new String[3];


    public static final String MyLOGINPREFERENCES = "MyLoginPrefs";
    public static final String Email = "Email";
    public static final String Password="Password";
    public static final String[] loginlist = new String[3];

    public static void Share( Activity activity,String userid,String username,String companyname,String image,String role) {

       SharedPreferences sharedPreferences = activity.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(id,userid);
        editor.putString(name,username);
        editor.putString(company,companyname);
        editor.putString(userimage,image);
        editor.putString(Userrole,role);

        editor.commit();
    }
    public static void ProjectShare(Context activity, String projectid, String projectname) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(MyProjectPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Projectid,projectid);
        editor.putString(Projectname,projectname);
        editor.commit();
    }
    public static void LoginShare(Context activity, String email, String password) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(MyLOGINPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Email,email);
        editor.putString(Password,password);
        editor.commit();
    }
    public static void ClearAll(Activity activity) {
       SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        Share(activity,null,null,null,null,null);
        sharedPreferences.edit().clear().commit();
    }
    public static void ClearAllProjectShare(Activity activity) {
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        ProjectShare(activity,null,null);
        sharedPreferences.edit().clear().commit();
    }

    public static void ClearAllLoginShare(Activity activity) {
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        LoginShare(activity,null,null);
        sharedPreferences.edit().clear().commit();
    }
    public static String[] GetSharePrefrance(Activity activity)
    {
        SharedPreferences sharedPreferences=activity.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        list[0]=(sharedPreferences.getString(id ,null));
        list[1]=(sharedPreferences.getString(name,null));
        list[2]=(sharedPreferences.getString(company,null));
        list[3]=(sharedPreferences.getString(userimage,null));
        list[4]=(sharedPreferences.getString(Userrole,null));


        return list;
    }
    public static String[] ProjectGetSharePrefrance(Activity activity)
    {
        SharedPreferences sharedPreferences=activity.getSharedPreferences(MyProjectPREFERENCES,Context.MODE_PRIVATE);
        itemlist[0]=(sharedPreferences.getString(Projectid ,null));
        itemlist[1]=(sharedPreferences.getString(Projectname,null));
        return itemlist;
    }
    public static String[] LoginGetSharePrefrance(Activity activity)
    {
        SharedPreferences sharedPreferences=activity.getSharedPreferences(MyLOGINPREFERENCES,Context.MODE_PRIVATE);
        loginlist[0]=(sharedPreferences.getString(Email ,null));
        loginlist[1]=(sharedPreferences.getString(Password,null));
       // System.out.println(Stream.of(list).anyMatch(X ->X == "Vishal"));
        return loginlist;
    }

}