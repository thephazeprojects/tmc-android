package com.phazeprojectsystems.SharePool;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by user on 17/1/18.
 */

public class NotificationData extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "PhaseNotificationDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "PhaseNotification";
    public static final String COLUMN_MSG = "Messege";
    public static final String COLUMN_id = "id";
    public static final String TIMEOFNOTIFICATION = "dateTimeInfo";

    public static final  String gettaskname = "TaskName";
    public static final String  getduwdate= "DueDate";
    public static final String  getdescription = "TaskDiscription";
    public static final String  gettaskid = "Task_Id";
    public static final String  getaddtag = "Tag";



    private static final String NEWTABLE_NAME = "NEWPhaseNotification";
    public static final String NEWCOLUMN_MSG = "NEWMessege";
    public static final String NEWCOLUMN_id = "id";
    public static final String NEWTIMEOFNOTIFICATION = "NEWdateTimeInfo";

    public static final  String NEWgettaskname = "NEWTaskName";
    public static final String  NEWgetduwdate= "NEWDueDate";
    public static final String  NEWgetdescription = "NEWTaskDiscription";
    public static final String  NEWgettaskid = "NEWTask_Id";
    public static final String  NEWgetaddtag = "NEWTag";



    public NotificationData(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }


    public void  AddNotification(SQLiteDatabase db ,String msg ,String Timeofnotification,String taskname,
                                 String duwdate,String description, String taskid ,String addtag){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_MSG,msg);
        contentValues.put(TIMEOFNOTIFICATION,Timeofnotification);
        contentValues.put(gettaskname,taskname);
        contentValues.put(getduwdate,duwdate);
        contentValues.put(getdescription,description);
        contentValues.put(gettaskid,taskid);
        contentValues.put(getaddtag,addtag);
        db.insert(TABLE_NAME,null,contentValues);
    }

    public void  NewAddNotification(SQLiteDatabase db ,String msg ,String Timeofnotification,String taskname,
                                 String duwdate,String description, String taskid ,String addtag){
        ContentValues contentValues = new ContentValues();
        contentValues.put(NEWCOLUMN_MSG,msg);
        contentValues.put(NEWTIMEOFNOTIFICATION,Timeofnotification);
        contentValues.put(NEWgettaskname,taskname);
        contentValues.put(NEWgetduwdate,duwdate);
        contentValues.put(NEWgetdescription,description);
        contentValues.put(NEWgettaskid,taskid);
        contentValues.put(NEWgetaddtag,addtag);
        db.insert(NEWTABLE_NAME,null,contentValues);
    }


    public void Deletenotification(SQLiteDatabase sqLiteDatabase,int ID){

        String delete = " DELETE FROM "+TABLE_NAME+" WHERE  "+COLUMN_id+" = " +ID+";";
        sqLiteDatabase.execSQL(delete);
    }

    public void NewDeletenotification(SQLiteDatabase sqLiteDatabase,int ID){

        String delete = " DELETE FROM "+NEWTABLE_NAME+" WHERE  "+COLUMN_id+" = " +ID+";";
        sqLiteDatabase.execSQL(delete);
    }


    public Cursor GetNotification(SQLiteDatabase db){
        String[] projection ={COLUMN_id,COLUMN_MSG,TIMEOFNOTIFICATION,gettaskname,getduwdate,getdescription,gettaskid,getaddtag};

        return (db.query(TABLE_NAME,projection,null,null,null,null,null));

    }


    public Cursor NewGetNotification(SQLiteDatabase db){
        String[] projection ={NEWCOLUMN_id,NEWCOLUMN_MSG,NEWTIMEOFNOTIFICATION,NEWgettaskname,NEWgetduwdate,NEWgetdescription,NEWgettaskid,NEWgetaddtag};

        return (db.query(NEWTABLE_NAME,projection,null,null,null,null,null));

    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String create ="CREATE TABLE  "+TABLE_NAME+" (  "
                + COLUMN_id +"  INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_MSG +"  varchar(255),"
                + TIMEOFNOTIFICATION +"  varchar(255),"
                + gettaskname +"  varchar(255),"
                + getduwdate +"  varchar(255),"
                + getdescription +"  varchar(255),"
                + gettaskid +"  varchar(255),"
                + getaddtag +"  varchar(255));";


        String Newcreate ="CREATE TABLE  "+NEWTABLE_NAME+" (  "
                + NEWCOLUMN_id +"  INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NEWCOLUMN_MSG +"  varchar(255),"
                + NEWTIMEOFNOTIFICATION +"  varchar(255),"
                + NEWgettaskname +"  varchar(255),"
                + NEWgetduwdate +"  varchar(255),"
                + NEWgetdescription +"  varchar(255),"
                + NEWgettaskid +"  varchar(255),"
                + NEWgetaddtag +"  varchar(255));";

        sqLiteDatabase.execSQL(create);

        sqLiteDatabase.execSQL(Newcreate);
    }

    public  void RemoveNewNotification(SQLiteDatabase sqLiteDatabase){


       // db.execSQL(""+ TABLE_NAME);

        String query = "delete from "+NEWTABLE_NAME;
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

        String newsql = "DROP TABLE IF EXISTS " + NEWTABLE_NAME + ";";
        sqLiteDatabase.execSQL(sql);
        sqLiteDatabase.execSQL(newsql);
        onCreate(sqLiteDatabase);
    }
}
