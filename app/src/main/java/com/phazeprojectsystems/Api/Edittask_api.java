package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.AssignModle;
import com.phazeprojectsystems.Model.Create_ProjectData;
import com.phazeprojectsystems.Model.UnAssignModle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by NAVJOT SINGH on 02-10-2017.
 */

public interface Edittask_api {


    @Multipart
    @POST("/update_task")
    public void update_task(@Part("userid") String userid,
                            @Part("projectid") String projectid,
                            @Part("task_id")String taskid,
                            @Part("taskname")String taskname,
                            @Part("due_date")String due_date,
                            @Part("description")String description,
                            @Part("create_date") String create_date,
                            @Part("add_tag") String add_tag,
                            @Part("add_member") String add_member,
                            Callback<Create_ProjectData> res);


    @Multipart
    @POST("/unassignedtask")
    public void unassignedtask(@Part("user_id") String userid,
                               @Part("utask_id") String utask_id,
                               @Part("aduser_id") String aduser_id,
                               Callback<UnAssignModle> res);

    @Multipart
    @POST("/assign_task")
    public void assignedtask(@Part("userid") String userid,
                               @Part("taskid") String taskid,
                               @Part("projectid") String projectid,
                               Callback<AssignModle> res);


}
