package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.DeleteTaskData;

//import okhttp3.Call;
import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/11/2017.
 */

public interface DeleteTask_Api {
    @Multipart
    @POST("/delete_task")
    void Delete(@Part("task_id")String task_id, Callback<DeleteTaskData> res);
}
