package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Completetasklistmodel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/8/2017.
 */

public interface GetCompleteTask_Api {
    @Multipart
    @POST("/gettasklistcomplete")
    void get_complete_task(@Part("user_id")String userid ,
                           @Part("project_id") String projectid,
                           Callback<Completetasklistmodel> res);
}
