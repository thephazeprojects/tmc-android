package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.CompleteProject;
import com.phazeprojectsystems.Model.CompleteProjectModle;
import com.phazeprojectsystems.Model.DeleteProjectModel;
import com.phazeprojectsystems.Model.IncompleteProject_modle;
import com.phazeprojectsystems.Model.PermanentDeleteModle;
import com.phazeprojectsystems.Model.Project_Data;
import com.phazeprojectsystems.Model.RestoreData_modle;
import com.phazeprojectsystems.Model.UserprojectModel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/22/2017.
 */

public interface Get_User_Project {
    @Multipart
    @POST("/getall_project")
     void GetProject(@Part("userid") String userid,
                            Callback<Project_Data> res);

    @Multipart
    @POST("/userproject")
    void Getuserproject(@Part("user_id") String userid,
                    Callback<UserprojectModel> res);

    @Multipart
    @POST("/allcomplete_project")
    void GetCompleteProject(@Part("user_id") String userid,
                    Callback<CompleteProjectModle> res);
    @Multipart
    @POST("/allincomplete_project")
    void GetInCompleteProject(@Part("user_id") String userid,
                            Callback<IncompleteProject_modle> res);
    @Multipart
    @POST("/completeproject")
    void CompleteProject(@Part("user_id") String userid,
                         @Part("project_id") String project_id,
                              Callback<CompleteProject> res);

    @Multipart
    @POST("/incompleteproject")
    void InCompleteProject(@Part("user_id") String userid,
                           @Part("project_id") String project_id,
                             Callback<CompleteProject> res);


    @Multipart
    @POST("/allrecycleproject")
    void GetDeletedProject(@Part("userid") String userid,
                              Callback<DeleteProjectModel> res);



    @Multipart
    @POST("/permanentdeleteproject")
    void PermanentDelete(@Part("project_id") String project_id,
                           Callback<PermanentDeleteModle> res);


    @Multipart
    @POST("/restorerecycleproject")
    void RestoreProject(@Part("project_id") String project_id,
                         Callback<RestoreData_modle> res);
}
