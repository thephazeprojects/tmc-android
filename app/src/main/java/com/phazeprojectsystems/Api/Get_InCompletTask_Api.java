package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.InCompletetasklistmodel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/11/2017.
 */

public interface Get_InCompletTask_Api {
    @Multipart
    @POST("/gettasklistpending")
    void GetInCompletTask(@Part("user_id")String userid,
                          @Part("project_id") String projectid,
                          Callback<InCompletetasklistmodel> res);
}
