package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ForgetPasswordData;
import com.phazeprojectsystems.Model.InviteFriendData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/2/2017.
 */

public interface Forget_Password_Api {
    @Multipart
    @POST("/forget_password")
    void Forget_Password(@Part("email")String email,Callback<ForgetPasswordData> responce);



    @Multipart
    @POST("/invitefriends")
    void invitefrinds(@Part("user_id")String user_id,
                      @Part("email")String email,
                      Callback<InviteFriendData> res);

}










