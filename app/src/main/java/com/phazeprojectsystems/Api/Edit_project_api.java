package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Create_ProjectData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by NAVJOT SINGH on 02-10-2017.
 */

public interface Edit_project_api {

    @Multipart
    @POST("/update_project")
    public void editproject(@Part("userid") String userid,
                              @Part("project_id") String projectid,
                              @Part("projectname")String projectname,
                              @Part("due_date")String due_date,
                              @Part("description")String description,
                              @Part("address") String address,Callback<Create_ProjectData> res);
}
