package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ProjectDetailData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by NAVJOT SINGH on 28-09-2017.
 */

public interface ProjectDetail_Api {
    @Multipart
    @POST("/project_details")
    void ProjectDetil(@Part("project_id") String project_id, Callback<ProjectDetailData> res);
}
