package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.DuedateAlert_modle;
import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;


/**
 * Created by user on 22/1/18.
 */

public interface DueDateAlert_Api {
    @POST("/projectduedatenotification")
    void DueDateAlert(Callback<DuedateAlert_modle> res);
}
