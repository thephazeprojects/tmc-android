package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.AssigneeByTask_model;
import com.phazeprojectsystems.Model.CompanyData;
import com.phazeprojectsystems.Model.DeleteMemberUser_modle;
import com.phazeprojectsystems.Model.Get_Assigned_User_modle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/19/2017.
 */

public interface User_by_Company_Api
   {

    @Multipart
    @POST("/user_by_company")
    void UserbyCompany(@Part("company_name")String company_name,Callback<CompanyData> res);



       @Multipart
       @POST("/deleteinviteuser")
       void DeleteCompanyUser(@Part("user_id")String user_id,Callback<DeleteMemberUser_modle> res);


       @Multipart
       @POST("/userassignedtotask")
       void  getAssignedUser(@Part("task_id")String task_id,Callback<Get_Assigned_User_modle> res);



       @Multipart
       @POST("/excludetaskuser")
       void  AssigneeUserBuyTask(@Part("taskid")String task_id,
                                 @Part("companyname")String companyname,
                                 Callback<AssigneeByTask_model> res);



   }
