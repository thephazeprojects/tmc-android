package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.UploadPicData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by Ravi on 9/20/2017.
 */

public interface UploadPic_Api {
    @Multipart
    @POST("/upload_pic")
    void uploadpic(@Part("userid")String userid,
                   @Part("image")TypedFile image,
                   Callback<UploadPicData> res);
}