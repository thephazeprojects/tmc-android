package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ChangeEmailData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by NAVJOT SINGH on 23-11-2017.
 */

public interface Token_submit_Api {


    @Multipart
    @POST("/submittoken")
    void tokensubmit(@Part("token")String userid,
                   @Part("user_id")String image,
                     Callback<ChangeEmailData> res);


}
