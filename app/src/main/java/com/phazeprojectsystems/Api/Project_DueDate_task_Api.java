package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;
import com.phazeprojectsystems.Model.Project_Due_date_Task_modle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by user on 3/1/18.
 */

public interface Project_DueDate_task_Api {
    @Multipart
    @POST("/get_project_all_duedate_task")
    void getProjectDueDateTask(@Part("project_id")String project_id,
                                     Callback<Project_Due_date_Task_modle> res);

}
