package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ProjectAllTaskData;
import com.phazeprojectsystems.Model.Usertasklistmodel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Vishal on 9/27/2017.
 */

public interface ProjectAllTask_Api {
    @Multipart
    @POST("/getlistoftask")
    void projectAlltask(@Part("userid")String userid,
                        @Part("project_id")String project_id , Callback<ProjectAllTaskData> res);


    @Multipart
    @POST("/gettasklist")
    void usertasklist(@Part("user_id")String userid,
                      @Part("project_id")String project_id,
                        Callback<Usertasklistmodel> res);


}
