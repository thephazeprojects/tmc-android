package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Get_Project_CompleteTask_modle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by user on 3/1/18.
 */

public interface Project_Complete_task_Api {
    @Multipart
    @POST("/get_project_complete_task")
    void getProjectcompleteTask(@Part("project_id")String project_id,
                                     Callback<Get_Project_CompleteTask_modle> res);
}
