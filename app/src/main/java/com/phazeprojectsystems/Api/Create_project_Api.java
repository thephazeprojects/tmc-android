package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Create_ProjectData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/17/2017.
 */

public interface Create_project_Api {
    @Multipart
    @POST("/create_project")
    public void Createproject(@Part("userid") String userid,
                              @Part("projectname")String projectname,
                              @Part("address") String address,
                              @Part("description")String description,
                              @Part("due_date")String due_date,Callback<Create_ProjectData> res);
}
