package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.GetAllTaskData;
import com.phazeprojectsystems.Model.Get_In_complete_Project_task_modle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by user on 3/1/18.
 */

public interface Project_Incomplete_Task_Api {
    @Multipart
    @POST("/get_project_incomplete_task")
    void getProjectAllIncompleteTask(@Part("project_id")String project_id,
                    Callback<Get_In_complete_Project_task_modle> res);



}
