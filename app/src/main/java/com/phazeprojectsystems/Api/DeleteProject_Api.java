package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.Commentdelete_model;
import com.phazeprojectsystems.Model.DeleteProjectData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;


public interface DeleteProject_Api {
    @Multipart
    @POST("/delete_project")
    void DeleteProject(
                       @Part("project_id")String project_id,Callback<DeleteProjectData> res);


    @Multipart
    @POST("/deletecomments")
    void Deletecomment(@Part("commentsid") String deletecomments,
                       Callback<Commentdelete_model> res);






    @Multipart
    @POST("/delete_user")
    void deleteuser(@Part("user_id") String userid,
                       Callback<Commentdelete_model> res);
}