package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ChangePasswordData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/2/2017.
 */

public interface Change_Password_Api {
    @Multipart
    @POST("/change_passowrd")
    void Changepassword(@Part("id")String id,
                        @Part("oldpassword")String oldpassword,
                        @Part("newpassword")String newpassword,
                        @Part("repeatnewpassword")String repeatnewpassword,
                        Callback<ChangePasswordData>res);}
