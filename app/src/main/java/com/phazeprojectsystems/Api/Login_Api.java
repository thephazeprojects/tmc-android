package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.LoginData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/2/2017.
 */

public interface Login_Api {
    @Multipart
    @POST("/signin")
    void userlogin(@Part("email") String email,
                   @Part("password")String password, Callback<LoginData> responce);}

