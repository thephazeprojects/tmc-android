package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.RegisterData;
import com.phazeprojectsystems.Model.Writereviewmodel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/2/2017.
 */

public interface Register_Api {
    @Multipart
    @POST("/user_register")
    void getregister(@Part("username") String username,
                     @Part("email")  String email,
                     @Part("companyname")String companyname,
                     @Part("password")String password,
                     @Part("confirm_password")String confirm_password,
                     @Part("loginwithfacebook") String loginwithfacebook,
                     @Part("loginwithgoogleplus") String loginwithgoogleplus,
                     Callback<RegisterData> responce);


    @Multipart
    @POST("/contact")
    void writereview(@Part("name") String username,
                     @Part("email")  String email,
                     @Part("messasge")String companyname,

                     Callback<Writereviewmodel> responce
    );


}
