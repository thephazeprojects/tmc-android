package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.CompleteTaskModel;
import com.phazeprojectsystems.Model.InCompleteTaskModle;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by user on 4/1/18.
 */

public interface In_Complete_Api {

    @Multipart
    @POST("/incomplete")
    void InComplete(@Part("userid")String userid,
                    @Part("task_id") String task_id,
                    @Part("time") String time,
                    Callback<InCompleteTaskModle> res);
}
