package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.GetProfilePicData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/20/2017.
 */

public interface GetProfilepic_Api {
   @Multipart
    @POST("/profile_pic")
    void GetProfilePic(@Part("userid")String userid,Callback<GetProfilePicData> res);
}
