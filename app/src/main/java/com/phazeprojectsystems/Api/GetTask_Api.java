package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.GetAllTaskData;
import com.phazeprojectsystems.Model.GetTaskModel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/5/2017.
 */

public interface GetTask_Api {
     @Multipart
     @POST("/get_task")
    void getAllTask(@Part("userid")String userid,
                 Callback<GetAllTaskData> res);
}
