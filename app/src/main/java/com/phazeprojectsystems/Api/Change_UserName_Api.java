package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ChangeUserNameData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/9/2017.
 */

public interface Change_UserName_Api {
    @Multipart
    @POST("/change_username")
    void ChnageUserName(@Part("id")String id,
                        @Part("username")String username, Callback<ChangeUserNameData>res);
}
