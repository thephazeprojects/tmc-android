package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.AllcommentModel;
import com.phazeprojectsystems.Model.CommentModel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by NAVJOT SINGH on 03-10-2017.
 */

public interface Comment_api {

    @Multipart
    @POST("/comment_task")
    void comment(@Part("userid") String userid,
                 @Part("taskid") String task_id,
                 @Part("projectid") String projectid,
                 @Part("comment") String comment,
                 @Part("image") TypedFile image,
                 Callback<CommentModel> res);

    @Multipart
    @POST("/all_comments")
    void all_comments(@Part("userid") String userid,
                      @Part("taskid") String task_id,
                      @Part("projectid") String projectid,
                      Callback<AllcommentModel> res);


}
