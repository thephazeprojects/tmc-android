package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.CompleteTaskModel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 9/8/2017.
 */

public interface Complete_Api {
    @Multipart
    @POST("/complete")
    void Complete(@Part("userid")String userid,
                  @Part("task_id") String task_id,
                  @Part("time")String time
                    ,Callback<CompleteTaskModel> res);
}
