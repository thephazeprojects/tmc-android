package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.CreateTaskData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/31/2017.
 */

public interface Create_Task_Api {
    @Multipart
    @POST("/create_task")
    void CreateTask(@Part("userid")String userid,
                   @Part("projectid")String projectid,
                   @Part("taskname")String taskname,
                   @Part("description")String description,
                   @Part("create_date") String create_date,
                    @Part("due_date")String due_date,
                    @Part("add_tag")String add_tag,
                    @Part("add_member")String addmenber,
                    Callback<CreateTaskData> Res);
}
