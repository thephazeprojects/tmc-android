package com.phazeprojectsystems.Api;

import com.phazeprojectsystems.Model.ChangeEmailData;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Ravi on 8/4/2017.
 */

public interface Change_Email_Api {
     @Multipart
    @POST("/change_email")
    void ChangeEmail(@Part("id") String id,
                     @Part("email")String email,
                     Callback<ChangeEmailData> responce);}
