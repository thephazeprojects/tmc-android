package com.phazeprojectsystems.Model;

/**
 * Created by user on 5/1/18.
 */

public class CompleteProject {
    String success;
    String result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
