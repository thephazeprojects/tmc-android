package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 8/4/2017.
 */

public class ChangeEmailData {
    String success;
    String message;
    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
