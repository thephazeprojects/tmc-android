package com.phazeprojectsystems.Model;

/**
 * Created by user on 21/2/18.
 */

public class AssigneeByTask_Result_model {
    private String id;
    private String username ;
    private String email ;
    private String companyname;
    private String password;
    private String login_with_facebook;
    private String login_with_google;
    private String userStatus;
    private String userimage ;
    private String thumb;
    private String parentid;
    private String role;

    private boolean isSelected=false;


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin_with_facebook() {
        return login_with_facebook;
    }

    public void setLogin_with_facebook(String login_with_facebook) {
        this.login_with_facebook = login_with_facebook;
    }

    public String getLogin_with_google() {
        return login_with_google;
    }

    public void setLogin_with_google(String login_with_google) {
        this.login_with_google = login_with_google;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserimage() {
        return userimage;
    }

    public void setUserimage(String userimage) {
        this.userimage = userimage;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
