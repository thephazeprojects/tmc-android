package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 9/21/2017.
 */

public class DeleteProjectData {
    private String msg;
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
