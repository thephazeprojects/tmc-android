package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 21/2/18.
 */

public class AssigneeByTask_model {
    private String success;
     private List<AssigneeByTask_Result_model> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<AssigneeByTask_Result_model> getResult() {
        return result;
    }

    public void setResult(List<AssigneeByTask_Result_model> result) {
        this.result = result;
    }
}
