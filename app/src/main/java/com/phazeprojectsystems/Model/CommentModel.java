
package com.phazeprojectsystems.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;



/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class CommentModel {

  private Long success;
  private String time;
  private String comment;
  private String username;
  @SerializedName("comment_list")
  private List<CommentLis> commentList;
  private String thumb;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public String getTime() {
    return time;
  }
  
  public void setTime(String time) {
    this.time = time;
  }

  public String getComment() {
    return comment;
  }
  
  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }

  public List<CommentLis> getCommentList() {
    return commentList;
  }
  
  public void setCommentList(List<CommentLis> commentList) {
    this.commentList = commentList;
  }

  public String getThumb() {
    return thumb;
  }
  
  public void setThumb(String thumb) {
    this.thumb = thumb;
  }

}
