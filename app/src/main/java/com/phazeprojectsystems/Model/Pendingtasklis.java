
package com.phazeprojectsystems.Model;


import com.google.gson.annotations.SerializedName;



/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class Pendingtasklis {

  @SerializedName("task_id")
  private String taskId;
  @SerializedName("user_id")
  private String userId;
  @SerializedName("project_id")
  private String projectId;
  @SerializedName("task_name")
  private String taskName;
  @SerializedName("description")
  private String description;
  @SerializedName("status")
  private String status;
  @SerializedName("create_date")
  private String createDate;
  @SerializedName("due_date")
  private String dueDate;
  @SerializedName("add_tag")
  private Integer addTag;
  @SerializedName("add_member")
  private String addMember;
  @SerializedName("update_time")
  private String updatetime;


  public String getTaskId() {
    return taskId;
  }
  
  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  public String getUserId() {
    return userId;
  }
  
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getProjectId() {
    return projectId;
  }
  
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getTaskName() {
    return taskName;
  }
  
  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }

  public String getStatus() {
    return status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }

  public String getCreateDate() {
    return createDate;
  }
  
  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }

  public String getDueDate() {
    return dueDate;
  }
  
  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public Integer getAddTag() {
    return addTag;
  }
  
  public void setAddTag(Integer addTag) {
    this.addTag = addTag;
  }

  public String getAddMember() {
    return addMember;
  }
  
  public void setAddMember(String addMember) {
    this.addMember = addMember;
  }

  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }
}
