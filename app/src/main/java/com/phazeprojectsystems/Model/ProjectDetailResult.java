package com.phazeprojectsystems.Model;

/**
 * Created by NAVJOT SINGH on 28-09-2017.
 */

public class ProjectDetailResult {
    private String projectId;
    private String userId;
    private String projactName;
    private String date;
    private String address;
    private String description;

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    private String due_date;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjactName() {
        return projactName;
    }

    public void setProjactName(String projactName) {
        this.projactName = projactName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
