
package com.phazeprojectsystems.Model;

import java.util.List;




/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class InCompletetasklistmodel {

  private Long success;
  private List<Pendingtasklis> pendingtasklist;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public List<Pendingtasklis> getPendingtasklist() {
    return pendingtasklist;
  }
  
  public void setPendingtasklist(List<Pendingtasklis> pendingtasklist) {
    this.pendingtasklist = pendingtasklist;
  }

}
