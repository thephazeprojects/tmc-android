package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ravi on 9/11/2017.
 */

public class GetInCompleteTaskData {
    @SerializedName("result")
    @Expose
    private List<GetInCompleteTaskResult> result = null;

    public List<GetInCompleteTaskResult> getResult() {
        return result;
    }

    public void setResult(List<GetInCompleteTaskResult> result) {
        this.result = result;
    }

    public GetInCompleteTaskData withResult(List<GetInCompleteTaskResult> result) {
        this.result = result;
        return this;
    }
}
