
package com.phazeprojectsystems.Model;






/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class Writereviewmodel {

  private Long success;
  private String msg;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public String getMsg() {
    return msg;
  }
  
  public void setMsg(String msg) {
    this.msg = msg;
  }

}
