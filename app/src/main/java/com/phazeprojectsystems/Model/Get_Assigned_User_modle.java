package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 6/2/18.
 */

public class Get_Assigned_User_modle {
    String success;
    List<GetAssigneUser_Result_modle> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<GetAssigneUser_Result_modle> getResult() {
        return result;
    }

    public void setResult(List<GetAssigneUser_Result_modle> result) {
        this.result = result;
    }
}
