package com.phazeprojectsystems.Model;

import com.google.android.gms.common.api.Api;

/**
 * Created by Ravi on 8/31/2017.
 */

public class CreateTaskData {
    String task_id;
    String task_name;
    String success;
    String message;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
