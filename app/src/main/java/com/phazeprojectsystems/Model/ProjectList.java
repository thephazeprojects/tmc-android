package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 8/22/2017.
 */
    public class ProjectList {

        @SerializedName("project_id")
        @Expose
        private String projectId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("projact_name")
        @Expose
        private String projactName;

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public ProjectList withProjectId(String projectId) {
            this.projectId = projectId;
            return this;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public ProjectList withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public String getProjactName() {
            return projactName;
        }

        public void setProjactName(String projactName) {
            this.projactName = projactName;
        }

        public ProjectList withProjactName(String projactName) {
            this.projactName = projactName;
            return this;
        }

    }

