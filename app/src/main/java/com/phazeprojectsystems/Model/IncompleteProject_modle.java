package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 5/1/18.
 */

public class IncompleteProject_modle {

    @SerializedName("result")
    @Expose
    private List<InCompleteProjectResult> result = null;
    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("msg")
    @Expose
    private String msg;

    public List<InCompleteProjectResult> getProjectList() {
        return result;
    }

    public void setProjectList(List<InCompleteProjectResult> projectList) {
        this.result = projectList;
    }

    public IncompleteProject_modle withProjectList(List<InCompleteProjectResult> projectList) {
        this.result = projectList;
        return this;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public IncompleteProject_modle withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
