package com.phazeprojectsystems.Model;

/**
 * Created by user on 15/1/18.
 */

public class InviteFriendData {
    private String success;
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
