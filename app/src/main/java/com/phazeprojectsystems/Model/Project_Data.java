package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 8/22/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
public class Project_Data {

    @SerializedName("project_list")
    @Expose
    private List<ProjectList> projectList = null;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    public List<ProjectList> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ProjectList> projectList) {
        this.projectList = projectList;
    }

    public Project_Data withProjectList(List<ProjectList> projectList) {
        this.projectList = projectList;
        return this;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Project_Data withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Project_Data withMessage(String message) {
        this.message = message;
        return this;
    }

}
