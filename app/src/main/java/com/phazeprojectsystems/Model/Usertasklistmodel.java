
package com.phazeprojectsystems.Model;

import java.util.List;




/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class Usertasklistmodel {

  private Long success;
  private List<Usertasklis> Usertasklist;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public List<Usertasklis> getUsertasklist() {
    return Usertasklist;
  }
  
  public void setUsertasklist(List<Usertasklis> Usertasklist) {
    this.Usertasklist = Usertasklist;
  }

}
