package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 5/1/18.
 */

public class CompleteProjectModle {


    @SerializedName("result")
    @Expose
    private List<CompleteProjectResult> result = null;
    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("msg")
    @Expose
    private String msg;

    public List<CompleteProjectResult> getProjectList() {
        return result;
    }

    public void setProjectList(List<CompleteProjectResult> projectList) {
        this.result = projectList;
    }

    public CompleteProjectModle withProjectList(List<CompleteProjectResult> projectList) {
        this.result = projectList;
        return this;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public CompleteProjectModle withSuccess(String success) {
        this.success = success;
        return this;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
