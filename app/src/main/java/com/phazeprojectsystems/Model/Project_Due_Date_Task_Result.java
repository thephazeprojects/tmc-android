package com.phazeprojectsystems.Model;

/**
 * Created by user on 3/1/18.
 */

public class Project_Due_Date_Task_Result {
    private String task_id;
    private String user_id;
    private String  project_id;
    private String task_name;
    private String description;
    private String status;
    private String create_date;
    private String due_date;
    private Integer add_tag;
    private String add_member;
    private String update_time;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public Integer getAdd_tag() {
        return add_tag;
    }

    public void setAdd_tag(Integer add_tag) {
        this.add_tag = add_tag;
    }

    public String getAdd_member() {
        return add_member;
    }

    public void setAdd_member(String add_member) {
        this.add_member = add_member;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }
}
