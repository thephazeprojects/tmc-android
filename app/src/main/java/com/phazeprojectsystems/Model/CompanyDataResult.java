package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 9/19/2017.
 */

public class CompanyDataResult {

    private String id;
    private String username;
    private String email;
    private String companyname;
    private String password;
    private String loginWithFacebook;
    private String loginWithGoogle;
    private String userStatus;
    private String userimage;

    private boolean isSelected=false;


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CompanyDataResult withId(String id) {this.id = id;return this;}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CompanyDataResult withUsername(String username) {this.username = username;return this;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CompanyDataResult withEmail(String email) {this.email = email;return this;}

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public CompanyDataResult withCompanyname(String companyname) {this.companyname = companyname;return this;}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CompanyDataResult withPassword(String password) {this.password = password;return this;}

    public String getLoginWithFacebook() {
        return loginWithFacebook;
    }

    public void setLoginWithFacebook(String loginWithFacebook) {this.loginWithFacebook = loginWithFacebook;}

    public CompanyDataResult withLoginWithFacebook(String loginWithFacebook) {this.loginWithFacebook = loginWithFacebook;return this;}

    public String getLoginWithGoogle() {
        return loginWithGoogle;
    }

    public void setLoginWithGoogle(String loginWithGoogle) {
        this.loginWithGoogle = loginWithGoogle;
    }

    public CompanyDataResult withLoginWithGoogle(String loginWithGoogle) {
        this.loginWithGoogle = loginWithGoogle;
        return this;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public CompanyDataResult withUserStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public String getUserimage() {
        return userimage;
    }

    public void setUserimage(String userimage) {
        this.userimage = userimage;
    }

    public CompanyDataResult withUserimage(String userimage) {
        this.userimage = userimage;
        return this;
    }
}
