package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 5/1/18.
 */

public class CompleteProjectResult {

    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("projact_name")
    @Expose
    private String projactName;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public CompleteProjectResult withProjectId(String projectId) {
        this.projectId = projectId;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public CompleteProjectResult withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getProjactName() {
        return projactName;
    }

    public void setProjactName(String projactName) {
        this.projactName = projactName;
    }

    public CompleteProjectResult withProjactName(String projactName) {
        this.projactName = projactName;
        return this;
    }
}
