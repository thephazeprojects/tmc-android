package com.phazeprojectsystems.Model;

/**
 * Created by NAVJOT SINGH on 03-11-2017.
 */

public class Commentdelete_model {

    private String response;
    private Long success;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getSuccess() {
        return success;
    }

    public void setSuccess(Long success) {
        this.success = success;
    }
}
