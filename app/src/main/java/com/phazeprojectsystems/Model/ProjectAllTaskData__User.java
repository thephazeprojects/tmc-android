package com.phazeprojectsystems.Model;

/**
 * Created by NAVJOT SINGH on 28-11-2017.
 */

public class ProjectAllTaskData__User {

    private String id;
    private String username;
    private String thumb;
    private String companyname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }
}
