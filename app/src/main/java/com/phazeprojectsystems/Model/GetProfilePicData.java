package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 9/20/2017.
 */

public class GetProfilePicData {

    public Long getSuccess() {
        return success;
    }

    public void setSuccess(Long success) {
        this.success = success;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    private Long success;
            private String thumb ;

}
