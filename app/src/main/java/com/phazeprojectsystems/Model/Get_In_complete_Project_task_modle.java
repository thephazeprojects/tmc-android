package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 3/1/18.
 */

public class Get_In_complete_Project_task_modle {

    private String success;
    private List<GetInCompleteProjectTask__result> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<GetInCompleteProjectTask__result> getResult() {
        return result;
    }

    public void setResult(List<GetInCompleteProjectTask__result> result) {
        this.result = result;
    }
}
