package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ravi on 9/18/2017.
 */

public class GetAllTaskData {

    @SerializedName("result")
    @Expose
    private List<GetAllTaskDataResult> result = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<GetAllTaskDataResult> getResult() {
        return result;
    }

    public void setResult(List<GetAllTaskDataResult> result) {
        this.result = result;
    }

    public GetAllTaskData withResult(List<GetAllTaskDataResult> result) {
        this.result = result;
        return this;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public GetAllTaskData withSuccess(Integer success) {
        this.success = success;
        return this;
    }

}
