package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 9/20/2017.
 */

public class UploadPicData {
    private Integer success;
    private String msg;
    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {

        this.success = success;
    }

    public UploadPicData withSuccess(Integer success){
        this.success = success;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UploadPicData withMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
