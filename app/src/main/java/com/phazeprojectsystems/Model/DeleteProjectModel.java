package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 21/2/18.
 */

public class DeleteProjectModel {
    private String success;
    private List<DeleteProject_Result_Model> result;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<DeleteProject_Result_Model> getResult() {
        return result;
    }

    public void setResult(List<DeleteProject_Result_Model> result) {
        this.result = result;
    }
}
