package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ravi on 9/8/2017.
 */

public class GetCompleteTaskData {

    @SerializedName("result")
    @Expose
    private List<GetCompleteTaskResult> result = null;

    public List<GetCompleteTaskResult> getResult() {
        return result;
    }

    public void setResult(List<GetCompleteTaskResult> result) {
        this.result = result;
    }

    public GetCompleteTaskData withResult(List<GetCompleteTaskResult> result) {
        this.result = result;
        return this;
    }
}
