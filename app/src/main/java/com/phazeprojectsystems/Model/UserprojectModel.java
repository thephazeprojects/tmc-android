
package com.phazeprojectsystems.Model;

import java.util.List;




/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class UserprojectModel {

  private Long success;
  private List<Userprojec> Userproject;
  private String records;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public List<Userprojec> getUserproject() {
    return Userproject;
  }
  
  public void setUserproject(List<Userprojec> Userproject) {
    this.Userproject = Userproject;
  }

  public String getRecords() {
    return records;
  }

  public void setRecords(String records) {
    this.records = records;
  }
}
