package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by Vishal on 9/27/2017.
 */

public class ProjectAllTaskData {

     private List<ProjectAllTaskData__msg> msg ;
    private String  success;
    private String error;

    public List<ProjectAllTaskData__msg> getMsg() {
        return msg;
    }

    public void setMsg(List<ProjectAllTaskData__msg> msg) {
        this.msg = msg;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
