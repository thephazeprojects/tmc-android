package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 8/2/2017.
 */

public class ForgetPasswordData {
    private String success;
    private String messange;
    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessange() {
        return messange;
    }

    public void setMessange(String messange) {
        this.messange = messange;
    }
}
