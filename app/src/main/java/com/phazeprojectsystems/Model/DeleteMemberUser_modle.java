package com.phazeprojectsystems.Model;

/**
 * Created by user on 1/2/18.
 */

public class DeleteMemberUser_modle {
    private String success;
    private String result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
