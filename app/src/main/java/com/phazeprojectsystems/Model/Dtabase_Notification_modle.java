package com.phazeprojectsystems.Model;

import com.phazeprojectsystems.SharePool.NotificationData;

/**
 * Created by user on 17/1/18.
 */

public class Dtabase_Notification_modle {

    private int id;
    private String notificationTime;
    private String messege;

    private String gettaskname;
    private String getduwdate ;
    private String getdescription;

    private String gettaskid ;
    private String getaddtag;


    public Dtabase_Notification_modle(Integer id,String msg ,String time,String taskname,String Duedate,String discription,
                                      String taskid,String addtag){
        this.setId(id);
        this.setMessege(msg);
        this.setNotificationTime(time);
        this.setGettaskname(taskname);
        this.setGetduwdate(Duedate);
        this.setGetdescription(discription);
        this.setGettaskid(taskid);
        this.setGetaddtag(addtag);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getMessege() {
        return messege;
    }

    public void setMessege(String messege) {
        this.messege = messege;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getGettaskname() {
        return gettaskname;
    }

    public void setGettaskname(String gettaskname) {
        this.gettaskname = gettaskname;
    }

    public String getGetduwdate() {
        return getduwdate;
    }

    public void setGetduwdate(String getduwdate) {
        this.getduwdate = getduwdate;
    }

    public String getGetdescription() {
        return getdescription;
    }

    public void setGetdescription(String getdescription) {
        this.getdescription = getdescription;
    }

    public String getGettaskid() {
        return gettaskid;
    }

    public void setGettaskid(String gettaskid) {
        this.gettaskid = gettaskid;
    }

    public String getGetaddtag() {
        return getaddtag;
    }

    public void setGetaddtag(String getaddtag) {
        this.getaddtag = getaddtag;
    }
}
