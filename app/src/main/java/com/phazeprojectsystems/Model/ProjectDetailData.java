package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by NAVJOT SINGH on 28-09-2017.
 */

public class ProjectDetailData {
    private List<ProjectDetailResult> result = null;
    private Integer success;

    public List<ProjectDetailResult> getResult() {
        return result;
    }

    public void setResult(List<ProjectDetailResult> result) {
        this.result = result;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
