package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 3/1/18.
 */

public class Project_Due_date_Task_modle {

    private String success;
    private List<Project_Due_Date_Task_Result> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Project_Due_Date_Task_Result> getResult() {
        return result;
    }

    public void setResult(List<Project_Due_Date_Task_Result> result) {
        this.result = result;
    }


}
