package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by NAVJOT SINGH on 28-11-2017.
 */

public class ProjectAllTaskData__msg {

      private List<ProjectAllTaskData__User> user;
      private ProjectAllTaskData__Task  task;

    public List<ProjectAllTaskData__User> getUser() {
        return user;
    }

    public void setUser(List<ProjectAllTaskData__User> user) {
        this.user = user;
    }

    public ProjectAllTaskData__Task  getTask() {
        return task;
    }

    public void setTask(ProjectAllTaskData__Task task) {
        this.task = task;
    }
}
