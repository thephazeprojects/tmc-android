package com.phazeprojectsystems.Model;

/**
 * Created by Vishal on 9/27/2017.
 */

public class SelectedMemberData {
    private  String UserName;
    private String Userid;
    public SelectedMemberData(String username, String userid ){
        this.UserName=username;
        this.Userid=userid;
    }
    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String userid) {
        Userid = userid;
    }

}
