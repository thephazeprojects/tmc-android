package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by user on 3/1/18.
 */

public class Get_Project_CompleteTask_modle {
    private String success;
    private List<Get_Project_complete_task_result> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Get_Project_complete_task_result> getResult() {
        return result;
    }

    public void setResult(List<Get_Project_complete_task_result> result) {
        this.result = result;
    }
}
