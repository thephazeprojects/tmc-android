package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 8/2/2017.
 */

public class RegisterData {

    String userid;
    String success;
    String message;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

