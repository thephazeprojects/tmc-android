package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 9/11/2017.
 */

public class DeleteTaskData {
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    String response ;
     String success;
}
