package com.phazeprojectsystems.Model;

import java.util.List;

/**
 * Created by Ravi on 9/19/2017.
 */

public class CompanyData {
    private List<CompanyDataResult> result = null;
    private Integer success;

    public List<CompanyDataResult> getResult() {
        return result;
    }

    public void setResult(List<CompanyDataResult> result) {
        this.result = result;
    }

    public CompanyData withResult(List<CompanyDataResult> result) {
        this.result = result;
        return this;
    }

    public Integer getSuccess() {
        return success;}

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public CompanyData withSuccess(Integer success) {
        this.success = success;
        return this;
    }}
