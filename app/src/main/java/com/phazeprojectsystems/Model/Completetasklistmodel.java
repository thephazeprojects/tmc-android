
package com.phazeprojectsystems.Model;

import java.util.List;




/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class Completetasklistmodel {

  private Long success;
  private List<Completetasklis> completetasklist;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public List<Completetasklis> getCompletetasklist() {
    return completetasklist;
  }
  
  public void setCompletetasklist(List<Completetasklis> completetasklist) {
    this.completetasklist = completetasklist;
  }

}
