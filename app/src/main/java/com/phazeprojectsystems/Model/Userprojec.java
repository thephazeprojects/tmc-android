
package com.phazeprojectsystems.Model;


import com.google.gson.annotations.SerializedName;



/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class Userprojec {

  @SerializedName("project_id")
  private String projectId;
  @SerializedName("user_id")
  private String userId;
  @SerializedName("projact_name")
  private String projactName;
  private String date;
  private String address;
  private String description;
  @SerializedName("due_date")
  private String dueDate;


  public String getProjectId() {
    return projectId;
  }
  
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getUserId() {
    return userId;
  }
  
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getProjactName() {
    return projactName;
  }
  
  public void setProjactName(String projactName) {
    this.projactName = projactName;
  }

  public String getDate() {
    return date;
  }
  
  public void setDate(String date) {
    this.date = date;
  }

  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }

  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }

  public String getDueDate() {
    return dueDate;
  }
  
  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

}
