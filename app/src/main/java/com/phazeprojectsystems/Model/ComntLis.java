
package com.phazeprojectsystems.Model;


/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class ComntLis {

  private Integer comentid;
  private String userid;
  private String taskid;
  private String projectid;
  private String comment;
  private String time;
  private String username;
  private String ndate;

  public Integer getComentid() {
    return comentid;
  }

  public void setComentid(Integer comentid) {
    this.comentid = comentid;
  }



  public String getUserthumb() {
    return userthumb;
  }

  public void setUserthumb(String userthumb) {
    this.userthumb = userthumb;
  }

  private String userthumb;

  public String getAttach() {
    return attach;
  }

  public void setAttach(String attach) {
    this.attach = attach;
  }

  private String attach;

  public String getThumb() {
    return thumb;
  }

  public void setThumb(String thumb) {
    this.thumb = thumb;
  }

  private String thumb;

  public String getNdate() {
    return ndate;
  }

  public void setNdate(String ndate) {
    this.ndate = ndate;
  }



  public String getUserid() {
    return userid;
  }
  
  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getTaskid() {
    return taskid;
  }
  
  public void setTaskid(String taskid) {
    this.taskid = taskid;
  }

  public String getProjectid() {
    return projectid;
  }
  
  public void setProjectid(String projectid) {
    this.projectid = projectid;
  }

  public String getComment() {
    return comment;
  }
  
  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getTime() {
    return time;
  }
  
  public void setTime(String time) {
    this.time = time;
  }

  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }

}
