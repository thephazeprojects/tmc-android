
package com.phazeprojectsystems.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;



/**
 * Created by https://learnpainless.com
 * and http://edablogs.com
 */

public class AllcommentModel {

  private Long success;
  @SerializedName("comnt_list")
  private List<ComntLis> comntList;


  public Long getSuccess() {
    return success;
  }
  
  public void setSuccess(Long success) {
    this.success = success;
  }

  public List<ComntLis> getComntList() {
    return comntList;
  }
  
  public void setComntList(List<ComntLis> comntList) {
    this.comntList = comntList;
  }

}
