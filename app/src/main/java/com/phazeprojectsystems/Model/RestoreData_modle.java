package com.phazeprojectsystems.Model;

/**
 * Created by user on 21/2/18.
 */

public class RestoreData_modle {
   private  String success;
   private  String msg;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
