package com.phazeprojectsystems.Model;

/**
 * Created by Ravi on 8/17/2017.
 */

public class Create_ProjectData {
        String project_id;
        String success;
        String message;

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
