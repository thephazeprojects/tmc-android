package com.phazeprojectsystems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 9/18/2017.
 */

public class GetAllTaskDataResult {

    @SerializedName("projectdata")
    @Expose
    private String projectdata;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("task_id")
    @Expose
    private String taskId;
    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("task_name")
    @Expose
    private String taskName;
    @SerializedName("description")
    @Expose
    private String description;

    public String getProjectdata() {
        return projectdata;
    }

    public void setProjectdata(String projectdata) {
        this.projectdata = projectdata;
    }

    public GetAllTaskDataResult withProjectdata(String projectdata) {
        this.projectdata = projectdata;
        return this;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public GetAllTaskDataResult withProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public GetAllTaskDataResult withTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public GetAllTaskDataResult withProjectId(String projectId) {
        this.projectId = projectId;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public GetAllTaskDataResult withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public GetAllTaskDataResult withTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GetAllTaskDataResult withDescription(String description) {
        this.description = description;
        return this;
    }
}
