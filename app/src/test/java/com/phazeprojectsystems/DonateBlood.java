//package com.phazeprojectsystems;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.ProgressDialog;
//import android.content.ActivityNotFoundException;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Environment;
//import android.provider.MediaStore;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.navjotsingh.bloodbank.Activity.Activity.Hello.API.Register_API;
//import com.example.navjotsingh.bloodbank.Activity.Activity.Hello.Constant.Constant;
//import com.example.navjotsingh.bloodbank.Activity.Activity.Hello.Getter_Setter.Regiter;
//import com.example.navjotsingh.bloodbank.Activity.Activity.Hello.utils.Network_check;
//import com.example.navjotsingh.bloodbank.Activity.Activity.Hello.utils.Utility;
//import com.example.navjotsingh.bloodbank.R;
//
//import java.io.File;
//import java.sql.Timestamp;
//
//import retrofit.Callback;
//import retrofit.RestAdapter;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//import retrofit.mime.TypedFile;
//
//import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
//import static com.phazeprojectsystems.R.id.view;
//
//
//public class DonateBlood extends AppCompatActivity {
//    Button register;
//    String first_name, last_name, email, address, mobile_number, alternative_mobileno, place, Age, BloodGroup, Gender;
//    EditText firstname, lastname, email_address, address_, mobilenumber, alternativemobileno, place_d, Age_;
//    Spinner gender, bloodgroup;
//    Button uploadimage;
//    TextView uploadimagetext;
//    TypedFile avatarFile1 = null;
//    String picturePath = "";
//    final int PIC_CROP = 2;
//    final int CAMERA_CAPTURE = 1;
//    private String userChoosenTask;
//    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
//    private Uri picUri;
//    Activity ctx;
//    File uri;
//
//    ProgressDialog dialog;
//
//    private static int RESULT_LOAD_IMAGE = 1;
//
//    private String[] genderSpinner, bloodgroupSpinner;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_donate_blood);
//
//
//        if (android.os.Build.VERSION.SDK_INT >= 21) {
//            Window window = this.getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            window.setStatusBarColor(this.getResources().getColor(R.color.red));
//        }
//
//
//        find();
//        spinnerarray();
//        array_adapter();
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        switch (requestCode) {
//            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if(userChoosenTask.equals("Take Photo"))
//                        cameraIntent();
//                    else if(userChoosenTask.equals("Choose from Library"))
//                        galleryIntent();
//                } else {
//                    //code for deny
//                }
//                break;
//        }
//    }
//
//
//
//    private void spinnerarray() {
//        this.genderSpinner = new String[]{
//                "Gender", "Male", "Female"
//        };
//        this.bloodgroupSpinner = new String[]{
//                "Blood Group", "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"
//        };
//    }
//
//    private void array_adapter() {
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.spinner_item, genderSpinner);
//        gender.setAdapter(adapter);
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
//                R.layout.spinner_item, bloodgroupSpinner);
//        bloodgroup.setAdapter(adapter1);
//
//
//    }
//
//    //code for get the path of the image
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == SELECT_FILE){
//                onSelectFromGalleryResult(data);
//            }
//
//            else if (requestCode == REQUEST_CAMERA){
//                Log.e("log path",""+uri);
//                String myImagename = getImageName(String.valueOf(uri));
//                uploadimagetext.setText(myImagename);
//            }
//
//        }
//
//    }
//
//    // code for spliting the path of a image to show only image name
//    private String getImageName(String picturePath) {
//        String Imagename = null;
//
//        String[] name_array = picturePath.split("/");
//        Log.e("size of array", "" + name_array.length);
//
//        Imagename = name_array[name_array.length - 1];
//
//        Log.e("name of image", "" + Imagename);
//
//        return Imagename;
//    }
//
//
//    @SuppressWarnings("deprecation")
//    private void onSelectFromGalleryResult(Intent data) {//////////////////////////////////////////////////////////////////////////////////////////
//
//        Bitmap bm=null;
//        if (data != null) {
//
//            Uri selectedImage = data.getData();
//            String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//
//            Cursor cursor = getContentResolver().query(selectedImage,
//                    filePathColumn, null, null, null);
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            picturePath = cursor.getString(columnIndex);
//            cursor.close();
//
//            String myimagename = getImageName(picturePath);
//
//            uploadimagetext.setText(myimagename);
////            try {
////                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
//        }
//
////        ivImage.setImageBitmap(bm);
//    }
//
//
//    private void find() {
//
//
//        register = (Button) findViewById(R.id.register);
//        Age_ = (EditText) findViewById(R.id.age);
//        firstname = (EditText) findViewById(R.id.first_name);
//        lastname = (EditText) findViewById(R.id.last_name);
//        email_address = (EditText) findViewById(R.id.email_address);
//        address_ = (EditText) findViewById(R.id.address);
//        mobilenumber = (EditText) findViewById(R.id.mobile_number);
//        alternativemobileno = (EditText) findViewById(R.id.alternative_mobileno);
//        place_d = (EditText) findViewById(R.id.place);
//        gender = (Spinner) findViewById(R.id.spinner_gender);
//        bloodgroup = (Spinner) findViewById(R.id.spinner_bloodgroup);
//        uploadimage = (Button) findViewById(R.id.upload_image);
//        uploadimagetext = (TextView) findViewById(R.id.upload_image_text);
//
//
//        uploadimage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                selectImage();
//
//                Intent i = new Intent(
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                startActivityForResult(i, RESULT_LOAD_IMAGE);
//
//                try {
//                    //use standard intent to capture an image
//                    Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    //we will handle the returned data in onActivityResult
//                    startActivityForResult(captureIntent, CAMERA_CAPTURE);
//                } catch (ActivityNotFoundException anfe) {
//                    //display an error message
//                    String errorMessage = "Whoops - your device doesn't support capturing images!";
//                    Toast toast = Toast.makeText(DonateBlood.this, errorMessage, Toast.LENGTH_SHORT);
//                    toast.show();
////                }
//
//            }
//
//        });
//
//                register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                first_name = firstname.getText().toString();
//                last_name = lastname.getText().toString();
//                email = email_address.getText().toString();
//                Age = Age_.getText().toString();
//                address = address_.getText().toString();
//                mobile_number = mobilenumber.getText().toString();
//                alternative_mobileno = alternativemobileno.getText().toString();
//                place = place_d.getText().toString();
//                Gender = gender.getSelectedItem().toString();
//                BloodGroup = bloodgroup.getSelectedItem().toString();
//
//                if (first_name.trim().equalsIgnoreCase("")) {
//                    firstname.setError("Please Enter First Name");
//                    firstname.requestFocus();
//                } else if (last_name.trim().equalsIgnoreCase("")) {
//                    lastname.setError("Please Enter Last Name");
//                    lastname.requestFocus();
//
//                } else if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
//                    email_address.setError("Please Enter valid  email address");
//                    email_address.requestFocus();
//
//                } else if (Age.trim().equalsIgnoreCase("")) {
//                    Age_.setError("Please enter Age");
//                    Age_.requestFocus();
//
//                } else if (gender.getSelectedItem().toString().equals("Gender")) {
//
//                    gender.performClick();
//                    gender.requestFocus();
//
//
//                } else if (bloodgroup.getSelectedItem().toString().trim().equals("Blood Group")) {
//
//                    bloodgroup.requestFocus();
//                    bloodgroup.performClick();
//
//                } else if (mobile_number.trim().equalsIgnoreCase("")) {
//                    mobilenumber.setError("Please Enter  mobile number");
//                    mobilenumber.requestFocus();
//
//                }
//                else if (mobile_number.length()<10) {
//                    mobilenumber.setError("Please Enter correct mobile number");
//                    mobilenumber.requestFocus();
//
//                } else if (alternative_mobileno.trim().equalsIgnoreCase("")) {
//                    alternativemobileno.setError("Please Enter  mobile number");
//                    alternativemobileno.requestFocus();
//
//                }
//                else if (alternative_mobileno.length()<10) {
//                    alternativemobileno.setError("Please Enter correct mobile number");
//                    alternativemobileno.requestFocus();
//
//                }
//                else if (place.trim().equalsIgnoreCase("")) {
//                    place_d.setError("Please Enter place");
//                    place_d.requestFocus();
//
//                } else if (address.trim().equalsIgnoreCase("")) {
//                    address_.setError("Please Enter address");
//                    address_.requestFocus();
//                }
//                else {
//
//
//                        Register_user(first_name, last_name, email, Age, Gender, BloodGroup, mobile_number,
//                                alternative_mobileno, place, address, picturePath);
//                    }
//
//                            }
//        });
//
//    }
//
//    private void selectImage() {
//
//        final CharSequence[] items = { "Take Photo", "Choose from Library",
//                "Cancel" };
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(DonateBlood.this);
//        builder.setTitle("Add Photo!");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                boolean result=Utility.checkPermission(DonateBlood.this);
//
//                if (items[item].equals("Take Photo")) {
//                    userChoosenTask ="Take Photo";
//                    if(result)
//                        cameraIntent();
//
//                } else if (items[item].equals("Choose from Library")) {
//                    userChoosenTask ="Choose from Library";
//                    if(result)
//                        galleryIntent();
//
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }
//
//    private void galleryIntent()
//    {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        //intent.setType("image/*");
//        //intent.setAction(Intent.ACTION_GET_CONTENT);//
//
//        startActivityForResult(intent,SELECT_FILE);
//    }
//
//    private void cameraIntent()
//    {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        uri = getOutputMediaFile(MEDIA_TYPE_IMAGE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        startActivityForResult(intent, REQUEST_CAMERA);
//
//    }
//    private static File getOutputMediaFile(int type) {
//        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/.Dope/");
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
////                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
//                return null;
//            }
//        }
//
//        File mediaFile;
//        java.util.Date date = new java.util.Date();
//        if (type == MEDIA_TYPE_IMAGE) {
//            mediaFile = new File(mediaStorageDir, Long.toString(System.currentTimeMillis()) + "IMG_" + new Timestamp(date.getTime()).toString() + ".jpg");
//        }  else {
//            return null;
//        }
//        return mediaFile;
//    }
//
//    private void Register_user(String first_name, String last_name, String email, String Age, String Gender, String BloodGroup,
//                               String mobile_number, String alternative_mobileno, String place,
//                               String address, String avatarFile)
//    {
//
//        if (avatarFile != null) {
//            avatarFile1 = new TypedFile("image/*", new File(String.valueOf(avatarFile)));
//        }
//        if(Network_check.isNetWorking(DonateBlood.this))
//        {
//
//            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.constant).build();
//        Register_API locatioApi = restAdapter.create(Register_API.class);
//
//
//        dialog = new ProgressDialog(DonateBlood.this);
//        dialog.setTitle("Processing... ");
//            dialog.setMessage("Please wait.");
//
//        dialog.setCancelable(false);
//        dialog.show();
//
//               locatioApi.getuser(first_name, last_name, email, Age, Gender, BloodGroup, mobile_number, alternative_mobileno, place,
//                       address, avatarFile1, new Callback<Regiter>() {
//            @Override
//            public void success(Regiter regiter, Response response) {
//
//
//                String status = regiter.getSucess();
//
//                if (status.equalsIgnoreCase("1")) {
//
//                 String userdid= regiter.getResponse().getUserid();
//                    String image= regiter.getResponse().getImage();
//                    String profilename= regiter.getResponse().getFirstname();
//                    String city= regiter.getResponse().getCity();
//
////                    Toast.makeText(DonateBlood.this,userdid,Toast.LENGTH_LONG).show();
//
//                    SharedPreferences pref = getApplicationContext().getSharedPreferences("Hello", MODE_PRIVATE);
//                    SharedPreferences.Editor editor = pref.edit();
//                    editor.putString("userid", userdid);
//                    editor.putString("name", profilename);
//                    editor.putString("image", image);//
//                    editor.putString("place", city);//
//                    // Saving string
//                    editor.commit();
//
//                    firstname.setText("");
//                    lastname.setText("");
//                    email_address.setText("");
//                    Age_.setText("");
//                    gender.setSelection(0);
//                    bloodgroup.setSelection(0);
//                    address_.setText("");
//                    mobilenumber.setText("");
//                    alternativemobileno.setText("");
//                    place_d.setText("");
//                    {
//
////                        Toast.makeText(DonateBlood.this, "Register Succesfully", Toast.LENGTH_LONG).show();
//                    }
//                    dialog.dismiss();
//                    Intent i = new Intent(DonateBlood.this, FragmentAct.class);
//                    startActivity(i);
//
//                }
//                else {
//                    Toast.makeText(DonateBlood.this, "Email Id already exist", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//                Log.e("error", "" + error);
//
////                Toast.makeText(DonateBlood.this, "Failure", Toast.LENGTH_LONG).show();
//                dialog.dismiss();
//            }
//        });
//        }
//        else {
//            Toast.makeText(this, "Please check your network", Toast.LENGTH_SHORT).show();
//        }
//    }
//}
//
//
